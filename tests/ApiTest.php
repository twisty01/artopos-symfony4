<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 26-Apr-18
 * Time: 21:58
 */

namespace tests;

use App\Entity\Institution;
use App\Entity\Post;
use App\Repository\AuthorRepository;
use App\Repository\InstitutionRepository;
use App\Repository\PaintingRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiTest extends WebTestCase {

    public function testAuthors() {
        $client = static::createClient();
        $container = $client->getContainer();
        $repo = $container->get(AuthorRepository::class);
        $authors = $repo->findAll();
        foreach ($authors as $p) {
            $client->request('GET', 'http://localhost:8000/api/authors/' . $p->getId());
            $response = $client->getResponse();
            $responseData = json_decode($response->getContent(), true);
            $this->assertTrue($p->getId() == $responseData['id'], $responseData['id'] . " test test " . $p->getId());
            $this->assertTrue($p->getName() == $responseData['name'], "Error in 'name':" . $responseData['name']);
        }
        $client->request('GET', 'http://localhost:8000/api/authors');
        $response = $client->getResponse();
        $responseData = json_decode($response->getContent(), true);
        $this->assertTrue(count($responseData) == count($authors), "authors count mismatch");
    }

    public function testInstitutions() {
        $client = static::createClient();
        $container = $client->getContainer();
        $repo = $container->get(InstitutionRepository::class);
        $insts = $repo->findAll();
        foreach ($insts as $p) {
            $client->request('GET', 'http://localhost:8000/api/institutions/' . $p->getId());
            $response = $client->getResponse();
            $responseData = json_decode($response->getContent(), true);
            $this->assertTrue($p->getId() == $responseData['id'], $responseData['id'] . " test test " . $p->getId());
            $this->assertTrue($p->getName() == $responseData['name'], "Error in 'name':" . $responseData['name']);
        }
        $client->request('GET', 'http://localhost:8000/api/institutions');
        $response = $client->getResponse();
        $responseData = json_decode($response->getContent(), true);
        $this->assertTrue(count($responseData) == count($insts), "institutions count mismatch");
    }

    public function testPaintings() {
        $client = static::createClient();
        $container = $client->getContainer();
        $paintingRepo = $container->get(PaintingRepository::class);
        $paintings = $paintingRepo->findAll();
        foreach ($paintings as $p) {
            $client->request('GET', 'http://localhost:8000/api/paintings/' . $p->getId());
            $response = $client->getResponse();
            $responseData = json_decode($response->getContent(), true);
            $this->assertTrue($p->getId() == $responseData['id'], $responseData['id'] . " test test " . $p->getId());
            $this->assertTrue($p->getLat() == $responseData['lat'], "Error in 'lat':" . $responseData['lat']);
            $this->assertTrue($p->getLng() == $responseData['lng'], "Error in 'lng':" . $responseData['lng']);
            $this->assertTrue($p->getImg() == $responseData['img'], "Error in 'img':" . $responseData['img']);
            $this->assertTrue($p->getName() == $responseData['name'], "Error in 'name':" . $responseData['name']);

            $client->request('GET', 'http://localhost:8000/api/paintings/' . $p->getId() . '/posts');
            $postsRes = $client->getResponse();
            $resPosts = json_decode($postsRes->getContent(), true);
            $this->assertTrue(count($resPosts) == count($p->getPosts()), "painting posts count mismatch");
        }


        $client->request('GET', 'http://localhost:8000/api/paintings');
        $response = $client->getResponse();
        $responseData = json_decode($response->getContent(), true);
        $this->assertTrue(count($responseData) == count($paintings), "paintings count mismatch");
    }

    public function testAddPost() {
        $client = static::createClient();
        // TEST REGISTRATION
        $userContent = [
            'email' => self::randomReadableString(5) . '@' . self::randomReadableString(5) . '.' . self::randomReadableString(3),
            'password' => self::randomReadableString(10),
            'username' => self::randomReadableString(10),
        ];
        $client->request('POST', 'http://localhost:8000/api/users', [], [], [], json_encode($userContent));
        $userRes = $client->getResponse();
        $this->assertTrue($userRes->getStatusCode() >= 200 && $userRes->getStatusCode() < 400, "Registration status not OK\n" . $userRes->getStatusCode() . "\n" . $userRes->getContent());
        // TEST LOGIN
        $clientId = "6_4h2q66dz1ksgoocwk4ksgk0wksww4sc4w0wgoss4s8ko44cw4o";
        $clientSecret = "4rpyq7txeveo88ssw0cgwow4884k0g8oko4gkgogcs4g84kssk";
        $par = "?grant_type=password&client_id=" . $clientId . "&client_secret=" . $clientSecret . "&password=" . $userContent['password'] . "&username=" . $userContent['email'];
        $client->request('GET', 'http://localhost:8000/oauth/v2/token' . $par);
        $tokenRes = $client->getResponse();
        $tokenData = json_decode($tokenRes->getContent(), true);
        $this->assertArrayHasKey('access_token', $tokenData, "Token request - missing access_token : " . $tokenRes->getContent());
        $this->assertArrayHasKey('refresh_token', $tokenData, "Token request - missing refresh_token : " . $tokenRes->getContent());
        $this->assertArrayHasKey('expires_in', $tokenData, "Token request - missing  : " . $tokenRes->getContent());
        // TEST ADD POST
        $container = $client->getContainer();
        $paintingRepo = $container->get(PaintingRepository::class);
        $paintings = $paintingRepo->findAll();
        $painting = $paintings[2];
        $img = "data:image/gif;base64,R0lGODlhEAAQAMQAAORHHOVSKudfOulrSOp3WOyDZu6QdvCchPGolfO0o/XBs/fNwfjZ0frl3/zy7////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAkAABAALAAAAAAQABAAAAVVICSOZGlCQAosJ6mu7fiyZeKqNKToQGDsM8hBADgUXoGAiqhSvp5QAnQKGIgUhwFUYLCVDFCrKUE1lBavAViFIDlTImbKC5Gm2hB0SlBCBMQiB0UjIQA7";
        $newPostContent = [
            'img' => $img,
            'text' => self::randomReadableString(50),
            'lng' => 14.461678,
            'lat' => 50.075287,
            'paintingId' => $painting->getId()
        ];
        $client->request('POST', 'http://localhost:8000/api/posts?access_token=' . $tokenData['access_token'], [], [], [], json_encode($newPostContent));
        $newPostRes = $client->getResponse();
        $this->assertTrue($newPostRes->getStatusCode() >= 200 && $newPostRes->getStatusCode() < 400, "Add post status not OK : " . $newPostRes->getStatusCode() . "  :  " . $newPostRes->getContent());
        $id = $newPostRes->getContent();

        $client->request('GET', 'http://localhost:8000/api/posts/' . $id);
        $postRes = $client->getResponse();
        $postData = json_decode($postRes->getContent(), true);
        $this->assertTrue($postData['text'] == $newPostContent['text'], 'New post text mismatch');
        $this->assertTrue($postData['lat'] == $newPostContent['lat'], 'New post lat mismatch');
        $this->assertTrue($postData['lng'] == $newPostContent['lng'], 'New post lng mismatch');
        $this->assertArrayHasKey('img', $postData, 'New post does not have img');
        $this->assertArrayHasKey('tn', $postData, 'New post does not have tn');
    }


    static function randomReadableString($length = 20) {
        $string = '';
        $vowels = ["a", "e", "i", "o", "u"];
        $consonants = [
            'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm',
            'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'
        ];
        // Seed it
        srand((double)microtime() * 1000000);
        $max = $length / 2;
        for ($i = 1; $i <= $max; $i++) {
            $string .= $consonants[rand(0, 19)];
            $string .= $vowels[rand(0, 4)];
        }
        return $string;
    }


}