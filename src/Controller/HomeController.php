<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 13-Apr-18
 * Time: 12:12
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller {
    function indexAction() {
        return $this->render('index.html.twig', [

        ]);
    }
}