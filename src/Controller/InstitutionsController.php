<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 13-Apr-18
 * Time: 11:22
 */

namespace App\Controller;


use App\Controller\Traits\GetActionTrait;
use App\Controller\Traits\GetEntityNameInterface;
use App\Controller\Traits\GetPaintingsActionTrait;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;

class InstitutionsController extends FOSRestController implements ClassResourceInterface,GetEntityNameInterface {

    use GetActionTrait, GetPaintingsActionTrait;

    public function getEntityName(): string {
        return 'App:Institution';
    }
}