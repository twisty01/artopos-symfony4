<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 15-Apr-18
 * Time: 14:20
 */

namespace App\Controller\Traits;


interface GetEntityNameInterface {
    public function getEntityName() : string;
}