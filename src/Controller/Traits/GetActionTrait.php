<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 15-Apr-18
 * Time: 14:17
 */

namespace App\Controller\Traits;

use Swagger\Annotations as SWG;

trait GetActionTrait {

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the Entity defined by id",
     * )
     */
    public function getAction($id){
        $entity = $this->getDoctrine()->getRepository($this->getEntityName())->read($id);
        return $this->handleView($view = $this->view($entity, 200));
    }

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns all Entities",
     * )
     */
    public function cgetAction(){
        $entities = $this->getDoctrine()->getRepository($this->getEntityName())->findAll();
        return $this->handleView($view = $this->view($entities, 200));
    }

}