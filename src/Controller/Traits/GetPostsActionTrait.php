<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 15-Apr-18
 * Time: 14:18
 */

namespace App\Controller\Traits;

use Swagger\Annotations as SWG;

trait GetPostsActionTrait {

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns all Posts that belongs to the Entity, that is defined by id",
     * )
     */
    public function getPostsAction($id){
        $entity = $this->getDoctrine()->getRepository($this->getEntityName())->read($id);
        return $this->handleView($view = $this->view($entity->getPosts(), 200));
    }
}