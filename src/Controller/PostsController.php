<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 13-Apr-18
 * Time: 11:11
 */

namespace App\Controller;

use App\Controller\Traits\GetActionTrait;
use App\Controller\Traits\GetEntityNameInterface;
use App\Entity\Post;
use App\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;

class PostsController extends FOSRestController implements ClassResourceInterface, GetEntityNameInterface {
    use GetActionTrait;

    /**
     * @SWG\Response(
     *     response=200,
     *     description="Returns the User that is logged in."
     * )
     */
    function getCurrentAction() {
        $token = $this->container->get('security.token_storage')->getToken();
        if (!$token) return $this->handleView(new View("Unauthorized", Response::HTTP_UNAUTHORIZED));
        $user = $token->getUser();
        if (!$user || !is_a($user, User::class)) return $this->handleView(new View("Unauthorized", Response::HTTP_UNAUTHORIZED));
        return $this->handleView($this->view($user, 200));
    }

//    /**
//     * @SWG\Post(description="Creates new Post.")
//     * @SWG\Response(
//     *     response=200,
//     *     description="Returns Post id, if the Post was sucesfully created."
//     * )
//     */
    function cpostAction(Request $request) {
        $data = json_decode($request->getContent(), true);
        $lat = key_exists("lat", $data) ? $data["lat"] : null;
        $lng = key_exists("lng", $data) ? $data["lng"] : null;
        $text = key_exists("text", $data) ? $text = $data["text"] : null;
        $img = key_exists("img", $data) ? $data["img"] : null;
        $paintingId = key_exists("paintingId", $data) ? $data["paintingId"] : null;
        if (!$paintingId) return $this->handleView(new View("Bad request - missing painting id", Response::HTTP_NOT_ACCEPTABLE));
        if ((!$lat || !$lng) && !$text) return $this->handleView(new View("Bad request - missing text or coords", Response::HTTP_NOT_ACCEPTABLE));

        $token = $this->container->get('security.token_storage')->getToken();
        $user = $token->getUser();
        if (!$user || !is_a($user, User::class)) return $this->handleView(new View("Unauthorized", Response::HTTP_UNAUTHORIZED));
        $painting = $this->getDoctrine()->getRepository("App:Painting")->find($paintingId);
        if (!$painting) return $this->handleView(new View("Bad request - painting not found", Response::HTTP_NOT_ACCEPTABLE));
        $now = new \DateTime();
        $post = (new Post())->setLng($lng)->setLat($lat)->setUser($token->getUser())->setPainting($painting)->setText($text);
        if ($img) {
            $imgId = "posts/" . $post->getId() . "p" . $paintingId . "-u" . $user->getId() . "-d" . $now->format("Y-m-d");
            $response = $this->get('speicher210_cloudinary.uploader')->upload($img, [
                "public_id" => $imgId,
                "eager" => [
                    ["width" => 2000],
                    ["width" => 400]
                ]
            ]);
            $pubId = $response["public_id"];
            $eager = $response["eager"];
            if (!$eager || !$pubId) return $this->handleView(new View("Failed to upload image", Response::HTTP_NOT_ACCEPTABLE));
            $post->setImgPublicId($pubId);
            $post->setImg($eager[0]["url"]);
            $post->setTn($eager[1]["url"]);
        }
        $this->getDoctrine()->getRepository("App:Post")->create($post);
        return $this->handleView(new View($post->getId(), 200));
    }

    function deleteAction($id) {
        $token = $this->container->get('security.token_storage')->getToken();
        if (!$token) return $this->handleView(new View("Unauthorized", Response::HTTP_UNAUTHORIZED));
        $user = $token->getUser();
        if (!$user || !is_a($user, User::class)) return $this->handleView(new View("Unauthorized", Response::HTTP_UNAUTHORIZED));
        $repo = $this->getDoctrine()->getRepository("App:Post");
        $post = $repo->read($id);
        /** @var Post $post */
        if (!$post->getUser() || $post->getUser()->getId() != $user->getId()) return $this->handleView(new View("Unauthorized", Response::HTTP_UNAUTHORIZED));
        if ($post->getImgPublicId() && $post->getImg() && strpos($post->getImg(), 'cloudinary') !== false){
            $response = $this->get('speicher210_cloudinary.uploader')->destroy($post->getImgPublicId());
            if (!key_exists("result",$response) || $response['result'] != 'ok'){
                return $this->handleView(new View("Failed to delete image", Response::HTTP_NOT_ACCEPTABLE));            }
        }
        $post = $repo->delete($post);
        return $this->handleView(new View($post->getId(), 200));
    }

    public function getEntityName(): string {
        return 'App:Post';
    }
}