<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 13-Apr-18
 * Time: 11:02
 */

namespace App\Controller;

use App\Controller\Traits\GetActionTrait;
use App\Controller\Traits\GetEntityNameInterface;
use App\Controller\Traits\GetPostsActionTrait;
use App\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class UsersController extends FOSRestController implements ClassResourceInterface, GetEntityNameInterface {
    use GetActionTrait, GetPostsActionTrait;

    function getCurrentAction() {
        $token = $this->container->get('security.token_storage')->getToken();
        if (!$token) return $this->handleView(new View("Unauthorized", Response::HTTP_UNAUTHORIZED));
        $user = $token->getUser();
        if (!$user || !is_a($user, User::class)) return $this->handleView(new View("Unauthorized", Response::HTTP_UNAUTHORIZED));
        return $this->handleView($this->view($user, 200));
    }

    function postAction(Request $request) {
        $data = json_decode($request->getContent(), true);
        $name = key_exists("username", $data) ? $data['username'] : null;
        $email = key_exists("email", $data) ? $data['email'] : null;
        $password = key_exists("password", $data) ? $data['password'] : null;
        if (!$name || !$email || !$password || strlen($password) < 6 || !$this->isValidEmail($email)/* ... validate email and name and password*/) {
            // bad request
            return $this->handleView(new View("Bad request - one of required parameters missing", Response::HTTP_NOT_ACCEPTABLE));
        }
        $repo = $this->getDoctrine()->getRepository($this->getEntityName());
        if ($repo->findOneByEmail($email)) {
            return $this->handleView(new View("Email already exists", Response::HTTP_NOT_ACCEPTABLE));
        }
        if ($repo->findOneByUsername($email)) {
            return $this->handleView(new View("Username already exists", Response::HTTP_NOT_ACCEPTABLE));
        }
        $user = new User();
        $user->setEmail($email)->setUsername($name)->setPassword($this->get("security.password_encoder")->encodePassword($user, $password));
        $repo->create($user);
        return $this->handleView(new View("User created Successfully", 200));
    }

    public function getEntityName(): string {
        return 'App:User';
    }


    function isValidEmail($email){
        return filter_var($email, FILTER_VALIDATE_EMAIL) !== false;
    }
}