<?php
/**
 * Created by PhpStorm.
 * painting: twisty01
 * Date: 13-Apr-18
 * Time: 10:54
 */

namespace App\Controller;

use App\Controller\Traits\GetEntityNameInterface;
use App\Controller\Traits\GetPostsActionTrait;
use App\Entity\Author;
use App\Entity\Painting;
use App\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PaintingsController extends FOSRestController implements ClassResourceInterface, GetEntityNameInterface {

    use GetPostsActionTrait;

    public function getAction($id) {
        $painting = $this->getDoctrine()->getRepository($this->getEntityName())->read($id);
        return $this->handleView($view = $this->view($painting, 200));
    }

    public function cgetAction() {
        // todo params
        $paintings = $this->getDoctrine()->getRepository($this->getEntityName())->findBy(['active' => true], []);
        return $this->handleView($view = $this->view($paintings, 200));
    }

    public function getTagsAction($id) {
        $entity = $this->getDoctrine()->getRepository($this->getEntityName())->read($id);
        return $this->handleView($view = $this->view($entity->getTags(), 200));
    }

//    /**
//     * @SWG\Post(description="Creates new Painting.")
//     * @SWG\Response(
//     *     response=200,
//     *     description="Returns Painting id, if the Painting was sucesfully created."
//     * )
//     */
    function cpostAction(Request $request) {
        $data = json_decode($request->getContent(), true);
        $lat = key_exists("lat", $data) ? $data["lat"] : null;
        $lng = key_exists("lng", $data) ? $data["lng"] : null;
        $desc = key_exists("text", $data) ? $desc = $data["desc"] : null;
        $name = key_exists("name", $data) ? $desc = $data["name"] : null;
        $perex = key_exists("perex", $data) ? $desc = $data["perex"] : null;
        $created = key_exists("painting_created", $data) ? $desc = $data["painting_created"] : null;
        $authorId = key_exists("author_id", $data) ? $desc = $data["author_id"] : null;
        $authorName = key_exists("author_name", $data) ? $desc = $data["author_name"] : null;
        $img = key_exists("img", $data) ? $data["img"] : null;

        if (!$lat || !$lng || !$img || !$name) return $this->handleView(new View("Bad request", Response::HTTP_NOT_ACCEPTABLE));

        $token = $this->container->get('security.token_storage')->getToken();
        $user = $token->getUser();
        if (!$user || !is_a($user, User::class) || !in_array('ROLE_ADMIN', $user->getRoles()) || !in_array('ROLE_SUPER_ADMIN', $user->getRoles()))
            return $this->handleView(new View("Unauthorized", Response::HTTP_UNAUTHORIZED));

        $now = new \DateTime();
        $painting = (new Painting())->setLng($lng)->setLat($lat)->setUser($token->getUser())->setName($name)->setPaintingCreated($created)->setDescription($desc)->setPerex($perex);
        $imgId = "paintings/" . $painting->getId() . "p" . self::slugify($name) . "-u" . $user->getId() . "-d" . $now->format("Y-m-d");
        $response = $this->get('speicher210_cloudinary.uploader')->upload($img, [
            "public_id" => $imgId,
            "eager" => [
                ["width" => 2000],
                ["width" => 400]
            ]
        ]);
        $pubId = $response["public_id"];
        $eager = $response["eager"];
        if (!$eager || !$pubId) return $this->handleView(new View("Failed to upload image", Response::HTTP_NOT_ACCEPTABLE));
        $painting->setImgPublicId($pubId);
        $painting->setImg($eager[0]["url"]);
        $painting->setTn($eager[1]["url"]);
        $author = null;
        if ($authorId) {
            $author = $this->getDoctrine()->getRepository('App:Author')->find($authorId);
        }
        if (!$author && $authorName) {
            $author = (new Author())->setName($authorName);
        }
        $painting->setAuthor($author);
        $this->getDoctrine()->getRepository("App:Post")->create($painting);
        return $this->handleView(new View($painting->getId(), 200));
    }

//    public function postAction($id){
//        return $this->handleView(new View("User Updated Successfully", 200));
//    }
//    public function cpostAction(){
//        return $this->handleView(new View("User Updated Successfully", 200));
//    }

    public function getEntityName(): string {
        return 'App:Painting';
    }

    public static function slugify($string) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $string);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        // trim
        $text = trim($text, '-');
        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);
        $text = strtolower($text);
        if (empty($text)) return 'n-a';
        return $text;
    }
}