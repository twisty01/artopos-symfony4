<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 11-Apr-18
 * Time: 15:01
 */

namespace App\Repository;


use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait CRUD {

    function create($e) {
        if ( method_exists($e,'setCreated')){
            $e->setCreated(new \DateTime());
        }
        if ( method_exists($e,'setUpdated')){
            $e->setUpdated(new \DateTime());
        }
        $this->_em->persist($e);
        $this->_em->flush($e);
    }

    function read($id){
        $e = $this->find($id);
        if ( !$e) throw new NotFoundHttpException("Entity not found");
        return $e;
    }

    function update($e) {
        if ( method_exists($e,'setUpdated')){
            $e->setUpdated(new \DateTime());
        }
        $this->_em->flush($e);
    }

    function delete($e) {
        $this->_em->remove($e);
        $this->_em->flush($e);
    }
}