<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 13-Apr-18
 * Time: 11:45
 */

namespace App\Repository;

use App\Entity\Author;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class AuthorRepository extends ServiceEntityRepository {
    use CRUD;

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Author::class);
    }

}