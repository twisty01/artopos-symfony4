<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 14-Apr-18
 * Time: 14:22
 */

namespace App\Repository;


use App\Entity\Institution;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class InstitutionRepository extends ServiceEntityRepository {
    use CRUD;

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Institution::class);
    }

}
