<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 14-Apr-18
 * Time: 15:17
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

trait ApprovedTrait {

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @SWG\Property(description="Approved defines if the painting or post has been approved to be public for other users.")
     */
    private $approved;

    public function setApproved($approved) {
        $this->approved = $approved;
        return $this;
    }

    public function getApproved() {
        return $this->approved;
    }

}