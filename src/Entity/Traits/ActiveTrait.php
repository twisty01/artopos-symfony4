<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 03/11/2017
 * Time: 17:18
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

trait ActiveTrait {

    /**
     * @ORM\Column(type="boolean", length=100, nullable=true)
     * @JMS\Exclude
     */
    private $active;

    public function setActive($active) {
        $this->active = $active;
        return $this;
    }

    public function getActive() {
        return $this->active;
    }

}