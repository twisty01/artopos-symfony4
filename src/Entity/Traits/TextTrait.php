<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 04/11/2017
 * Time: 19:34
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait TextTrait {

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    public function getText() {
        return $this->text;
    }

    public function setText($text) {
        $this->text = $text;
        return $this;
    }

}