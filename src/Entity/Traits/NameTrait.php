<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 03/11/2017
 * Time: 17:18
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

trait NameTrait {

    /**
     * @ORM\Column(type="string", length=100)
     * @SWG\Property(type="string", maxLength=100)
     */
    private $name;

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

}