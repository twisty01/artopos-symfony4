<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 04/11/2017
 * Time: 19:34
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait DescriptionTrait {

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

}