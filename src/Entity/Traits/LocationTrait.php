<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 03/11/2017
 * Time: 17:15
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

trait LocationTrait {

    /**
     * @ORM\Column(type="float", nullable=true)
     * @SWG\Property(description="Defines latitude of resource location")
     */
    private $lat;
    /**
     * @ORM\Column(type="float", nullable=true)
     * @SWG\Property(description="Defines longitude of resource location")
     */
    private $lng;

    public function getLat() {
        return $this->lat;
    }

    public function setLat($lat) {
        $this->lat = $lat;
        return $this;
    }

    public function getLng() {
        return $this->lng;
    }

    public function setLng($lng) {
        $this->lng = $lng;
        return $this;
    }

}