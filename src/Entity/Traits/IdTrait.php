<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 03/11/2017
 * Time: 17:16
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

trait IdTrait {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @SWG\Property(description="The unique identifier of the entity.")
     */
    protected $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

}