<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 04/11/2017
 * Time: 19:32
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Swagger\Annotations as SWG;

trait ImgTrait {

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SWG\Property(description="Defines full path to image resource with max. width 2000px.")
     */
    private $img;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @SWG\Property(description="Defines full path to thumbnail image resource with max. width 400px.")
     */
    private $tn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @JMS\Exclude
     */
    private $imgPublicId;

    public function getImg() {
        return $this->img;
    }

    public function setImg($img) {
        $this->img = $img;
        return $this;
    }

    public function getImgPublicId() {
        return $this->imgPublicId;
    }

    public function setImgPublicId($imgPublicId) {
        $this->imgPublicId = $imgPublicId;
        return $this;
    }

    public function getTn() {
        return $this->tn;
    }

    public function setTn($tn){
        $this->tn = $tn;
        return $this;
    }

}