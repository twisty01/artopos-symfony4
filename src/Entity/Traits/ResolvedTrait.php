<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 14-Apr-18
 * Time: 15:18
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations as SWG;

trait ResolvedTrait {

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @SWG\Property(description="Resolved defines if the painting or post has been resolved (their location has been approved and confirmed).")
     */
    private $resolved;

    public function setResolved($resolved) {
        $this->resolved = $resolved;
        return $this;
    }

    public function getResolved() {
        return $this->resolved;
    }
}