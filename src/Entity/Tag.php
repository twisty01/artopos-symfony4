<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 15/10/2017
 * Time: 19:16
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\NameTrait;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 */
class Tag {
    use IdTrait;
    use NameTrait;

    /**
     * @var Collection<App\Entity\Painting>
     * @ORM\ManyToMany(targetEntity="App\Entity\Painting", inversedBy="tags")
     * @JMS\Exclude
     */
    private $paintings;

    //<editor-fold desc="getters and setters">

    public function __construct() {
        $this->paintings = new ArrayCollection();
        return $this;
    }

    public function addPainting(Painting $painting) {
        $this->paintings[] = $painting;
        return $this;
    }

    public function removePainting(Painting $painting) {
        $this->paintings->removeElement($painting);
        return $this;
    }

    public function getPaintings() {
        return $this->paintings;
    }
    //</editor-fold>
}
