<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 10-Apr-18
 * Time: 13:01
 */

namespace App\Entity\OAuth;


use App\Entity\Traits\IdTrait;
use FOS\OAuthServerBundle\Entity\Client as BaseClient;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Client extends BaseClient {
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct() {
        parent::__construct();
        // your own logic
    }
}