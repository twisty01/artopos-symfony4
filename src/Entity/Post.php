<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 15/10/2017
 * Time: 14:45
 */

namespace App\Entity;

use App\Entity\Traits\TextTrait;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\ActiveTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\ImgTrait;
use App\Entity\Traits\LocationTrait;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostRepository")
 */
class Post {
    use IdTrait;
    use ImgTrait;
    use LocationTrait;
    use TextTrait;
    use ActiveTrait;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var Painting
     * @ORM\ManyToOne(targetEntity="App\Entity\Painting",inversedBy="posts")
     * @JMS\Exclude
     */
    private $painting;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $resolved;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $approved;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="posts")
     * @JMS\Exclude
     */
    private $user;

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("userId")
     */
    public function getUserId() {
        return  $this->user ? $this->user->getId() : null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("userName")
     */
    public function getUserName() {
        return  $this->user ? $this->user->getUsername() : null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("paintingId")
     */
    public function getPaintingId() {
        return  $this->painting ? $this->painting->getId() : null;
    }

    // -----------------------------------------

    //<editor-fold desc="getters and setters">

    public function __construct() {
    }

    public function setApproved($approved) {
        $this->approved = $approved;
        return $this;
    }

    public function getApproved() {
        return $this->approved;
    }

    public function setUser(User $user = null) {
        $this->user = $user;
        return $this;
    }

    public function getUser() {
        return $this->user;
    }

    public function getResolved() {
        return $this->resolved;
    }

    public function setResolved($resolved) {
        $this->resolved = $resolved;
        return $this;
    }

    public function getPainting(): Painting {
        return $this->painting;
    }

    public function setPainting(Painting $painting): Post {
        $this->painting = $painting;
        return $this;
    }

    public function getCreated() {
        return $this->created;
    }

    public function setCreated($created){
        $this->created = $created;
        return $this;
    }

    //</editor-fold>
}
