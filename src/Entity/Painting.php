<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 15/10/2017
 * Time: 14:45
 */

namespace App\Entity;

use App\Entity\Traits\ApprovedTrait;
use App\Entity\Traits\ResolvedTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\ActiveTrait;
use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\ImgTrait;
use App\Entity\Traits\LocationTrait;
use App\Entity\Traits\NameTrait;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaintingRepository")
 */
class Painting {
    use IdTrait, NameTrait, DescriptionTrait, ImgTrait, LocationTrait, ActiveTrait, ApprovedTrait, ResolvedTrait;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $perex;
    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $paintingCreated;

    /**
     * @var Author
     * @ORM\ManyToOne(targetEntity="App\Entity\Author", inversedBy="paintings",cascade={"persist"})
     * @JMS\Exclude
     */
    private $author;

    /**
     * @var Collection<App\Entity\Tag>
     * @ORM\ManyToMany(targetEntity="App\Entity\Tag", mappedBy="paintings", cascade={"persist"})
     * @JMS\Exclude
     */
    private $tags;

    /**
     * @var Collection<App\Entity\Post>
     * @ORM\OneToMany(targetEntity="App\Entity\Post", mappedBy="painting", cascade={"all"})
     * @JMS\Exclude
     */
    private $posts;

    /**
     * @var Institution
     * @ORM\ManyToOne(targetEntity="App\Entity\Institution", inversedBy="paintings")
     * @JMS\Exclude
     */
    private $institution;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="paintings")
     * @JMS\Exclude
     */
    private $user;

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("institutionLink")
     */
    public function getInstitutionLink() {
        return  $this->institution ? $this->institution->getLink() : null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("institutionName")
     */
    public function getInstitutionName() {
        return  $this->institution ? $this->institution->getName() : null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("authorId")
     */
    public function getAuthorId() {
        return  $this->author ? $this->author->getId() : null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("authorName")
     */
    public function getAuthorName() {
        return  $this->author ? $this->author->getName() : null;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("userId")
     */
    public function getUserId() {
        return  $this->user ? $this->user->getId() : null;
    }

    //<editor-fold desc="getters and setters">

    public function __construct() {
        $this->tags = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    public function getPerex() {
        return $this->perex;
    }

    public function setPerex($perex) {
        $this->perex = $perex;
        return $this;
    }

    public function getPaintingCreated() {
        return $this->paintingCreated;
    }

    public function setPaintingCreated($paintingCreated) {
        $this->paintingCreated = $paintingCreated;
        return $this;
    }

    public function getAuthor() {
        return $this->author;
    }

    public function addTag(Tag $tag) {
        $this->tags[] = $tag;
        return $this;
    }

    public function removeTag(Tag $tag) {
        $this->tags->removeElement($tag);
        return $this;
    }

    public function getTags() {
        return $this->tags;
    }

    public function addPost(Post $post) {
        $this->posts[] = $post;
        return $this;
    }

    public function removePost(Post $post) {
        $this->posts->removeElement($post);
        return $this;
    }

    public function getPosts() {
        return $this->posts;
    }


    public function getInstitution() {
        return $this->institution;
    }

    public function setInstitution(Institution $institution = null) {
        $this->institution = $institution;
        return $this;
    }

    public function getUser() {
        return $this->user;
    }

    public function setUser(User $user = null) {
        $this->user = $user;
        return $this;
    }

    public function setAuthor(Author $author): Painting {
        $this->author = $author;
        return $this;
    }

    //</editor-fold>
}
