<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 22/10/2017
 * Time: 12:18
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\NameTrait;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InstitutionRepository")
 */
class Institution {
    use IdTrait;
    use NameTrait;
    use DescriptionTrait;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $link;

    /**
     * @var Collection<App\Entity\Painting>
     * @ORM\OneToMany(targetEntity="App\Entity\Painting", mappedBy="institution")
     * @JMS\Exclude
     */
    private $paintings;

    //<editor-fold desc="getters and setters">

    public function __construct() {
        $this->paintings = new ArrayCollection();
    }

    public function getLink() {
        return $this->link;
    }

    public function setLink($link) {
        $this->link = $link;
        return $this;
    }

    public function addPainting(Painting $painting) {
        $this->paintings[] = $painting;
        return $this;
    }

    public function removePainting(Painting $painting) {
        $this->paintings->removeElement($painting);
        return $this;
    }

    public function getPaintings() {
        return $this->paintings;
    }

    //</editor-fold>

}
