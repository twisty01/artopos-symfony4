<?php

namespace App\Entity;

use App\Entity\Traits\IdTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable {
    use IdTrait;
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Exclude
     */
    private $salt;

    /**
     * @ORM\Column(type="string", length=255)
     * @JMS\Exclude
     */
    private $password;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var Collection<Painting>
     * @ORM\OneToMany(targetEntity="App\Entity\Painting",mappedBy="user")
     * @JMS\Exclude
     */
    private $paintings;

    /**
     * @var Collection<Post>
     * @ORM\OneToMany(targetEntity="App\Entity\Post",mappedBy="user")
     * @JMS\Exclude
     */
    private $posts;

    /**
     * @var string[]
     * @ORM\Column(type="simple_array")
     * @JMS\Exclude
     */
    private $roles;

    public function __construct() {
        $this->isActive = true;
        $this->salt = md5(uniqid(null, true));
        $this->posts = new ArrayCollection();
        $this->paintings = new ArrayCollection();
        $this->roles = ['ROLE_USER'];
    }

    /**
     * @inheritDoc
     */
    public function getUsername() {
        return $this->username;
    }

    public function getisActive() {
        return $this->isActive;
    }

    public function getPaintings(): Collection {
        return $this->paintings;
    }

    public function getPosts(): Collection {
        return $this->posts;
    }

    public function setIsActive($isActive): void {
        $this->isActive = $isActive;
    }

    public function setUsername($username) {
        $this->username = $username;
        return $this;
    }

    public function getSalt() {
        return $this->salt;
    }

    public function setSalt($salt) {
        $this->salt = $salt;
        return $this;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    public function getRoles() {
        $this->roles[] = 'ROLE_USER';
        return array_unique($this->roles);
    }

    public function eraseCredentials() {
    }

    /**
     * @see \Serializable::serialize()
     */
    public function serialize() {
        return serialize(
            array(
                $this->id,
            )
        );
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized) {
        list ($this->id) = unserialize($serialized);
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function setRoles(array $roles) {
        $this->roles = $roles;
        return $this;
    }

    public function setPaintings(Collection $paintings): User {
        $this->paintings = $paintings;
        return $this;
    }

    public function setPosts(Collection $posts): User {
        $this->posts = $posts;
        return $this;
    }

    public function addPost(Post $post): User {
        $this->posts->add($post);
        return $this;
    }

    public function removePost(Post $post): User {
        $this->posts->removeElement($post);
        return $this;
    }

    public function addPainting(Painting $painting): User {
        $this->paintings->add($painting);
        return $this;
    }

    public function removePainting(Painting $painting): User {
        $this->paintings->removeElement($painting);
        return $this;
    }

}