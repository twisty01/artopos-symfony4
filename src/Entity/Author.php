<?php
/**
 * Created by PhpStorm.
 * User: fisera
 * Date: 15/10/2017
 * Time: 19:13
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\DescriptionTrait;
use App\Entity\Traits\IdTrait;
use App\Entity\Traits\ImgTrait;
use App\Entity\Traits\NameTrait;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AuthorRepository")
 */
class Author {
    use IdTrait;
    use NameTrait;
    use DescriptionTrait;
    use ImgTrait;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $born;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $died;

    /**
     * @var Collection<App\Entity\Painting>
     * @ORM\OneToMany(targetEntity="App\Entity\Painting", mappedBy="author")
     * @JMS\Exclude
     */
    private $paintings;

    //<editor-fold desc="getters and setters">

    public function __construct() {
        $this->paintings = new ArrayCollection();
    }

    public function getBorn() {
        return $this->born;
    }

    public function setBorn($born) {
        $this->born = $born;
        return $this;
    }

    public function getDied() {
        return $this->died;
    }


    public function setDied($died) {
        $this->died = $died;
        return $this;
    }

    public function addPainting(Painting $painting) {
        $this->paintings[] = $painting;
        return $this;
    }

    public function removePainting(Painting $painting) {
        $this->paintings->removeElement($painting);
        return $this;
    }

    /**
     * Get paintings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaintings() {
        return $this->paintings;
    }

    //</editor-fold>
}
