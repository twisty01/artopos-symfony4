<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 11-Apr-18
 * Time: 14:54
 */

namespace App\Command;


use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class CreateUserCommand extends ContainerAwareCommand {
    private $userRepository;
    private $encoder;

    public function __construct(UserRepository $userRepository, UserPasswordEncoderInterface $encoder) {
        parent::__construct();
        $this->userRepository = $userRepository;
        $this->encoder = $encoder;
    }

    protected function configure() {
        $this
            ->setName('app:user:create')
            ->setDescription('Creates a new User')
            ->addOption(
                'email',
                null,
                InputOption::VALUE_REQUIRED
            )
            ->addOption(
                'username',
                null,
                InputOption::VALUE_REQUIRED
            )
            ->addOption(
                'password',
                null,
                InputOption::VALUE_REQUIRED
            )
            ->addOption(
                'role',
                null,
                InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $u = new User();
        $u->setUsername($input->getOption('username'));
        $u->setEmail($input->getOption('email'));
        $u->setPassword($this->encoder->encodePassword($u, $input->getOption('password')));
        $u->setRoles($input->getOption('role'));
        $this->userRepository->create($u);
        $output->writeln("User created!");
        return 0;
    }

}