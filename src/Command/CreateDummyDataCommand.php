<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 11-Apr-18
 * Time: 14:54
 */

namespace App\Command;

use App\Entity\Author;
use App\Entity\Institution;
use App\Entity\Painting;
use App\Entity\Post;
use App\Entity\Tag;
use App\Repository\AuthorRepository;
use App\Repository\InstitutionRepository;
use App\Repository\PaintingRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CreateDummyDataCommand extends ContainerAwareCommand {
    private $authorRepository;
    private $paintingRepository;
    private $institutionRepository;
    private $tagRepository;
    private $postRepository;
    private $userRepository;
    private $assetManager;
    private $urlGenerator;

    public function __construct(
        PaintingRepository $paintingRepository,
        PostRepository $postRepository,
        TagRepository $tagRepository,
        InstitutionRepository $institutionRepository,
        AuthorRepository $authorRepository,
        UserRepository $userRepository,
        Packages $assetsManager,
        UrlGeneratorInterface $urlGenerator
    ) {
        parent::__construct();
        $this->paintingRepository = $paintingRepository;
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
        $this->tagRepository = $tagRepository;
        $this->institutionRepository = $institutionRepository;
        $this->authorRepository = $authorRepository;
        $this->assetManager = $assetsManager;
        $this->urlGenerator = $urlGenerator;
    }

    protected function configure() {
        $this->setName('app:create-dummy-data');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        // add institutions
        $institutions = [];
        foreach ($this->getPlaceholdersInstitutions() as $i) {
            $n = (new Institution())->setName($i);
            $this->institutionRepository->create($n);
            $institutions [] = $n;
        }
        // add tags
        $tagCount = rand(30, 50);
        for (; $tagCount > 0; $tagCount--) {
            $this->tagRepository->create((new Tag())->setName($this->randomReadableString(rand(3, 15))));
        }
        // add authors
//        $authors = [];
//        foreach ($this->getPlaceholdersAuthors() as $a) {
//            $n = (new Author())->setName($a);
//            $this->authorRepository->create($n);
//            $authors [] = $n;
//        }

        $tl = $this->getTopLeft();
        $br = $this->getBottomRight();
        $users = $this->userRepository->findAll();
        foreach ($this->getPaintings() as $pl) {
            if ($pl['status'] == 'Nalezeno' || $this->f_rand() < 0.2) {

                $a = $this->authorRepository->findOneBy(['name' => $pl['author']]);
                $painting = (new Painting())
                    ->setName($pl['title'])
                    ->setDescription($this->randomText(rand(6, 25)))
                    ->setPerex($pl['description'])
                    ->setLat($this->f_rand($br[0], $tl[0]))
                    ->setLng($this->f_rand($tl[1], $br[1]))
                    ->setTn("http://www.artopos.net" . $pl['img'])
                    ->setImg("http://www.artopos.net" . str_replace("thumbnail", "large_photo", ($pl['img'])))
                    ->setResolved($pl['status'] == 'Nalezeno')
                    ->setActive(true)
                    ->setApproved(true)
                    ->setPaintingCreated(DateTime::createFromFormat("d-m-Y", "" . rand(1, 30) . "-" . rand(1, 12) . "-" . rand(1000, 2000)))
                    ->setInstitution($institutions[rand(0, count($institutions) - 1)])
                    ->setAuthor($a ? $a : (new Author())->setName($pl['author']));
                $postCount = max(0, rand(0, 10));
                for ($i = 0; $i < $postCount; $i++) {
                    $img = "https://artopos.000webhostapp.com/img/placeholders/" . rand(1, 60) . ".jpg";
                    $painting->addPost((new Post())
                        ->setActive(true)
                        ->setPainting($painting)
                        ->setApproved(true)
                        ->setResolved(rand(0, 10) > 8 && $painting->getResolved())
                        ->setImg($img)
                        ->setTn($img)
                        ->setLat($painting->getLat() + rand(-700, 700) / 30000)
                        ->setLng($painting->getLng() + rand(-700, 700) / 30000)
                        ->setText($this->randomText(rand(6, 25)))
                        ->setUser($users[rand(0, count($users) - 1)]));
                }
                $this->paintingRepository->create($painting);
            }
        }
    }

    function f_rand($min = 0, $max = 1, $mul = 10000000) {
        if ($min > $max) return false;
        return mt_rand($min * $mul, $max * $mul) / $mul;
    }

    function randomText($wordsCount) {
        $str = "";
        for ($i = 1; $i <= $wordsCount; $i++) {
            $str .= $this->randomReadableString(rand(3, 13)) . (rand(0, 100) > 80 ? ". " : " ");
        }
        return $str;
    }

    function randomReadableString($length = 20) {
        $string = '';
        $vowels = ["a", "e", "i", "o", "u"];
        $consonants = [
            'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm',
            'n', 'p', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'
        ];
        // Seed it
        srand((double)microtime() * 1000000);
        $max = $length / 2;
        for ($i = 1; $i <= $max; $i++) {
            $string .= $consonants[rand(0, 19)];
            $string .= $vowels[rand(0, 4)];
        }
        return $string;
    }

    private function getPlaceholdersInstitutions() {
        return ["Galerie výtvarného umění v Chebu", "Muzeum Cheb", "Muzeum Šumavy Sušice", "Galerie Středočeského kraje Kutná Hora", "Pro arte", "Alšova jihočeská galerie v Hluboké nad Vltavou", "Památník Mladovožicka, Mladá Vožice", "Oblastní galerie v Liberci", "Galerie Zlatá husa, Praha", "Jihomoravské muzeum ve Znojmě", "Národní galerie v Praze", "Galerie hlavního města Prahy", "Nadační fond Eleutheria", "Sbírka Patrika Šimona", "Galerie moderního umění v Roudnici n. L.", "Západočeská galerie v Plzni", "Galerie výtvarného umění v Ostravě", "Město Cheb", "Egerland-Museum Marktrewitz", "soukromá sbírka", "Kunstforum Ostdeutsche Galerie Regensburg", "Oblastní galerie Vysočiny v Jihlavě", "Galerie umění Karlovy Vary", "Moravská galerie v Brně", "Artotéka města Plzně", "Galerie Klatovy / Klenová", "Galerie České spořitelny"];
    }

    private function getTopLeft() { return [50.847211, 12.543774]; }

    private function getBottomRight() { return [47.841690, 19.356543]; }

    private function getPaintings() {
        return [
            [
                "link" => "/ubytovani/", "img" => "/templates/artopos/images/redac/thumbnail///1118_mikolas-ales-ubytovani.jpg",
                "author" => 'MIkoláš Aleš',
                "title" => 'Ubytování',
                "status" => 'Nalezeno',
                "description" => 'Obraz s motivem ze Suchdola, kde pobýval u svého mecenáše Alexandra Brandejse, zachycuje bránu statku v ulici Ke kozím hřbetům 3/4. Převzato z webu www.historiesuchdola.cz. ...',
            ],
            [
                "link" => "/chalupa/", "img" => "/templates/artopos/images/redac/thumbnail///1117_hudeceks.jpg",
                "author" => 'Antonín Hudeček',
                "title" => 'Chalupa',
                "status" => 'Nalezeno',
                "description" => 'Obraz, zachycující vesnické stavení, zřejmě obyčejnou stodolu, u vodního toku, se od roku 1964 nachází ve sbírkách chebské galerie. Není datován, ani opatřen přípisem s názvem, ...',

            ],
            [
                "link" => "/letni-odpoledne/", "img" => "/templates/artopos/images/redac/thumbnail///1116_hudecek.jpg",
                "author" => 'Antonín Hudeček',
                "title" => 'Letní odpoledne',
                "status" => 'Nalezeno',
                "description" => 'Obraz původně ze sbírky Karla Lejnara z Velimi u Kolína nabízela v roce 2011 aukční síň Dorotheum pod nepůvodním názvem Letní odpoledne a byl zakoupen do ...',

            ],
            [
                "link" => "/parnik-na-seine/", "img" => "/templates/artopos/images/redac/thumbnail///1115_cze_mg.jpg",
                "author" => 'Josef Šíma',
                "title" => 'Parník na Seině',
                "status" => 'Nalezeno',
                "description" => 'Obraz s tradičním názevm Parník na Seině ve skutečnosti zachycuje jeden z parníků Pražské paroplavební společnosti v typické červenobílé barevnosti. Podle paprsčitých výřezů a asymetricky umístěných ...',

            ],
            [
                "link" => "/nadrazi/", "img" => "/templates/artopos/images/redac/thumbnail///1114_20170725_142840.jpg",
                "author" => 'Bohumil Kubišta',
                "title" => 'Nádraží',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/dvorek-u-kamenice-nad-lipou/", "img" => "/templates/artopos/images/redac/thumbnail///1113_dvorek.jpg",
                "author" => 'Ladislav Pejchl',
                "title" => 'Dvorek u Kamenice nad Lipou',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/hury/", "img" => "/templates/artopos/images/redac/thumbnail///1111_hury.jpg",
                "author" => 'Čeněk Choděra',
                "title" => 'Hůry',
                "status" => 'Hledáme',
                "description" => 'Viz http://www.selskebaroko.cz/selskebaroko/hury/hury.htm .                        ...',

            ],
            [
                "link" => "/paloucek/", "img" => "/templates/artopos/images/redac/thumbnail///1106_110dada.jpg",
                "author" => 'Zdeněk Daněk',
                "title" => 'Palouček',
                "status" => 'Nalezeno',
                "description" => 'Zámecká obora v Cholticích  49.979563, 15.616099 Část obory je udržována jako zámecký park a část byla ponechána přes 80  let sama sobě ...',

            ],
            [
                "link" => "/bradlec-u-jicina/", "img" => "/templates/artopos/images/redac/thumbnail///1105_26099_1_tn3.jpg",
                "author" => 'V. Hanuš',
                "title" => 'Bradlec u Jičína',
                "status" => 'Hledáme',
                "description" => 'Na severovýchodě jičínské kotliny nad obcí Bradlecká Lhota se tyčí zalesněný vrch s pozůstatky zříceniny hradu, od něhož získala obec jméno, Bradlec. Stavitel si mistrně vybral ...',

            ],
            [
                "link" => "/ponte-di-san-francesco-v-subiacu/", "img" => "/templates/artopos/images/redac/thumbnail///1104_206022.jpg",
                "author" => 'František Viktor Mokrý',
                "title" => 'Ponte di San Francesco v Subiacu',
                "status" => 'Nalezeno',
                "description" => 'Přestože vzadu je autorský přípis "Ponte di San Mauro", ve skutečnosti jde o středověký most svatého Františka z roku 1356 v Subiacu. (Autora asi zmátlo, že ...',

            ],
            [
                "link" => "/z-libne-1839/", "img" => "/templates/artopos/images/redac/thumbnail///1103_219700.jpg",
                "author" => 'Jaroslav Kotas',
                "title" => 'Z Libně',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/karlovy-vary-1838/", "img" => "/templates/artopos/images/redac/thumbnail///1102_liesler.jpg",
                "author" => 'Josef Liesler',
                "title" => 'Karlovy Vary',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/blatska-naves-zliv/", "img" => "/templates/artopos/images/redac/thumbnail///1101_moravec.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Blatská náves (Zliv)',
                "status" => 'Hledáme',
                "description" => '"Zliv bývala blatská ves, menší než Pištín, ale pak se stala městem. Hlavně díky továrně na šamotové zboží a díky železniční trati se rozrostla a z ...',

            ],
            [
                "link" => "/kuchelnik/", "img" => "/templates/artopos/images/redac/thumbnail///1100_217015.jpg",
                "author" => 'Bedřich Černý',
                "title" => 'Kuchelník',
                "status" => 'Hledáme',
                "description" => 'Mlýn stojí pod jezem na pravém břehu Sázavy v místě, kde se do Sázavy vlévá malý potok. Ve mlýně byla restaurace a camp. Mlýn je v ...',

            ],
            [
                "link" => "/kostel-svate-kateriny/", "img" => "/templates/artopos/images/redac/thumbnail///1098_216315.jpg",
                "author" => 'Václav Bartovský',
                "title" => 'Kostel svaté Kateřiny v Havlíčkově Brodě',
                "status" => 'Nalezeno',
                "description" => 'Kostelík sv. Kateřiny v Havlíčkově Brodě postavený za hradbami města u tehdy jediného mostu přes Sázavu.           ...',

            ],
            [
                "link" => "/krajina-u-spytihnevi/", "img" => "/templates/artopos/images/redac/thumbnail///1097_216270.jpg",
                "author" => 'Karel Hofman',
                "title" => 'Krajina u Spytihněvi',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/u-bubovickeho-rybnika/", "img" => "/templates/artopos/images/redac/thumbnail///1096_216316.jpg",
                "author" => 'Bohumil Ullrych',
                "title" => 'U Bubovického rybníka',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ze-smichovskeho-pristavu/", "img" => "/templates/artopos/images/redac/thumbnail///1095_wiso.jpg",
                "author" => 'Jaromír Wíšo',
                "title" => 'Ze smíchovského přístavu',
                "status" => 'Nalezeno',
                "description" => 'Pohled na bývalý smíchovský vorový přístav z Císařské louky. Vzadu s komínem je zlíchovský lihovar a dále Dívčí hrady.        ...',

            ],
            [
                "link" => "/plynarna/", "img" => "/templates/artopos/images/redac/thumbnail///1094_dilo_14598544184aaf85330901d.jpg",
                "author" => 'Jaroslav Pleskal',
                "title" => 'Plynárna',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/hustopece-zamek/", "img" => "/templates/artopos/images/redac/thumbnail///1093_cze_mg.jpg",
                "author" => 'Jan Konůpek',
                "title" => 'Hustopeče - zámek',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/stara-telc/", "img" => "/templates/artopos/images/redac/thumbnail///1092_cze_mg.jpg",
                "author" => 'Otakar Kubín (Othon Coubine)',
                "title" => 'Stará Telč',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/sedlecky-mlyn-u-mikulova/", "img" => "/templates/artopos/images/redac/thumbnail///1088_cze_mg.jpg",
                "author" => 'Karel Werner',
                "title" => 'Sedlecký mlýn u Mikulova',
                "status" => 'Nalezeno',
                "description" => 'Originální název díla: Die Lehmühle bei Nikolsburg. Zobrazná budova s polovalbou je podle všeho ta, co tu dodnes stojí. Situace na místě byla v té době však ...',

            ],
            [
                "link" => "/kunstat-hrncirna/", "img" => "/templates/artopos/images/redac/thumbnail///1087_cze_mg.jpg",
                "author" => 'Jánuš Kubíček',
                "title" => 'Kunštát (Hrnčírna)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ulicka-ze-stare-prahy/", "img" => "/templates/artopos/images/redac/thumbnail///1086_cze_mg.jpg",
                "author" => 'Antonín Slavíček',
                "title" => 'Ulička ze staré Prahy',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/v-zizkove/", "img" => "/templates/artopos/images/redac/thumbnail///1085_cze_mg.jpg",
                "author" => 'Jan Trampota',
                "title" => 'V Žižkově',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/krajina-od-jihlavy/", "img" => "/templates/artopos/images/redac/thumbnail///1083_cze_mg.jpg",
                "author" => 'Roman Havelka',
                "title" => 'Krajina  od Jihlavy (Památník královské přísahy)',
                "status" => 'Nalezeno',
                "description" => 'Renesanční památník, dílo kameníka Štěpána Tettelmayera, stojí na louce na břehu řeky Jihlavy, nedaleko Pražského mostu. Byl vybudován v místech, kde dne 30. ledna 1527 překročil ...',

            ],
            [
                "link" => "/smichov-ze-zlichova/", "img" => "/templates/artopos/images/redac/thumbnail///1082_cze_mg.jpg",
                "author" => 'Karel Malich',
                "title" => 'Smíchov ze Zlíchova',
                "status" => 'Nalezeno',
                "description" => 'Autor stál přímo u kostelíka sv. Filipa a Jakuba na Zlíchově (viz naznačená skála dole) nad železniční tratí směrem na Plzeň a výpadovkou na Strakonice. Komín ...',

            ],
            [
                "link" => "/jaro-v-holicich/", "img" => "/templates/artopos/images/redac/thumbnail///1080_cze_mg.jpg",
                "author" => 'Karel Malich',
                "title" => 'Jaro v Holicích',
                "status" => 'Nalezeno',
                "description" => 'Pohled směrem k výraznému terénnímu zlomu nad Holicemi,  který odděluje Orlickou tabuli (podhůří Orlických hor) od polabské roviny, v níž leží Holice. Malich, který zde žil, ...',

            ],
            [
                "link" => "/putim/", "img" => "/templates/artopos/images/redac/thumbnail///1079_cze_mg.jpg",
                "author" => 'Oldřich Koníček',
                "title" => 'Putim',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/mesto/", "img" => "/templates/artopos/images/redac/thumbnail///1078_6ub099090.jpg",
                "author" => 'Otakar Nejedlý',
                "title" => 'Město',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/holany/", "img" => "/templates/artopos/images/redac/thumbnail///1077_plekanec.jpg",
                "author" => 'Imrich Plekanec',
                "title" => 'Holany',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/tkalcovna-bratri-buxbaumu-v-upici/", "img" => "/templates/artopos/images/redac/thumbnail///1076_a-58334-1.jpg",
                "author" => 'Bedřich Mudroch',
                "title" => 'Tkalcovna bratří Buxbaumů v Úpici',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/jizera-u-mlade-bolselavi/", "img" => "/templates/artopos/images/redac/thumbnail///1075_12970-1.jpg",
                "author" => 'Bedřich Mudroch',
                "title" => 'Jizera u Mladé Boleslavi',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/jarosov/", "img" => "/templates/artopos/images/redac/thumbnail///1074_hroch.jpg",
                "author" => 'Vladimír Hroch',
                "title" => 'Jarošov',
                "status" => 'Hledáme',
                "description" => 'Jarošov je částí Uherského Hradiště, zde pohled přes řeku Moravu.                 ...',

            ],
            [
                "link" => "/most-v-ceskem-krumlove/", "img" => "/templates/artopos/images/redac/thumbnail///1073_211918.jpg",
                "author" => 'Otakar Nejedlý',
                "title" => 'Most v Českém Krumlově',
                "status" => 'Hledáme',
                "description" => 'Je otázkou, zda jde opravdu o Český Krumlov...                   ...',

            ],
            [
                "link" => "/pravoslavny-kostelik/", "img" => "/templates/artopos/images/redac/thumbnail///1072_svetislav-vukovic.jpg",
                "author" => 'Svetislav Vukovič',
                "title" => 'Pravoslavný kostelík',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/trh-ve-veseli-nad-mor-na-navsi/", "img" => "/templates/artopos/images/redac/thumbnail///1071_cze_mg.jpg",
                "author" => 'Ludvík Ehrenhaft',
                "title" => 'Trh ve Veselí nad Moravou (Na návsi)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zahrada-ve-svineticich/", "img" => "/templates/artopos/images/redac/thumbnail///1069_cze_mg.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Zahrada ve Sviněticích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/mlada-boleslav-1793/", "img" => "/templates/artopos/images/redac/thumbnail///1068_cze_mg.jpg",
                "author" => 'Jan Konůpek',
                "title" => 'Mladá Boleslav',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/podebrady-1791/", "img" => "/templates/artopos/images/redac/thumbnail///1066_cze_mg.jpg",
                "author" => 'Alfréd Justitz',
                "title" => 'Poděbrady',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/z-pocapel-na-pardubicku/", "img" => "/templates/artopos/images/redac/thumbnail///1065_grus-m-618-001-p1.jpg",
                "author" => 'Jaroslav Grus',
                "title" => 'Z Počápel na Pardubicku',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/jaro-v-bele/", "img" => "/templates/artopos/images/redac/thumbnail///1064_2190-bohumil-hradecny-jaro-v-bele-2190.jpg",
                "author" => 'Bohumil Hradečný',
                "title" => 'Jaro v Bělé',
                "status" => 'Hledáme',
                "description" => 'Pohled ke Kozákovu od Bělé.                      ...',

            ],
            [
                "link" => "/velhartice/", "img" => "/templates/artopos/images/redac/thumbnail///1063_velhartice.jpg",
                "author" => 'Bohumil Hradečný',
                "title" => 'Velhartice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/prerovska-ulicka/", "img" => "/templates/artopos/images/redac/thumbnail///1061_antonin-kubat-prerovska-ulicka_1469626731826.jpg",
                "author" => 'Antonín Kubát',
                "title" => 'Přerovská ulička',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/plzenske-sidliste/", "img" => "/templates/artopos/images/redac/thumbnail///1060_20160926_142545.jpg",
                "author" => 'Jindřich Krátký',
                "title" => 'Sídliště',
                "status" => 'Hledáme',
                "description" => 'Obraz, který visí ve studiovně Studijní a vědecké knihovny v Plzni, zobrazuje bytovkovu zástavbu nejspíš z 50. let. K plzeňskému uměleckému prostředí má malíř celoživotní vazby, ...',

            ],
            [
                "link" => "/nova-krizovatka-v-plzni/", "img" => "/templates/artopos/images/redac/thumbnail///1059_svatopluk_janke_nova_krizovatka_v_plzni_1984-1985_zapadoceska_galerie_v_plzni.jpg",
                "author" => 'Svatopluk Janke',
                "title" => 'Nová křižovatka v Plzni',
                "status" => 'Hledáme',
                "description" => 'Autor se po dlouhou dobu věnoval tématu plzeňských památek, z nichž mnohé opravoval i jako architekt. Zde si však jako motiv zvolil betonový most a právě ...',

            ],
            [
                "link" => "/u-krize/", "img" => "/templates/artopos/images/redac/thumbnail///1062_209182.jpg",
                "author" => 'Bořivoj Žufan',
                "title" => 'U kříže',
                "status" => 'Nalezeno',
                "description" => 'Pohled na dnešní restauraci "U Karla IV." v Praze - Libni poblíž křižovatky U Kříže. Pohled ze Zenklovy ulice, v pozadí usedlost Báň (v místech u ...',

            ],
            [
                "link" => "/tani-u-stepanovic/", "img" => "/templates/artopos/images/redac/thumbnail///1057_27616_1_tn3-jambor-tani-u-stepanovic.jpg",
                "author" => 'Josef Jambor',
                "title" => 'Tání u Štěpánovic',
                "status" => 'Hledáme',
                "description" => 'V roce 1931 se Josef Jambor trvale odstěhoval do Tišnova, kde si vzal za ženu Boženu Chlupovou. Štěpánovice leží nedaleko od Tišnova.     ...',

            ],
            [
                "link" => "/horni-sadova-trida-v-karlovych-varech/", "img" => "/templates/artopos/images/redac/thumbnail///1056_o-17164.jpg",
                "author" => 'Otakar Lebeda',
                "title" => 'Horní sadová třída v Karlových Varech',
                "status" => 'Nalezeno',
                "description" => 'Malíř zobrazil pohled směrem k dnes již neexistující synagoze v Sadové třídě (Parkstrasse) - ta se kdysi dělila na Horní (Obere), kde malíř stál, a Dolní ...',

            ],
            [
                "link" => "/z-karlovych-varu/", "img" => "/templates/artopos/images/redac/thumbnail///1055_karlstejnska.jpg",
                "author" => 'Otakar Lebeda',
                "title" => 'Z Karlových Varů',
                "status" => 'Nalezeno',
                "description" => 'Motiv z nejoblíbenější karlovarské promenády Stará louka (Alte Wiese), která byla centrem společenského života. Na obraze je patrné zatočení ulice doleva (směrem proti proudu Teplé, jejíž ...',

            ],
            [
                "link" => "/kostel-ve-francii-/", "img" => "/templates/artopos/images/redac/thumbnail///1054_206330.jpg",
                "author" => 'Friedrich Feigl',
                "title" => 'Kostel ve Francii (?)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/javornice-v-lete/", "img" => "/templates/artopos/images/redac/thumbnail///1053_206192.jpg",
                "author" => 'Vojtěch Sedláček',
                "title" => 'Javornice v létě',
                "status" => 'Hledáme',
                "description" => 'Motiv z Javornice, kde s rodinou pravidelně trávil dlouhé letní pobyty.                ...',

            ],
            [
                "link" => "/krajina-s-rekou/", "img" => "/templates/artopos/images/redac/thumbnail///1052_30s170311_83.jpg",
                "author" => 'Jiří Krejčí',
                "title" => 'Krajina s řekou',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-caslav-1769/", "img" => "/templates/artopos/images/redac/thumbnail///1051_screenshot-2017-03-12-11.jpg",
                "author" => 'František Antonín Jelínek',
                "title" => 'Pohled na Čáslav',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/zimni-slunecko-nad-kostelcem-u-hermanova-mestce/", "img" => "/templates/artopos/images/redac/thumbnail///1050_screenshot-2017-03-09-20.jpg",
                "author" => 'Karel Jan Sigmund',
                "title" => 'Zimní slunéčko (Nad Kostelcem u Heřmanova Městce)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/krajina-se-vsi/", "img" => "/templates/artopos/images/redac/thumbnail///1049_screenshot-2017-03-09-20.jpg",
                "author" => 'Fína Maternová - Řivnáčová',
                "title" => 'Krajina se vsí',
                "status" => 'Hledáme',
                "description" => 'Pohled na neznámou vesnici.                       ...',

            ],
            [
                "link" => "/zimni-mestecko/", "img" => "/templates/artopos/images/redac/thumbnail///1048_6ub098220_r.jpg",
                "author" => 'neznámý autor',
                "title" => 'Zimní městečko',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/milcin/", "img" => "/templates/artopos/images/redac/thumbnail///1047_6ub098009.jpg",
                "author" => 'Josef Kočí',
                "title" => 'Milčín',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/briza-u-silerovy-hospody/", "img" => "/templates/artopos/images/redac/thumbnail///1046_27212_1_tn3.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Bříza u Šilerovy hospody',
                "status" => 'Nalezeno',
                "description" => 'Šilerův hostinec dodnes stojí a slouží svému účelu. Malíř zachytil pohled z hlavní silnice 9. května podél domu do volné krajiny. Také budova vzadu (stodola?) dodnes ...',

            ],
            [
                "link" => "/pohled-na-klatovy/", "img" => "/templates/artopos/images/redac/thumbnail///1045_maslyimg_1785.jpg",
                "author" => 'František Emler',
                "title" => 'Pohled na Klatovy',
                "status" => 'Nalezeno',
                "description" => 'Malíř si postavil stojan na hranu lomu, dnes zaplaveného, v lokalitě, které se dnes říká Lomeček. Lomy jsou zde dva, zde se jedná o Svatováclavský blíže ...',

            ],
            [
                "link" => "/pohled-na-kolin/", "img" => "/templates/artopos/images/redac/thumbnail///1044_img_1779.jpg",
                "author" => 'Bohuslav Kutil',
                "title" => 'Pohled na Kolín',
                "status" => 'Hledáme',
                "description" => 'Pohled na chrám sv. Bartoloměje v Kolíně.                    ...',

            ],
            [
                "link" => "/pod-kozakovem/", "img" => "/templates/artopos/images/redac/thumbnail///1043_oplt-m-2868-001-p1.jpg",
                "author" => 'Oldřich Oplt',
                "title" => 'Pod Kozákovem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/z-nemecke-rybne/", "img" => "/templates/artopos/images/redac/thumbnail///1042_slavicek-jan-m-504-001-p1-30380.jpg",
                "author" => 'Jan Slavíček',
                "title" => 'Z Německé Rybné',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/dvur-popluzi-u-helvikovic/", "img" => "/templates/artopos/images/redac/thumbnail///1041_61_52216.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Dvůr Popluží u Helvíkovic',
                "status" => 'Nalezeno',
                "description" => 'Statek dnes neexistuje a na jeho místě se nachází areál firmy Dibaq, a.s.              ...',

            ],
            [
                "link" => "/vesnice-s-kostelem/", "img" => "/templates/artopos/images/redac/thumbnail///1038_i2-gnqcv_uuco8ghk7q_vd4sjtugkm.jpg",
                "author" => 'Jan Jůzl',
                "title" => 'Kostel svatého Prokopa ve Žďáru nad Sázavou',
                "status" => 'Nalezeno',
                "description" => 'Pohled na kosel svatého Prokopa, před ním Moučkův dům, kde se dnes nachází expozice zdejšího Regionálního muzea, vpravo fara.        ...',

            ],
            [
                "link" => "/karlovy-vary-1749/", "img" => "/templates/artopos/images/redac/thumbnail///1037_i2-gg9w4_smclmih9k_q9jrxvbrou.jpg",
                "author" => 'Jaroslav Šetelík',
                "title" => 'Karlovy Vary',
                "status" => 'Nalezeno',
                "description" => 'Malíř zde zobrazil Zámeckou věž, poslední pozůstatek malého gotického hrádku, který nechal vystavět císař Karel IV. Během velkého požáru města v roce 1604 vyhořel a roku ...',

            ],
            [
                "link" => "/korunni-ulice-v-praze/", "img" => "/templates/artopos/images/redac/thumbnail///1036_i2-kfupz_2rd9e6hxje_xi5ealbfcf.jpg",
                "author" => 'Jaromír Kunc',
                "title" => 'Korunní ulice v Praze',
                "status" => 'Nalezeno',
                "description" => 'Přestože se obraz objevil na aukčním trhu pod názvem Vinohradská ulice, ve skutečnosti jde o ulici Korunní. Vlevo je vidět vinohradská vodárna. Jde o pozdní dílo ...',

            ],
            [
                "link" => "/cesta-podel-skaly-u-okore/", "img" => "/templates/artopos/images/redac/thumbnail///1035_i2-8p7lv_gbmdwlob2b_b3gecgsuxy.jpg",
                "author" => 'Jaroslav Zdrůbecký',
                "title" => 'Cesta podél skály u Okoře',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/chebska-ulicka/", "img" => "/templates/artopos/images/redac/thumbnail///1034_i2-qhgde_2lmqs8u4kb_dijmkkgby6.jpg",
                "author" => 'Bojmír Hutta',
                "title" => 'Chebská ulička',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/periferie/", "img" => "/templates/artopos/images/redac/thumbnail///1033_olej-platno-1939-70-x-90-cm-sign.jpg",
                "author" => 'Bedřich Mudroch',
                "title" => 'Periferie',
                "status" => 'Hledáme',
                "description" => 'Periferie Prahy (?) s pohledem do volné krajiny a Řípem na obzoru.               ...',

            ],
            [
                "link" => "/dub-v-lanech/", "img" => "/templates/artopos/images/redac/thumbnail///1031_o_156.jpg",
                "author" => 'Otakar Nejedlý',
                "title" => 'Dub v Lánech',
                "status" => 'Nalezeno',
                "description" => 'Informaci o stromu z obrazu Otakara Nejedlého ze sbírek chebské GAVU poskytl Jaroslav Michálek z Muzea Sokolov, který se starými stromy systematicky zabývá a před několika ...',

            ],
            [
                "link" => "/park-v-roznove/", "img" => "/templates/artopos/images/redac/thumbnail///1028_194908.jpg",
                "author" => 'Bohumír Jaroněk',
                "title" => 'Park v Rožnově',
                "status" => 'Hledáme',
                "description" => 'Park s kostelem Všech svatých v pozadí.                    ...',

            ],
            [
                "link" => "/v-mostaru/", "img" => "/templates/artopos/images/redac/thumbnail///1026_194718.jpg",
                "author" => 'Ludvík Kuba',
                "title" => 'V Mostaru',
                "status" => 'Hledáme',
                "description" => 'Akvarel, který Kuba namaloval během ročního pobytu v Mostaru. Odjel sem po svatbě (1895) spolu se svou ženou Olgou, roz. Joujovou.      ...',

            ],
            [
                "link" => "/jenstejn/", "img" => "/templates/artopos/images/redac/thumbnail///1025_183232.jpg",
                "author" => 'Vladislav Panuška',
                "title" => 'Jenštejn',
                "status" => 'Nalezeno',
                "description" => 'Pohled na zříceninu hradu Jenštejn přes Dolení rybník.                   ...',

            ],
            [
                "link" => "/folimanka-pod-karlovem/", "img" => "/templates/artopos/images/redac/thumbnail///1024_111fol.jpg",
                "author" => 'Karel Holan',
                "title" => 'Folimanka pod Karlovem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zima-v-hlubocepich/", "img" => "/templates/artopos/images/redac/thumbnail///1023_178715.jpg",
                "author" => 'Josef Multrus',
                "title" => 'Zima v Hlubočepích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-vlakove-nadrazi-praha-smichov/", "img" => "/templates/artopos/images/redac/thumbnail///1022_199303.jpg",
                "author" => 'Josef Král',
                "title" => 'Pohled na vlakové nádraží Praha-Smíchov',
                "status" => 'Nalezeno',
                "description" => 'Pohled na severní nákladovou část nádraží Praha - Smíchov. Vpravo je dobře vidět oblouk jedné traťové koleje k železničnímu mostu přes Vltavu mezi Smíchovem a Podskalím. ...',

            ],
            [
                "link" => "/pristav-st-brieuc-v-bretani/", "img" => "/templates/artopos/images/redac/thumbnail///1021_199317.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Přístav St. Brieuc v Bretani',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/vesnice-v-orlickych-horach/", "img" => "/templates/artopos/images/redac/thumbnail///1020_199261.jpg",
                "author" => 'Jan Trampota',
                "title" => 'Vesnice v Orlických horách',
                "status" => 'Hledáme',
                "description" => 'Vesnice či městečko nejspíš z Orlických hor někde z okolí Slatiny nad Zdobnicí, kde v letech 1921 až 23 působil, možná Žamberk či Vamberk.... Datace je ...',

            ],
            [
                "link" => "/vesnice-s-nadrazim-a-kostelem/", "img" => "/templates/artopos/images/redac/thumbnail///1019_199267.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Vesnice s nádražím a kostelem',
                "status" => 'Hledáme',
                "description" => 'Název, pod kterým byl obraz v roce 2016 nabízen na aukčním trhu,  je nejspíš jen orientační a neodpovídá přípisu na zadní straně.     ...',

            ],
            [
                "link" => "/hradcansky-rybnik-u-volyne/", "img" => "/templates/artopos/images/redac/thumbnail///1018_bohac-maxmilian.jpg",
                "author" => 'Maxmilián Boháč',
                "title" => 'Hradčanský rybník u Volyně',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ve-volyni/", "img" => "/templates/artopos/images/redac/thumbnail///1017_168526.jpg",
                "author" => 'Josef Holub',
                "title" => 'Ve Volyni',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-mesto-slany-v-zime/", "img" => "/templates/artopos/images/redac/thumbnail///1016_199248.jpg",
                "author" => 'Josef Holub',
                "title" => 'Pohled na město Slaný v zimě',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ulicka-pod-hajem/", "img" => "/templates/artopos/images/redac/thumbnail///1015_196613.jpg",
                "author" => 'Josef Holub',
                "title" => 'Ulička Pod Hájem',
                "status" => 'Hledáme',
                "description" => 'Ulička v Kralupech nad Vltavou                      ...',

            ],
            [
                "link" => "/kystra/", "img" => "/templates/artopos/images/redac/thumbnail///1014_199122.jpg",
                "author" => 'Emil Filla',
                "title" => 'Kystra',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/u-vody/", "img" => "/templates/artopos/images/redac/thumbnail///1013_199103.jpg",
                "author" => 'Emil Artur Pittermann (Longen)',
                "title" => 'U vody',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/statek-v-cimicich/", "img" => "/templates/artopos/images/redac/thumbnail///1012_antonin_lhotak_-_statek_v_cimicich.jpg",
                "author" => 'Antonín Lhoták',
                "title" => 'Statek v Čimicích',
                "status" => 'Nalezeno',
                "description" => 'Sochař a akvarelista působící v Praze zde zachytil štít v duchu lidového baroka, dnes ovšem přestavěný, v Čimicích v Pošumaví; dodnes však zůstala zachována zeď s ...',

            ],
            [
                "link" => "/motiv-z-pisku/", "img" => "/templates/artopos/images/redac/thumbnail///1011_166751.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Motiv z Písku',
                "status" => 'Hledáme',
                "description" => 'Motiv s kostelem Nanebevzetí Panny Marie v pozadí.                   ...',

            ],
            [
                "link" => "/krajina-s-kostelem-1713/", "img" => "/templates/artopos/images/redac/thumbnail///1010_183093.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Kostel sv. Petra a Pavla na Budči',
                "status" => 'Hledáme',
                "description" => 'Malíř zde zobrazil kostel sv. Petra a Pavla na Budči, přesné místo je ještě třeba určit.           ...',

            ],
            [
                "link" => "/jihoceska-evsnice/", "img" => "/templates/artopos/images/redac/thumbnail///1009_191135.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Jihočeská vesnička',
                "status" => 'Hledáme',
                "description" => 'Prodáváno na aukčním trhu pod názvem Jihočeská vesnička, ovšem nejspíš není potvrzené, že opravdu jde o jižní Čechy.         ...',

            ],
            [
                "link" => "/zimni-cesta/", "img" => "/templates/artopos/images/redac/thumbnail///1007_a-75364-1.jpg",
                "author" => 'Josef Krejsa',
                "title" => 'Zimní cesta',
                "status" => 'Hledáme',
                "description" => 'Podle špatně čitelné signatury by mělo jít o dílo Josefa Krejsy. Pak by nejspíš šlo o motiv z okolí Husince.       ...',

            ],
            [
                "link" => "/baska/", "img" => "/templates/artopos/images/redac/thumbnail///1006_2013-josef-soukup-baska-2013.jpg",
                "author" => 'Josef Soukup',
                "title" => 'Baška',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/chaloupky-u-zamach/", "img" => "/templates/artopos/images/redac/thumbnail///1005_20160707_165105.jpg",
                "author" => 'Karel Boháček',
                "title" => 'Chaloupky u Zamach',
                "status" => 'Hledáme',
                "description" => 'Zvláštní skalní motiv rokle (či lomu?) z Boháčkovy rodné obce, která se dříve jmenovala Velké Zamachy, dnes jen Zamachy, osada Chaloupky leží nedaleko.    ...',

            ],
            [
                "link" => "/panorama-tabora-z-celakovic/", "img" => "/templates/artopos/images/redac/thumbnail///1004_050.jpg",
                "author" => 'František Sembdner',
                "title" => 'Panorama Tábora z Čelkovic',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/na-otave-pohled-od-zichovic-k-svatoboru/", "img" => "/templates/artopos/images/redac/thumbnail///1002_a-78064-1.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Na Otavě (Pohled od Žichovic k Svatoboru)',
                "status" => 'Nalezeno',
                "description" => 'Moravcovo plátno od Žichovic pod Rabím směrem na Čepice ukazuje, jak krajina kolem Otavy za 80 let zarostla divokou vegetací. Vrch zcela vzadu je skutečně Svatobor, ...',

            ],
            [
                "link" => "/tresnovka-nad-zoo-v-troji/", "img" => "/templates/artopos/images/redac/thumbnail///1001_wp_20160520_11_50_45_pro.jpg",
                "author" => 'Vlasta Fischerová - Vostřebalová',
                "title" => 'Třešňovka nad ZOO v Troji',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/krucemburk-1697/", "img" => "/templates/artopos/images/redac/thumbnail///1000_wp_20160520_11_42_34_pro.jpg",
                "author" => 'Jan Zrzavý',
                "title" => 'Krucemburk',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/tovarna-v-libni/", "img" => "/templates/artopos/images/redac/thumbnail///999_wp_20160520_11_45_05_pro.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Továrna v Libni',
                "status" => 'Nalezeno',
                "description" => 'Podle charakteristického tvaru komína, který by měl patři Zámeckému pivovaru, i celkové situace (vlevo dlouhé návrší, vpravo stromy při nábřeží) by mohlo jít o bývalou Budečskou ...',

            ],
            [
                "link" => "/periferie-prahy/", "img" => "/templates/artopos/images/redac/thumbnail///997_wp_20160520_11_44_56_pro.jpg",
                "author" => 'Karel Holan',
                "title" => 'Periferie Prahy',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/delnicke-domky-v-hlubokych-masuvkach/", "img" => "/templates/artopos/images/redac/thumbnail///996_stehlik.jpg",
                "author" => 'Antonín František Stehlík',
                "title" => 'Dělnické domky v Hlubokých Mašůvkách',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/cesta-ke-kazinu/", "img" => "/templates/artopos/images/redac/thumbnail///995_kazin.jpg",
                "author" => 'Jaroslav Otčenášek',
                "title" => 'Cesta ke Kazínu',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/cihelna-u-stozice/", "img" => "/templates/artopos/images/redac/thumbnail///994_vysocina-hlava-cihelna.jpg",
                "author" => 'Vratislav Hlava (Rudolf Bém)',
                "title" => 'Cihelna u Stožic',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/horazdovice-od-jarova/", "img" => "/templates/artopos/images/redac/thumbnail///993_a-11146-2.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Horažďovice od Jarova',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ulice-pod-skalou/", "img" => "/templates/artopos/images/redac/thumbnail///992_14769d5d-2b9b-4f1d-bcad-d72e4702a6e8.jpg",
                "author" => 'Bedřich Mudroch',
                "title" => 'Ulice pod skálou',
                "status" => 'Hledáme',
                "description" => 'Neznámé místo s výrazným motivem skály, díky níž by nemělo být příliš obtížné ho lokalizovat.            ...',

            ],
            [
                "link" => "/zluty-dum/", "img" => "/templates/artopos/images/redac/thumbnail///991_dilo-001-00002-1-v3.jpg",
                "author" => 'Vlasta J. Ambrožová',
                "title" => 'Žlutý dům',
                "status" => 'Nalezeno',
                "description" => 'Pohled na později zbořený blok domů v ulicích Pod Slovany (323, 324) a Na Moráni. Existuje kresba s naprosto stejným motivem a přípisem Na Moráni, ovšem ...',

            ],
            [
                "link" => "/lounky-u-roudnice/", "img" => "/templates/artopos/images/redac/thumbnail///990_picture-1.jpg",
                "author" => 'Vlasta J. Ambrožová',
                "title" => 'Lounky u Roudnice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/sidliste-v-podhradi/", "img" => "/templates/artopos/images/redac/thumbnail///989_40.jpg",
                "author" => 'Viliam Mousson',
                "title" => 'Sídliště v podhradí',
                "status" => 'Hledáme',
                "description" => 'Trenčínský hrad s nově budovaným sídlištěm.                     ...',

            ],
            [
                "link" => "/svitava-u-adamova/", "img" => "/templates/artopos/images/redac/thumbnail///988_493-antonin-majer-svitava-u-adamova.jpg",
                "author" => 'Antonín Majer',
                "title" => 'Svitava u Adamova',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/predmestska-krajina/", "img" => "/templates/artopos/images/redac/thumbnail///987_o_566.jpg",
                "author" => 'Karel Holan',
                "title" => 'Předměstská krajina',
                "status" => 'Hledáme',
                "description" => 'Neznámé místo, nejspíš někde v Praze.                     ...',

            ],
            [
                "link" => "/zimni-krajina-u-vitanova/", "img" => "/templates/artopos/images/redac/thumbnail///986_o_605.jpg",
                "author" => 'František Kaván',
                "title" => 'Zimní krajina u Vítanova',
                "status" => 'Hledáme',
                "description" => 'V roce 1904 se František Kaván přestěhoval do Vítanova nedaleko Hlinska, kde si pořídil vlastní domek. Ten o dva roky později prodal a odstěhoval se do ...',

            ],
            [
                "link" => "/beharov/", "img" => "/templates/artopos/images/redac/thumbnail///985_14243-kalvodaaloisbeharovolejlepenka25x35.jpg",
                "author" => 'Alois Kalvoda',
                "title" => 'Běhařov',
                "status" => 'Hledáme',
                "description" => 'Pozdní drobný olej Aloise Kalvody z doby, kdy vlastnil zámek Běhařov na Klatovsku.              ...',

            ],
            [
                "link" => "/bertramka/", "img" => "/templates/artopos/images/redac/thumbnail///983_6519_1_m.jpg",
                "author" => 'Josef Multrus',
                "title" => 'Bertramka',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/stare-skvrnany/", "img" => "/templates/artopos/images/redac/thumbnail///982_150.jpg",
                "author" => 'Jaroslav Bůžek',
                "title" => 'Staré Skvrňany',
                "status" => 'Nalezeno',
                "description" => 'Pohled na straré Skvrňany se Škodovkou v pozadí nedlouho před jejcih zničení nálety Američanů na konci 2. sv. války. Z celé obce dodnes přežila jen stará ...',

            ],
            [
                "link" => "/stare-mesto/", "img" => "/templates/artopos/images/redac/thumbnail///981_056.jpg",
                "author" => 'František Emler',
                "title" => 'Staré město',
                "status" => 'Hledáme',
                "description" => 'Pasáž není přímo na obraze blíže určena, nejspíš se jedná o okolí Anežského kláštera v Praze.           ...',

            ],
            [
                "link" => "/udoli-u-vimperka/", "img" => "/templates/artopos/images/redac/thumbnail///979_022a.jpg",
                "author" => 'František Xaver Böhm',
                "title" => 'Údolí u Vimperka',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/u-hotelu-richmond-v-karlovych-varech/", "img" => "/templates/artopos/images/redac/thumbnail///978_013.jpg",
                "author" => 'Josef Liesler',
                "title" => 'U hotelu Richmond v Karlových Varech',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/vesnicka-pod-kopcem-1661/", "img" => "/templates/artopos/images/redac/thumbnail///974_885-frantisek-michl-vesnicka-pod-kopcem.jpg",
                "author" => 'František Michl',
                "title" => 'Kolinec',
                "status" => 'Hledáme',
                "description" => 'Obraz lze jednoznačně určit podle charateristické věžě kostela sv. Jakuba většího. V Kolinci se Michl usadil v roce 1946, kdy zde koupil vilku. Obraz je však ...',

            ],
            [
                "link" => "/cermna/", "img" => "/templates/artopos/images/redac/thumbnail///972_2777-frantisek-michl-cermna-2777.jpg",
                "author" => 'František Michl',
                "title" => 'Čermná',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/hrdlorezy/", "img" => "/templates/artopos/images/redac/thumbnail///971_9116.jpg",
                "author" => 'Oldřich Koníček',
                "title" => 'Hrdlořezy',
                "status" => 'Hledáme',
                "description" => 'Pohled z Hrdlořez směrem k vrchu Tábor.                    ...',

            ],
            [
                "link" => "/cesta-do-vsi/", "img" => "/templates/artopos/images/redac/thumbnail///970_6252.jpg",
                "author" => 'Oldřich Koníček',
                "title" => 'Cesta do vsi',
                "status" => 'Hledáme',
                "description" => 'Stejný kostel se objevuje i na dalších Koníčkových obrazech, na jednom je ztotožněn se Sány u Žehuně, což bude pravděpodobně chybné.      ...',

            ],
            [
                "link" => "/uboc/", "img" => "/templates/artopos/images/redac/thumbnail///968_166873.jpg",
                "author" => 'František Michl',
                "title" => 'Úboč',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zamek-zichovice/", "img" => "/templates/artopos/images/redac/thumbnail///967_zichovice.jpg",
                "author" => 'Oldřich Koníček',
                "title" => 'Zámek Žichovice',
                "status" => 'Nalezeno',
                "description" => 'Pohled na zámek v Žichovicích od jihu. Vpředu a z levého boku jsou na zámku patrné dva přístavky zbořené kolem roku 1956 (vz foto). "Během podzimu ...',

            ],
            [
                "link" => "/pohled-na-zelezny-brod/", "img" => "/templates/artopos/images/redac/thumbnail///966_1957817296_aukce_1.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Pohled na Železný Brod',
                "status" => 'Nalezeno',
                "description" => 'Pohled na Železný Brod s Jizerou a výraznou dominantou radnice.                 ...',

            ],
            [
                "link" => "/zitko-u-vsi/", "img" => "/templates/artopos/images/redac/thumbnail///965_a-59169-1.jpg",
                "author" => 'Vratislav Hlava (Rudolf Bém)',
                "title" => 'Žitko u vsi (Stožice)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ve-stresovickach/", "img" => "/templates/artopos/images/redac/thumbnail///964_951-karel-holan-ulice-ve-stresovickach.jpg",
                "author" => 'Karel Holan',
                "title" => 'Ulice ve Střešovičkách',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/siesta-pod-vezi/", "img" => "/templates/artopos/images/redac/thumbnail///963_2617-jan-rambouek-siesta.jpg",
                "author" => 'Jan Rambousek',
                "title" => 'Siesta. Pod věží',
                "status" => 'Nalezeno',
                "description" => 'Motiv z Anežského kláštera z místa, kde původně stávala loď kostela sv. Františka. Dnes na jejím místě stojí novodobá dostavba.       ...',

            ],
            [
                "link" => "/doly/", "img" => "/templates/artopos/images/redac/thumbnail///962_celek.jpg",
                "author" => 'Josef (Joža) Bok',
                "title" => 'Doly',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/spicak/", "img" => "/templates/artopos/images/redac/thumbnail///961_celek-1.jpg",
                "author" => 'Josef (Joža) Bok',
                "title" => 'Špičák',
                "status" => 'Hledáme',
                "description" => 'Kamenolom na úbočí vrchu Špičák u Deštného, v současnosti již opuštěný.                ...',

            ],
            [
                "link" => "/lukavec-u-horic/", "img" => "/templates/artopos/images/redac/thumbnail///960_dvorak___lukavec.jpg",
                "author" => 'Bohuslav Dvořák',
                "title" => 'Lukavec u Hořic',
                "status" => 'Nalezeno',
                "description" => 'Malíř se díval z označeného místa západním směrem přes zatopenou louku. Jasným vodítkem je les po levé straně, který se nazývá Chlum. Celý Lukavec je lesy ...',

            ],
            [
                "link" => "/zima-v-rozne/", "img" => "/templates/artopos/images/redac/thumbnail///958_182894.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Zima v Rožné',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/krajina-s-domem/", "img" => "/templates/artopos/images/redac/thumbnail///957_182990.jpg",
                "author" => 'Oldřich Kerhart',
                "title" => 'Krajina s domem',
                "status" => 'Hledáme',
                "description" => 'Krajinný motiv je natolik výrazný, že by mělo být možné jej určit, přestože na samotném obraze není specifikován.         ...',

            ],
            [
                "link" => "/ricky/", "img" => "/templates/artopos/images/redac/thumbnail///956_182999.jpg",
                "author" => 'Alois Fišárek',
                "title" => 'Říčky',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-z-podlesi-ke-krizankam/", "img" => "/templates/artopos/images/redac/thumbnail///955_183978.jpg",
                "author" => 'Josef Jambor',
                "title" => 'Pohled  z Podlesí ke Křížánkám',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/bechynsky-zamek/", "img" => "/templates/artopos/images/redac/thumbnail///954_183981.jpg",
                "author" => 'František Holan',
                "title" => 'Bechyňský zámek',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/u-kostela-sv-anny/", "img" => "/templates/artopos/images/redac/thumbnail///953_183180.jpg",
                "author" => 'František Vavřina',
                "title" => 'U kostela sv. Anny',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ustek/", "img" => "/templates/artopos/images/redac/thumbnail///952_elvedere-ustek-orlik.jpg",
                "author" => 'Emil Orlik',
                "title" => 'Kostel v Úštěku',
                "status" => 'Nalezeno',
                "description" => 'Pohled na závěr barokního kostela svatého Petra a Pavla v Úštěku s pomníkem Josefa II. Vpravo hotel Herrenhaus (Panský dům).       ...',

            ],
            [
                "link" => "/klasterni-zahrada-v-tachove/", "img" => "/templates/artopos/images/redac/thumbnail///951_bottcher-tachau-belvedere.jpg",
                "author" => 'Rudolf Böttcher',
                "title" => 'Klášterní zahrada v Tachově',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/bastia-1624/", "img" => "/templates/artopos/images/redac/thumbnail///950_92189.jpg",
                "author" => 'Jiří Horník',
                "title" => 'Bastia',
                "status" => 'Hledáme',
                "description" => 'V letech 1947 - 48 studoval Horník spolu s dalšími mladými českými umělci (bratři Hejnové, Bojmír Hutta) v rámci stipendijního programu na pařížské akademii. Z tohoto ...',

            ],
            [
                "link" => "/krajina-s-kostelem1212/", "img" => "/templates/artopos/images/redac/thumbnail///949_ullrych-artplus.jpg",
                "author" => 'Bohumil Ullrych',
                "title" => 'Krajina s kostelem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/velky-a-maly-blanik-z-krekovicke-navsi/", "img" => "/templates/artopos/images/redac/thumbnail///948_177002.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Velký a malý Blaník z Křekovické návsi',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostel-sv-mikulase-se-zborenymi-kasarnami/", "img" => "/templates/artopos/images/redac/thumbnail///947_dsc_3331.jpg",
                "author" => 'Bojmír Hutta',
                "title" => 'Kostel sv. Mikuláše se zbořenými kasárnami',
                "status" => 'Nalezeno',
                "description" => 'Kasárna z roku 1839, která dala jméno Kasárnímu náměstí (Kasernplatz), dostala v závěru války zásah při náletu 20. dubna 1945, kterému padly za oběť i novogotické ...',

            ],
            [
                "link" => "/skolni-ulice-v-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///946_o_139.jpg",
                "author" => 'Jan Spáčil',
                "title" => 'Z Chebu (Školní ulice)',
                "status" => 'Nalezeno',
                "description" => 'Jeden z drobných Spáčilových obrázků starého Chebu zachycuje místo, které bylo zcela převrstveno novou zástavbou: nároží ulic Mincovní (Münzgasse) a Školní (Schulgasse) za dnešním radničním dvorem. ...',

            ],
            [
                "link" => "/ze-supetaru/", "img" => "/templates/artopos/images/redac/thumbnail///944_tmb2_galerie_img_1377.jpg",
                "author" => 'Václav Špála',
                "title" => 'Ze Supetaru',
                "status" => 'Hledáme',
                "description" => 'Supertar je centrum chorvatského ostrova Brać.                     ...',

            ],
            [
                "link" => "/prozareny-uvoz/", "img" => "/templates/artopos/images/redac/thumbnail///943_176092.jpg",
                "author" => 'Alois Kalvoda',
                "title" => 'Prozářený úvoz',
                "status" => 'Hledáme',
                "description" => 'Krajina z autorova moravského období.                      ...',

            ],
            [
                "link" => "/opravy-stareho-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///942_o_477.jpg",
                "author" => 'Jan Spáčil',
                "title" => 'Opravy starého Chebu',
                "status" => 'Nalezeno',
                "description" => 'Drobný olej na dřevě Jana Spáčila je svou malířskou bravurou asi nejpůsobivější ze série „studií na místě“, jak někdy psal na jejich zadní straně, v nichž ...',

            ],
            [
                "link" => "/v-zime-na-brance/", "img" => "/templates/artopos/images/redac/thumbnail///941_175120.jpg",
                "author" => 'Václav Rabas',
                "title" => 'V zimě na Brance',
                "status" => 'Hledáme',
                "description" => 'Ulice Na Brance se nachází v Rabasových Krušovicích.                   ...',

            ],
            [
                "link" => "/motiv-z-vysokeho-myta/", "img" => "/templates/artopos/images/redac/thumbnail///940_175197.jpg",
                "author" => 'Václav Prágr',
                "title" => 'Motiv z Vysokého Mýta',
                "status" => 'Hledáme',
                "description" => 'Autorem je místní malíř pokojů Václav Prágr. V aukci Dorothea dílo připsáno návrhářce modernistických hraček Mince (Vilemíně) Podhajské (!).        ...',

            ],
            [
                "link" => "/ze-stromovky/", "img" => "/templates/artopos/images/redac/thumbnail///939_175200.jpg",
                "author" => 'Kamil Lhoták',
                "title" => 'Ze Stromovky',
                "status" => 'Hledáme',
                "description" => 'Motiv se zajímavou fragmentárně zachycenou modernistickou (funkcionalistickou?) architekturou s výraznou markýzou.                ...',

            ],
            [
                "link" => "/jedova-chyse/", "img" => "/templates/artopos/images/redac/thumbnail///938_175489.jpg",
                "author" => 'Stanislav Feikl',
                "title" => 'Jedová chýše',
                "status" => 'Nalezeno',
                "description" => 'Oblíbený pražský motiv prostranství na křížení ulic Apolinářská a Viničná se svatovojtěšským sloupem - více u obrazu E. A. Longena na stejné téma, kde jsou též ...',

            ],
            [
                "link" => "/radimova/", "img" => "/templates/artopos/images/redac/thumbnail///935_168267.jpg",
                "author" => 'Karel Holan',
                "title" => 'Radimova',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/anger-v-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///934_o-4200.jpg",
                "author" => 'Eduard Pulz',
                "title" => 'Anger v Chebu',
                "status" => 'Nalezeno',
                "description" => 'Eduard Pulz působil v Chebu jako středoškolský profesor. Dnes jsou známy pouze tři jeho obrazy s chebskými motivy, ovšem hned dva z nich, kdysi zakoupené Moderní ...',

            ],
            [
                "link" => "/v-hajanech/", "img" => "/templates/artopos/images/redac/thumbnail///932_148836.jpg",
                "author" => 'Alois Moravec',
                "title" => 'V Hajanech',
                "status" => 'Nalezeno',
                "description" => 'Náves v Hajanech u Blatné s kaplí sv. Anny.                  ...',

            ],
            [
                "link" => "/u-chebskeho-hradu/", "img" => "/templates/artopos/images/redac/thumbnail///945_g_0716_kraft.jpg",
                "author" => 'Adam Kraft',
                "title" => 'U chebského hradu',
                "status" => 'Nalezeno',
                "description" => 'Domek v Dobrovského ulici (Rahmturmgasse) s Černou věží v pozadí patřil k dnes zbořenému Rejtarskému dvoru (Ausreiterhof) za ním. Do této podoby s výrazným hrázděním byl ...',

            ],
            [
                "link" => "/pohled-na-cheb-1599/", "img" => "/templates/artopos/images/redac/thumbnail///929_dsc_3563.jpg",
                "author" => 'Adam Kraft',
                "title" => 'Pohled na Cheb',
                "status" => 'Nalezeno',
                "description" => 'Pohled od řeky směrem ke kostelu sv. Mikuláše s hradem a zástavbou starého města patří k ikonickým pohledům na město, oblíbeným jak malíři, tak i fotografy. ...',

            ],
            [
                "link" => "/krajina-s-kostelikem-1597dd/", "img" => "/templates/artopos/images/redac/thumbnail///928_168553.jpg",
                "author" => 'Antonín Hudeček',
                "title" => 'Krajina s kostelíkem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/klasterni-ulicka-v-praze/", "img" => "/templates/artopos/images/redac/thumbnail///926_jan-bedrich-placek.jpg",
                "author" => 'Jan Bedřich Plaček',
                "title" => 'Klášterní ulička v Praze',
                "status" => 'Nalezeno',
                "description" => 'Klášterní ulička, blíže o motivu u obrazu na stejné téma od J. Matičky.              ...',

            ],
            [
                "link" => "/nove-dvory-1590/", "img" => "/templates/artopos/images/redac/thumbnail///924_168401.jpg",
                "author" => 'Miloš Jiránek',
                "title" => 'Nové Dvory',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostelni-schody-v-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///923_166626.jpg",
                "author" => 'Rudolf Herbert Schlindenbuch',
                "title" => 'Kostelní schody v Chebu',
                "status" => 'Nalezeno',
                "description" => 'Tento motiv se v leptu objevuje i v chebském albu R. H. Schlindenbucha, které vzniklo nejspíš také v roce 1923.
K vlastnímu schodišti: pod kostele sv. Mikuláše ...',

            ],
            [
                "link" => "/hospodarske-staveni/", "img" => "/templates/artopos/images/redac/thumbnail///922_166623.jpg",
                "author" => 'Josef Štolovský',
                "title" => 'Hospodářské stavení',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/motiv-z-hronova/", "img" => "/templates/artopos/images/redac/thumbnail///920_166562-1.jpg",
                "author" => 'Ladislav Langié',
                "title" => 'Motiv z Hronova',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/motiv-z-breznice/", "img" => "/templates/artopos/images/redac/thumbnail///919_166544-1.jpg",
                "author" => 'Ludvík Kuba',
                "title" => 'Motiv z Březnice',
                "status" => 'Nalezeno',
                "description" => 'Pohled na kněžiště kostela sv. Ignáce a svatého Františka Xaverského s mostem přes říčku Skalici.            ...',

            ],
            [
                "link" => "/stredovek-v-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///916_g_1479_schlindenbuch.jpg",
                "author" => 'Rudolf Herbert Schlindenbuch',
                "title" => 'Středověk v Chebu',
                "status" => 'Nalezeno',
                "description" => 'Tato grafika z chebského alba R. H. Schlinedbucha je téměř doslova (včetně názvu!) okopírovaná podle staršího leptu Fritze Pontiniho Kus středověku v Chebu. Tajemná a temná ...',

            ],
            [
                "link" => "/sankovani-na-janskem-namesti/", "img" => "/templates/artopos/images/redac/thumbnail///914_dsc_3186.jpg",
                "author" => 'Franz Dietl',
                "title" => 'Sáňkování na Jánském náměstí',
                "status" => 'Nalezeno',
                "description" => 'Tento pozoruhodný obraz dokládá, že Franz Dietl byl malířem na chebské poměry mimořádně progresivním, který během studií poznal vývoj moderního umění a dokázal jej reflektovat ve ...',

            ],
            [
                "link" => "/pohled-na-cheb/", "img" => "/templates/artopos/images/redac/thumbnail///913_dsc_3558.jpg",
                "author" => 'Hans Wohlrab',
                "title" => 'Pohled na Cheb',
                "status" => 'Nalezeno',
                "description" => 'Pohled od řeky směrem ke kostelu sv. Mikuláše s hradem a zástavbou starého města patří k ikonickým pohledům na město, oblíbeným jak malíři, tak i fotografy. ...',

            ],
            [
                "link" => "/pred-hradni-branou-cheb/", "img" => "/templates/artopos/images/redac/thumbnail///911_47_karasek_ws_15_2947-kopie.jpg",
                "author" => 'Rudolf Karasek',
                "title" => 'Před hradní branou (Cheb)',
                "status" => 'Nalezeno',
                "description" => 'Liberec byl mezi dvěma válkami vedle Prahy nejvýznamnějším entrem německé kultury v Čechách. V roce 1922 zde vznikla výtvarná skupina Oktobergruppe, jejímiž členy byli malíři Erwin ...',

            ],
            [
                "link" => "/mlynska-ulice-v-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///908_dsc_3478.jpg",
                "author" => 'Václav Kadlec',
                "title" => 'Mlýnská ulice v Chebu',
                "status" => 'Nalezeno',
                "description" => 'Václav Kadlec a Bojmír Hutta zde rok po sobě zachytili  chátrající domy v Mlýnské ulici (Mühlgasse) svažující se z Jánského náměstí (Johannisplatz) dolů ...',

            ],
            [
                "link" => "/slikova-ulice-v-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///907_pulec-1-kopie.jpg",
                "author" => 'František Pulec',
                "title" => 'Šlikova ulice v Chebu',
                "status" => 'Nalezeno',
                "description" => 'František Pulec je jako malíř zcela nevýznamný, nikoliv náhodou se později profiloval jako pouhý kopista starých mistrů. Tento raný obraz však má přece jen jisté ...',

            ],
            [
                "link" => "/za-horni-branou-v-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///906_o_3.jpg",
                "author" => 'Jan Spáčil',
                "title" => 'Za Horní branou v Chebu',
                "status" => 'Nalezeno',
                "description" => 'Drobný olej zachycuje místo za bývalou Horní branou, po jejímž zboření byla někdy v druhé půli 19. století postavena v ulici 9. května (Schmeykal strasse) ona zvláštní ...',

            ],
            [
                "link" => "/chebske-namesti/", "img" => "/templates/artopos/images/redac/thumbnail///904_007_hroch-kopie.jpg",
                "author" => 'Vladimír Hroch',
                "title" => 'Chebské náměstí',
                "status" => 'Nalezeno',
                "description" => 'Město netvoří jen domy a ulice, ale i jeho obyvatelé, kteří tuto hmotná strukturu vytvořili, využívají ji a přizpůsobují svým potřebám. Obě části se nedají ...',

            ],
            [
                "link" => "/pohled-ulici-elisky-krasnohorske-k-janskemu-namesti/", "img" => "/templates/artopos/images/redac/thumbnail///903_o_236_muzeum_cheb-2.jpg",
                "author" => 'Bojmír Hutta',
                "title" => 'Pohled ulicí Elišky Krásnohorské k Jánskému náměstí',
                "status" => 'Nalezeno',
                "description" => 'Pohled do ulice Elišky Krásnohorské, bývalé Vohburggasse, v roce 1953 ještě zachované, cca v roce 1961 zcela zdemolované v rámci asanace Chebu. Jak ukazuje doprovodné foto, ...',

            ],
            [
                "link" => "/kotel-se-zlatym-navrsim-od-mrklova/", "img" => "/templates/artopos/images/redac/thumbnail///902_kavan___kotel_se_zlatym_navrsim-1-marold.jpg",
                "author" => 'František Kaván',
                "title" => 'Kotel se Zlatým návrším od Mrklova',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/janske-namesti-v-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///901_002_hutta.jpg",
                "author" => 'Bojmír Hutta',
                "title" => 'Jánské náměstí v Chebu',
                "status" => 'Nalezeno',
                "description" => 'Horní strana Jánského náměstí - nejstaršího náměstí v Chebu, které bylo centrem původní kupecké osady - už po asanaci a nahrazení zbořených domů novostavbami, jako je ...',

            ],
            [
                "link" => "/dobrovskeho-ulice-v-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///900_001_kadlec.jpg",
                "author" => 'Václav Kadlec',
                "title" => 'Dobrovského ulice v Chebu',
                "status" => 'Nalezeno',
                "description" => 'První fáze demolice historického Chebu v Dobrovského, dříve Rahmturmgasse, hned po druhé světové válce. Zástavba v blízkosti hradu vzala pak téměř celá za své, včetně domů ...',

            ],
            [
                "link" => "/stara-praha/", "img" => "/templates/artopos/images/redac/thumbnail///899_162999.jpg",
                "author" => 'Jaromír Stretti-Zamponi',
                "title" => 'Stará Praha',
                "status" => 'Nalezeno',
                "description" => 'Dvorek na jižní straně Vlašské ulice, při nároží krátké slepé uličky směřující k jihu - dnes již zastavěný dvěma domy, jedním ze 30. let 20. stol. ...',

            ],
            [
                "link" => "/u-rezabince/", "img" => "/templates/artopos/images/redac/thumbnail///898_162406.jpg",
                "author" => 'Alois Moravec',
                "title" => 'U Řežabince',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/namesticko/", "img" => "/templates/artopos/images/redac/thumbnail///897_162014.jpg",
                "author" => 'Alois Kirnig',
                "title" => 'Náměstíčko',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/suchdol-nad-odrou/", "img" => "/templates/artopos/images/redac/thumbnail///896_162013.jpg",
                "author" => 'Hugo Baar',
                "title" => 'Suchdol nad Odrou',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/stare-domy-v-labske-tynici/", "img" => "/templates/artopos/images/redac/thumbnail///895_161944.jpg",
                "author" => 'Miloš Jiránek',
                "title" => 'Staré domy v Labské Týnici',
                "status" => 'Hledáme',
                "description" => 'Labská Týnice je starší název Týnce nad Labem.                   ...',

            ],
            [
                "link" => "/krajina-s-kostelikem-1553/", "img" => "/templates/artopos/images/redac/thumbnail///894_5441_1_m.jpg",
                "author" => 'Alois Kalvoda',
                "title" => 'Krajina s kostelíkem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/trh-na-navsi/", "img" => "/templates/artopos/images/redac/thumbnail///893_159319.jpg",
                "author" => 'František Mořic Nágl',
                "title" => 'Trh na návsi',
                "status" => 'Hledáme',
                "description" => 'Neznámé místo nejspíš někde v okolí Telče, kde žil.                  ...',

            ],
            [
                "link" => "/vesnicka-slavnost-na-chodsku-drazenov/", "img" => "/templates/artopos/images/redac/thumbnail///891_159287-1.jpg",
                "author" => 'Václav Malý',
                "title" => 'Vesnická slavnost na Chodsku (Draženov)',
                "status" => 'Nalezeno',
                "description" => 'Náves v Draženově s kapličkou Panny Marie.                    ...',

            ],
            [
                "link" => "/podvecer-na-vltave/", "img" => "/templates/artopos/images/redac/thumbnail///890_145682.jpg",
                "author" => 'Viktor Stretti',
                "title" => 'Podvečer na Vltavě',
                "status" => 'Nalezeno',
                "description" => 'Nábřeží u Šítkovské vodárenské věže s pískaři.                    ...',

            ],
            [
                "link" => "/kralupy-nad-vltavou/", "img" => "/templates/artopos/images/redac/thumbnail///889_screenshot-2015-04-29-12.jpg",
                "author" => 'Josef Holub',
                "title" => 'Kralupy nad Vltavou',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/restaurace-v-troji/", "img" => "/templates/artopos/images/redac/thumbnail///887_primaryemler.jpg",
                "author" => 'František Emler',
                "title" => 'Restaurace v Troji',
                "status" => 'Nalezeno',
                "description" => 'Přívoz mezi Císařským ostrovem a Trojou byl v provozu 1966, pak byl nahrazen pontonovým mostem, který je na obraze nejspíš zachycen. Zhruba stejný pohled zobrazili i ...',

            ],
            [
                "link" => "/mysi-dira/", "img" => "/templates/artopos/images/redac/thumbnail///886_primary-1-maticka-mysi-dira.jpg",
                "author" => 'Josef Matička',
                "title" => 'Myší díra',
                "status" => 'Nalezeno',
                "description" => 'Dnes neexistující slepá ulička (na jejím místě parkoviště), někdy ponechávaná bez názvu, jindy uváděná jako slepá odbočka Kozí ulice nebo jako Mauseloch Gasse - Myší díra. Géza ...',

            ],
            [
                "link" => "/usti-certovky/", "img" => "/templates/artopos/images/redac/thumbnail///885_primaryrychlovskyuusti.jpg",
                "author" => 'Antonín Rychlovský',
                "title" => 'Ústí Čertovky',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kvetouci-kastan/", "img" => "/templates/artopos/images/redac/thumbnail///884_primary-kvetouci-kastan.jpg",
                "author" => 'Josef Matička',
                "title" => 'Kvetoucí kaštan',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/u-obecniho-dvora/", "img" => "/templates/artopos/images/redac/thumbnail///883_primary-u-becniho-dvora.jpg",
                "author" => 'Josef Matička',
                "title" => 'U Obecního dvora',
                "status" => 'Hledáme',
                "description" => 'Dům U Kulichů v tzv. Vrabčírně / Spatzengasse (pokračování ul. U Milosrdných - Barmherzigen Gasse) s průhledem do Anežské ulice / Agnesgasse, dům vlevo vybíhající rohem do ulice ...',

            ],
            [
                "link" => "/frantisek-se-boura/", "img" => "/templates/artopos/images/redac/thumbnail///882_maticka-frantsek-se-boura.jpg",
                "author" => 'Josef Matička',
                "title" => 'František se bourá',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostel-a-fara-sv-ducha/", "img" => "/templates/artopos/images/redac/thumbnail///881_101139.jpg",
                "author" => 'Stanislav Feikl',
                "title" => 'Kostel a fara sv. Ducha',
                "status" => 'Nalezeno',
                "description" => 'Ze zobrazené zástavby zůstal jen sám kostel, ve ostatní podlehlo asanaci.                ...',

            ],
            [
                "link" => "/ujezd-u-prerova/", "img" => "/templates/artopos/images/redac/thumbnail///880_148659.jpg",
                "author" => 'Augustin Mervart',
                "title" => 'Újezd u Přerova',
                "status" => 'Hledáme',
                "description" => 'U Přerova leží Újezdů hned několik - Horní, Dolní či Velký....                ...',

            ],
            [
                "link" => "/milotice-nad-becvou/", "img" => "/templates/artopos/images/redac/thumbnail///879_149721.jpg",
                "author" => 'Jan Konůpek',
                "title" => 'Milotice nad Bečvou',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostelik-1533/", "img" => "/templates/artopos/images/redac/thumbnail///878_150793.jpg",
                "author" => 'Alois Kalvoda',
                "title" => 'Stříbrná Skalice',
                "status" => 'Nalezeno',
                "description" => 'Obraz zachycuje východní stranu severní části náměstí ve Stříbrné Skalici, nyní okres Praha - východ. Budova s cibulovitou střešní bání je radnice č.p. 19. První vrata ...',

            ],
            [
                "link" => "/predjari-u-janovskeho-kostela/", "img" => "/templates/artopos/images/redac/thumbnail///877_154008.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Předjaří u Janovského kostela',
                "status" => 'Hledáme',
                "description" => 'Přesné místo pohledu na kostelík v Janově u Mladé Vožice nebude obtížné lokalizovat... Podle soupisu prací jde o obraz z roku 1947.     ...',

            ],
            [
                "link" => "/most-v-hlubocepich/", "img" => "/templates/artopos/images/redac/thumbnail///873_holy-pictura.jpg",
                "author" => ' ',
                "title" => 'Most v Hlubočepích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/opusteny-klaster-san-giacomo-u-dubrovnika/", "img" => "/templates/artopos/images/redac/thumbnail///872_150888.jpg",
                "author" => 'Arnošt Hofbauer',
                "title" => 'Opuštěný klášter San Giacomo u Dubrovníka',
                "status" => 'Nalezeno',
                "description" => 'Klášter sv. Jakuba (Sveti Jakov) se nachází nedaleko stejnojmenné oblíbené pláže.                ...',

            ],
            [
                "link" => "/cerna-zima/", "img" => "/templates/artopos/images/redac/thumbnail///870_149805.jpg",
                "author" => 'Bedřich Mudroch',
                "title" => 'Černá zima',
                "status" => 'Hledáme',
                "description" => 'Mudroch v roce 1937 působil v Trutnově jako středoškolský profesor, takže nejspíš jde o tohle město.           ...',

            ],
            [
                "link" => "/v-parku/", "img" => "/templates/artopos/images/redac/thumbnail///868_143552.jpg",
                "author" => 'Bedřich Mudroch',
                "title" => 'V parku',
                "status" => 'Hledáme',
                "description" => 'Dříve Jubilejní park, dnes Park Výstaviště. Mudroch v té době v Mladé Boleslavi působil.             ...',

            ],
            [
                "link" => "/na-jizere/", "img" => "/templates/artopos/images/redac/thumbnail///867_150791.jpg",
                "author" => 'Bedřich Mudroch',
                "title" => 'Na Jizeře',
                "status" => 'Hledáme',
                "description" => 'Krajina nejspíš z okolí Mladé Boleslavi, kde působil.                   ...',

            ],
            [
                "link" => "/karmelitska-ulice/", "img" => "/templates/artopos/images/redac/thumbnail///866_150843.jpg",
                "author" => 'František Serafinský Hnátek',
                "title" => 'Karmelitská ulice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/arena-v-ronde/", "img" => "/templates/artopos/images/redac/thumbnail///865_148968.jpg",
                "author" => 'František Tavík Šimon',
                "title" => 'Aréna v Rondě',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/bez-nazvu-nove-mesto-nad-metuji/", "img" => "/templates/artopos/images/redac/thumbnail///864_149092.jpg",
                "author" => 'Antonín Kybal',
                "title" => 'Bez názvu (Nové Město nad Metují)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/v-lisabonu/", "img" => "/templates/artopos/images/redac/thumbnail///863_148959kars.jpg",
                "author" => 'Jiří (Georg) Kars',
                "title" => 'V Lisabonu',
                "status" => 'Nalezeno',
                "description" => 'Záběr byl pořízen z horního patra nebo střechy budovy na nároží Avenida da Liberdade a Praca dos Restauradores v centru Lisabonu, která dnes slouží jako vyhledávané ...',

            ],
            [
                "link" => "/pohled-na-nezname-mesto/", "img" => "/templates/artopos/images/redac/thumbnail///862_pokorny-stehlik.jpg",
                "author" => 'Jaroslav Pokorný',
                "title" => 'Pohled na Brno',
                "status" => 'Hledáme',
                "description" => 'Jde podle všeho o značně stylizovaný pohled na Brno z Červeného kopce. Špilberk je celkem jasný, kostel napravo se dvěma věžemi je asi svatý Michal a ...',

            ],
            [
                "link" => "/vesnice-s-bozima-mukama/", "img" => "/templates/artopos/images/redac/thumbnail///861_87777michl.jpg",
                "author" => 'František Michl',
                "title" => 'Vesnice s božími muky',
                "status" => 'Hledáme',
                "description" => 'Krajina nejspíš na Chodsku, kde Michl působil.                    ...',

            ],
            [
                "link" => "/namesti-v-tynci-nad-labem/", "img" => "/templates/artopos/images/redac/thumbnail///860_130202.jpg",
                "author" => 'Miloš Jiránek',
                "title" => 'Náměstí v Týnci nad Labem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ruzova-ulice-v-tabore/", "img" => "/templates/artopos/images/redac/thumbnail///859_148399jansa.jpg",
                "author" => 'Václav Jansa',
                "title" => 'Růžová ulice v Táboře',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/most-v-podebraech/", "img" => "/templates/artopos/images/redac/thumbnail///858_148823.jpg",
                "author" => 'Čeněk Choděra',
                "title" => 'Most v Poděbradech',
                "status" => 'Hledáme',
                "description" => 'Ocelový most, který byl postaven r. 1889 na místě řetězového mostu. Měl dvě pole o světlosti 46 metrů se středním pilířem oválného půdorysu s kvádrovým obkladem. ...',

            ],
            [
                "link" => "/brezina-nad-jizerou/", "img" => "/templates/artopos/images/redac/thumbnail///857_130584chodera.jpg",
                "author" => 'Čeněk Choděra',
                "title" => 'Březina nad Jizerou',
                "status" => 'Hledáme',
                "description" => 'Motiv s kostelem sv. Vavřince.                      ...',

            ],
            [
                "link" => "/koupaliste-a-elektrarna-v-kostelci/", "img" => "/templates/artopos/images/redac/thumbnail///856_148822.jpg",
                "author" => 'Čeněk Choděra',
                "title" => 'Koupaliště a elektrárna v Kostelci',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/jindrichuv-hradec-1507/", "img" => "/templates/artopos/images/redac/thumbnail///855_90085.jpg",
                "author" => 'Jiří Kaucký',
                "title" => 'Jindřichův Hradec',
                "status" => 'Nalezeno',
                "description" => 'Rondel jindřichohradeckého zámku přes řeku Nežárku s dodnes stojícím domem.                 ...',

            ],
            [
                "link" => "/mestecko-v-bridlicnatem-kraji--budisov-n-b/", "img" => "/templates/artopos/images/redac/thumbnail///854_import_fotoinzerat_98078.jpg",
                "author" => 'František Smolka',
                "title" => 'Městečko v břidličnatém kraji /  Budišov n. B.',
                "status" => 'Hledáme',
                "description" => 'Pohled na Budišov nad Budišovkou.                      ...',

            ],
            [
                "link" => "/drahanovice/", "img" => "/templates/artopos/images/redac/thumbnail///853_779074194_aukce_1.jpg",
                "author" => 'Jindřich Lenhart',
                "title" => 'Drahanovice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/predjari-na-vsi/", "img" => "/templates/artopos/images/redac/thumbnail///852_120209lenhart.jpg",
                "author" => 'Jindřich Lenhart',
                "title" => 'Předjaří na vsi',
                "status" => 'Hledáme',
                "description" => 'Motiv z periferie Olomouce, kde Lenhart často maloval.                   ...',

            ],
            [
                "link" => "/tabor-1499/", "img" => "/templates/artopos/images/redac/thumbnail///850_111051tabor.jpg",
                "author" => 'Jindřich Schenk',
                "title" => 'Tábor',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/mestska-brana/", "img" => "/templates/artopos/images/redac/thumbnail///849_28053bubenicek.jpg",
                "author" => 'Jindřich Bubeníček',
                "title" => 'Příhrádek v Pardubicích',
                "status" => 'Nalezeno',
                "description" => 'Nádvoří tzv. "Příhrádku" v Pardubicích. Středověké opevněné předpolí zámku se zbytky nejstarší městské fortifikace bylo významným prvkem v komunikačním plánu města. Tzv. Příhrádek, na plánech z 18. ...',

            ],
            [
                "link" => "/chalupy-u-cafourkova-mlyna/", "img" => "/templates/artopos/images/redac/thumbnail///848_68425tekly.jpg",
                "author" => 'Otakar Teklý',
                "title" => 'Chalupy u Cafourkova mlýna',
                "status" => 'Hledáme',
                "description" => 'Prostřední mlýn v Písku, zvaný též Cafourkův stával na konci bývalé Rybářské ulice. Nazýval se podle čimelického mlynáře Josefa Cafourka, který ho koupil v roce 1876 ...',

            ],
            [
                "link" => "/litomerice-1494/", "img" => "/templates/artopos/images/redac/thumbnail///847_ferdinand-adamek-111.jpg",
                "author" => 'Ferdinand Adámek',
                "title" => 'Litoměřice',
                "status" => 'Hledáme',
                "description" => 'Vedle farního kostela na náměstí je vidět další dominantu náměstí: v jádru goticko renesanční dům Kalich s charakteristickou střešní bání v podobě kalicha.    ...',

            ],
            [
                "link" => "/salamanca/", "img" => "/templates/artopos/images/redac/thumbnail///846_121806vlk.jpg",
                "author" => 'Ferdinand Vlk',
                "title" => 'Salamanca',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/karlovy-vary-1491/", "img" => "/templates/artopos/images/redac/thumbnail///845_98705.jpg",
                "author" => 'Ferdinand Kotvald',
                "title" => 'Karlovy Vary',
                "status" => 'Nalezeno',
                "description" => 'Pohled z promenády Stará louka přes Teplou do Vřídelní ulice s kostelem sv. Máří Magdalény v pozadí.          ...',

            ],
            [
                "link" => "/pohled-na-jicin/", "img" => "/templates/artopos/images/redac/thumbnail///844_97276-2.jpg",
                "author" => 'Jaro Procházka',
                "title" => 'Pohled na Jičín',
                "status" => 'Nalezeno',
                "description" => 'Pohled od jihu na centrum Jičína se zbytky městských hradeb v popředí, zatímco v pozadí je vlevo Valdštenský palác, uprostřed presbytář kostela sv. Jakuba Většího, před ...',

            ],
            [
                "link" => "/pohled-na-emauzy-1489/", "img" => "/templates/artopos/images/redac/thumbnail///843_126048.jpg",
                "author" => 'Jaro Procházka',
                "title" => 'Pohled na Emauzy',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/podzimni-mala-strana/", "img" => "/templates/artopos/images/redac/thumbnail///842_133001.jpg",
                "author" => 'Jaro Procházka',
                "title" => 'Podzimní Malá Strana',
                "status" => 'Nalezeno',
                "description" => 'Vrtbovská zahrada s Braunovou sochou Apollona s kostelem Panny Marie Vítězné.                ...',

            ],
            [
                "link" => "/narodni-trida/", "img" => "/templates/artopos/images/redac/thumbnail///841_146453.jpg",
                "author" => 'Jaro Procházka',
                "title" => 'Národní třída',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-brno/", "img" => "/templates/artopos/images/redac/thumbnail///840_8471petrovan.jpg",
                "author" => 'Bedřich Petrovan',
                "title" => 'Pohled na Brno',
                "status" => 'Hledáme',
                "description" => 'Motiv z industriální periferie Brna - Černovic.                    ...',

            ],
            [
                "link" => "/pohled-na-emauzy/", "img" => "/templates/artopos/images/redac/thumbnail///839_tomsa-emauzy.jpg",
                "author" => 'Karel Tomsa',
                "title" => 'Pohled na Emauzy',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/vez-kostela-sv-klimenta-v-praze/", "img" => "/templates/artopos/images/redac/thumbnail///838_92260tomsa.jpg",
                "author" => 'Karel Tomsa',
                "title" => 'Věž kostela sv. Klimenta v Praze',
                "status" => 'Nalezeno',
                "description" => 'Pohled Novomlýnskou ulicí na věž kostela sv. Klimenta v Praze.                 ...',

            ],
            [
                "link" => "/palecek-u-slaneho/", "img" => "/templates/artopos/images/redac/thumbnail///837_9840palecek.jpg",
                "author" => 'Václav Toman',
                "title" => 'Páleček u Slaného',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostel-sv-josta-v-ceskem-krumlove/", "img" => "/templates/artopos/images/redac/thumbnail///836_screenshot-2015-02-05-20.jpg",
                "author" => 'Jiří Stibral',
                "title" => 'Neznámý kostel',
                "status" => 'Hledáme',
                "description" => 'Obrázek byl publikován pod návem Kostel sv. Jošta v Českém Krumlově, tento kostel to však evidentně není.          ...',

            ],
            [
                "link" => "/podzim-v-krizanech/", "img" => "/templates/artopos/images/redac/thumbnail///835_68484vacke.jpg",
                "author" => 'Josef Vacke',
                "title" => 'Podzim v Křížanech',
                "status" => 'Hledáme',
                "description" => 'Do Křižan přišel Josef Vacke v roce 1946 v reakci na výzvy úřadů, které musely řešit problém opuštěných Sudet. Pražští umělci se tak stali národními správci ...',

            ],
            [
                "link" => "/hrad-rabi-1475/", "img" => "/templates/artopos/images/redac/thumbnail///834_primaryullyrch-rabi.jpg",
                "author" => 'Bohumil Ullrych',
                "title" => 'Hrad Rabí',
                "status" => 'Nalezeno',
                "description" => 'Pohled ze svahu vrchu Březiny při silnici na Horažďovice zhruba na úrovni kostela sv. Jana Nep. Oproti dnešku na panoramatickém pohledu chybí dům se štítem, který ...',

            ],
            [
                "link" => "/pod-vysehradem/", "img" => "/templates/artopos/images/redac/thumbnail///833_17986havlikova.jpg",
                "author" => 'Věra Havlíková',
                "title" => 'Pod Vyšehradem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/louvecienne-u-parize/", "img" => "/templates/artopos/images/redac/thumbnail///832_18962.jpg",
                "author" => 'Božena Jirásková - Jelínková',
                "title" => 'Louvecienne u Paříže',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/za-humny-kyselov/", "img" => "/templates/artopos/images/redac/thumbnail///831_39019drabkova.jpg",
                "author" => 'Marta Rožánková - Drábková',
                "title" => 'Za humny (Kyselov)',
                "status" => 'Hledáme',
                "description" => 'Kyselov, něm. Giesshübel, místní část Slavonína, od r. 1974 městská část Olomouce se nalézá 4 km jižně od centra Olomouce, na pravém břehu potoka Nemilanka pod ...',

            ],
            [
                "link" => "/most-ve-veverske-bitysce/", "img" => "/templates/artopos/images/redac/thumbnail///830_31647bochorakova.jpg",
                "author" => 'Helena Bochořáková - Dittrichová',
                "title" => 'Most ve Veverské Bitýšce',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/brnenska-krajina/", "img" => "/templates/artopos/images/redac/thumbnail///829_dilo-002-00009-1-v3.jpg",
                "author" => 'Helena Bochořáková - Dittrichová',
                "title" => 'Brněnská krajina',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/v-ohbi-reky-trikralka/", "img" => "/templates/artopos/images/redac/thumbnail///828_4968braunerova.jpg",
                "author" => 'Zdeňka Braunerová',
                "title" => 'V ohbí řeky (Tříkrálka)',
                "status" => 'Nalezeno',
                "description" => 'Autorka zobrazila bývalou viniční usedlost Tříkrálka u Sedleckého přívozu na severním okraji Prahy. Přívoz spojoval pražskou čtvrť Sedlec s osadou Zámky patřící do Bohnic. V minulosti sloužil ...',

            ],
            [
                "link" => "/telc/", "img" => "/templates/artopos/images/redac/thumbnail///827_89555telc.jpg",
                "author" => 'Zdeňka Braunerová',
                "title" => 'Telč',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/chmelarska-vesnice/", "img" => "/templates/artopos/images/redac/thumbnail///826_19861balas.jpg",
                "author" => 'Zděněk Balaš',
                "title" => 'Chmelařská vesnice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/dvur-s-bilym-konem/", "img" => "/templates/artopos/images/redac/thumbnail///825_primarymaticka-dvur-s-bilym-konem.jpg",
                "author" => 'Josef Matička',
                "title" => 'Dvůr s bílým koněm',
                "status" => 'Nalezeno',
                "description" => 'Páté nádvoří Anežského kláštera; nižší dvoupodlažní budova vlevo (souběžná s Vltavou, Géza Včelička ji nazývá "obytným přístavkem An. kl. ze 17. st.") a konvent vpravo dodnes ...',

            ],
            [
                "link" => "/krajina-dubrovnik/", "img" => "/templates/artopos/images/redac/thumbnail///824_filla_dubrovnik_orez_ostrava.jpg",
                "author" => 'Emil Filla',
                "title" => 'Krajina - Dubrovnik',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/lazne-mseno/", "img" => "/templates/artopos/images/redac/thumbnail///823_14450.jpg",
                "author" => 'František Hladík',
                "title" => 'Lázně Mšené',
                "status" => 'Nalezeno',
                "description" => 'Obraz zachycuje čilý ruch před lázeňským domem Dvorana v Lázních Mšené. Jde o malé lázně v údolí Mšenského potoka na východě obce Mšené-lázně v okrese Litoměřice. ...',

            ],
            [
                "link" => "/za-mostem-v-pisku/", "img" => "/templates/artopos/images/redac/thumbnail///818_synecky.jpg",
                "author" => 'Luboš Synecký',
                "title" => 'Za mostem v Písku',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ze-starych-holesovic-ii/", "img" => "/templates/artopos/images/redac/thumbnail///817_haise-u-leekr-ii.jpg",
                "author" => 'Václav Haise',
                "title" => 'Ze starých Holešovic II',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/u-elektrarny/", "img" => "/templates/artopos/images/redac/thumbnail///816_haise-u-leekr.jpg",
                "author" => 'Václav Haise',
                "title" => 'U elektrárny',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/u-stare-skoly/", "img" => "/templates/artopos/images/redac/thumbnail///815_595-gustav-macoun-u-milosrdnych-2.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Nádvoří domu v ulici U Staré školy',
                "status" => 'Nalezeno',
                "description" => 'Dvůr domu čp. 154 v ulici U Staré školy zbořeného v roce 1911.              ...',

            ],
            [
                "link" => "/roh-kozi-ulice-a-u-milosrdnych/", "img" => "/templates/artopos/images/redac/thumbnail///814_586-gustav-macoun-prazske-zakouti-2.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Roh Kozí ulice a U Milosrdných',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/koupani-u-celakovic/", "img" => "/templates/artopos/images/redac/thumbnail///813_471-bohumir-cila-koupani-u-celakovic-2.jpg",
                "author" => 'Bohumír Číla',
                "title" => 'Koupání u Čelákovic',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ulice-v-tucapech/", "img" => "/templates/artopos/images/redac/thumbnail///812_1604-borivoj-zufan-ulice-v-tucapech-1604-2.jpg",
                "author" => 'Bořivoj Žufan',
                "title" => 'Ulice v Tučapech',
                "status" => 'Hledáme',
                "description" => 'Nejspíš jde o nějvětší ves toho jména ležící u Soběslavi.                 ...',

            ],
            [
                "link" => "/namesti-konecneho-v-brne/", "img" => "/templates/artopos/images/redac/thumbnail///810_1586-milan-klvana-namesti-konecneho-1586.jpg",
                "author" => 'Milan Klvaňa',
                "title" => 'Náměstí Konečného v Brně',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/cukrovar-v-libani/", "img" => "/templates/artopos/images/redac/thumbnail///809_1674-frantisek-max-cukrovar-liban-1674.jpg",
                "author" => 'František Max',
                "title" => 'Cukrovar v Libáni',
                "status" => 'Nalezeno',
                "description" => 'Vlečka dnes již neexistuje, ale některé zachycené budovy stojí dodnes - "most", ten je však dnes obložen plechem, nebo budova vpravo, jíž projíždí vagón, se zvláštním ...',

            ],
            [
                "link" => "/komorany/", "img" => "/templates/artopos/images/redac/thumbnail///807_img_6874.jpg",
                "author" => 'Jiří Mandel',
                "title" => 'Komořany',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/horice-v-podkrkonosi/", "img" => "/templates/artopos/images/redac/thumbnail///802_img_7577.jpg",
                "author" => 'František Charvát',
                "title" => 'Hořice v Podkrkonoší',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/helmovsky-mlyn/", "img" => "/templates/artopos/images/redac/thumbnail///801_img_8464.jpg",
                "author" => 'J. Konečný',
                "title" => 'Helmovské mlýny',
                "status" => 'Hledáme',
                "description" => 'V místech dnešního ministerstva zemědělství a ministerstva dopravy stály od středověku Helmovské mlýny. Patřily sem i mlýn Helmův a Kubešův. Při regulaci Vltavy ve 30. letech ...',

            ],
            [
                "link" => "/z-vrsovic-pod-havlickovymi-sady/", "img" => "/templates/artopos/images/redac/thumbnail///799_img_8488.jpg",
                "author" => 'Jaromír Kunc',
                "title" => 'Z Vršovic (Pod Havlíčkovými sady)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pristav-v-troji/", "img" => "/templates/artopos/images/redac/thumbnail///798_120_bc6727holy.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Přívoz v Troji',
                "status" => 'Nalezeno',
                "description" => 'Pohled na Troju ze Stromovky u tehdejšího přívozu. Oblíbený motiv, který se objevuje i v díle Rudolfa Kremličky či Otakara Kerharta.      ...',

            ],
            [
                "link" => "/rybnik-u-tetaurovy-lhoty/", "img" => "/templates/artopos/images/redac/thumbnail///796_033_78ee90.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Rybník u Tetaurovy Lhoty',
                "status" => 'Hledáme',
                "description" => 'Podchýšská Lhota je malá vesnice, část obce Chyšky v okrese Písek. Nachází se asi 1,5 km na jihozápad od Chyšek. Je zde evidováno 27 adres.[1], Trvale ...',

            ],
            [
                "link" => "/pod-karlovem/", "img" => "/templates/artopos/images/redac/thumbnail///792_234holan.jpg",
                "author" => 'Karel Holan',
                "title" => 'Pod Karlovem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/abertamy/", "img" => "/templates/artopos/images/redac/thumbnail///791_186moravec.jpg",
                "author" => 'Otakar Moravec',
                "title" => 'Abertamy',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kuneticka-hora-1426/", "img" => "/templates/artopos/images/redac/thumbnail///790_027_c93a6a.jpg",
                "author" => 'Ladislav Vele',
                "title" => 'Kunětická hora',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/krajina-z-pecky/", "img" => "/templates/artopos/images/redac/thumbnail///788_967466hl.jpg",
                "author" => 'Vladimír Hlubuček',
                "title" => 'Krajina z Pecky',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/dornych/", "img" => "/templates/artopos/images/redac/thumbnail///787_39168.jpg",
                "author" => 'Robert Hliněnský',
                "title" => 'Dornych',
                "status" => 'Hledáme',
                "description" => 'Ulice v Brně, nazvaná podle bývalé předměstské obce s mlýnem, připojené roku 1850 k Brnu. Ty dva domy tam stále stojí a dřevěný plot je na protější straně ...',

            ],
            [
                "link" => "/mestska-periferie-1423/", "img" => "/templates/artopos/images/redac/thumbnail///786_38423.jpg",
                "author" => 'Robert Hliněnský',
                "title" => 'Městská periferie',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/mestska-periferie/", "img" => "/templates/artopos/images/redac/thumbnail///785_34316.jpg",
                "author" => 'Robert Hliněnský',
                "title" => 'Městská periferie',
                "status" => 'Hledáme',
                "description" => 'Motiv z periferie Brna.                       ...',

            ],
            [
                "link" => "/ulice-ke-kostelu/", "img" => "/templates/artopos/images/redac/thumbnail///784_145747hlinensky.jpg",
                "author" => 'Robert Hliněnský',
                "title" => 'Ulice ke kostelu',
                "status" => 'Hledáme',
                "description" => 'Obraz zachycuje pohled na Římské náměstí v Brně směrem k západu. Původní Židovský plácek byl centrem Židovské čtvrti ve středověkém Brně. Židé byli v roce 1454 ...',

            ],
            [
                "link" => "/domy/", "img" => "/templates/artopos/images/redac/thumbnail///783_147561hlin.jpg",
                "author" => 'Robert Hliněnský',
                "title" => 'Domy',
                "status" => 'Hledáme',
                "description" => 'Motiv nejspíš odněkud z Brna...                      ...',

            ],
            [
                "link" => "/v-bratislavskom-pristave/", "img" => "/templates/artopos/images/redac/thumbnail///782_145135.jpg",
                "author" => 'Martin Tvrdoň',
                "title" => 'V bratislavskom prístave',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/bily-stit-zbecno-u-krivoklatu/", "img" => "/templates/artopos/images/redac/thumbnail///781_144956.jpg",
                "author" => 'Alois Kalvoda',
                "title" => 'Bílý štít, Zbečno u Křivoklátu',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/cementarna-v-podoli-ii/", "img" => "/templates/artopos/images/redac/thumbnail///780_145685benes.jpg",
                "author" => 'Vlastimil Beneš',
                "title" => 'Cementárna v Podolí II',
                "status" => 'Hledáme',
                "description" => 'Druhá verze Benešova akvarelu stejného motivu.                     ...',

            ],
            [
                "link" => "/dejvicke-nadrazi-v-noci/", "img" => "/templates/artopos/images/redac/thumbnail///779_161dejvice.jpg",
                "author" => 'Vladimír Hlubuček',
                "title" => 'Dejvické nádraží v noci',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ve-strazi/", "img" => "/templates/artopos/images/redac/thumbnail///778_095.jpg",
                "author" => 'František Michl',
                "title" => 'Ve Stráži',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/hradec-kralove-v-zime/", "img" => "/templates/artopos/images/redac/thumbnail///777_145524mervart.jpg",
                "author" => 'Augustin Mervart',
                "title" => 'Hradec Králové v zimě',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/turnovska-cisticka/", "img" => "/templates/artopos/images/redac/thumbnail///776_145762.jpg",
                "author" => 'Jaroslav Klápště',
                "title" => 'Turnovská čistička',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-duchcov/", "img" => "/templates/artopos/images/redac/thumbnail///775_145783d.jpg",
                "author" => 'neznámý autor',
                "title" => 'Pohled na Duchcov',
                "status" => 'Hledáme',
                "description" => 'Pohled na Duchcov s klášterem a porcelánkou.                    ...',

            ],
            [
                "link" => "/turnov/", "img" => "/templates/artopos/images/redac/thumbnail///774_img_7846.jpg",
                "author" => 'Josef Jelínek',
                "title" => 'Turnov',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/jihoceska-naves/", "img" => "/templates/artopos/images/redac/thumbnail///773_144478.jpg",
                "author" => 'Jaroslav Novák',
                "title" => 'Jihočeská náves',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/slavisticke-kluziste-na-letne/", "img" => "/templates/artopos/images/redac/thumbnail///772_144435.jpg",
                "author" => 'Jaroslav Melichárek',
                "title" => 'Slávistické kluziště na Letné',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/krajina-s-kostelikem-1401/", "img" => "/templates/artopos/images/redac/thumbnail///771_144429-1.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Krajina s kostelíkem',
                "status" => 'Hledáme',
                "description" => 'Neatovaná drobná krajina, snad z okolí Mladé Vožice.                   ...',

            ],
            [
                "link" => "/hudlice/", "img" => "/templates/artopos/images/redac/thumbnail///765_121160.jpg",
                "author" => 'Alois Kalvoda',
                "title" => 'Hudlice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/slunecne-odpoledne/", "img" => "/templates/artopos/images/redac/thumbnail///764_121186.jpg",
                "author" => 'Alois Kalvoda',
                "title" => 'Slunečné odpoledne',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/jungmannova-ulice/", "img" => "/templates/artopos/images/redac/thumbnail///763_96770.jpg",
                "author" => 'Ferdinand Kotvald',
                "title" => 'Jungmannova ulice',
                "status" => 'Hledáme',
                "description" => 'Nedatovaný obraz snad někdy ze 60. let, vlevo čitelný štít "Divadlo komedie". Divadlo s tímto jménem sídlilo ve funkcionalistickém paláci od architekta Josefa Říhy z r. ...',

            ],
            [
                "link" => "/krusovice/", "img" => "/templates/artopos/images/redac/thumbnail///760_88989.jpg",
                "author" => 'Václav Rabas',
                "title" => 'Krušovice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/na-periferii/", "img" => "/templates/artopos/images/redac/thumbnail///759_89024.jpg",
                "author" => 'Josef Multrus',
                "title" => 'Na periferii',
                "status" => 'Nalezeno',
                "description" => 'Nedatovaný obraz Josefa Multruse s pohledem na Hlubočepský viadukt, jedno z oblíbených pražských malířských témat.            ...',

            ],
            [
                "link" => "/zimni-nalada/", "img" => "/templates/artopos/images/redac/thumbnail///758_123474.jpg",
                "author" => 'Václav Rabas',
                "title" => 'Zimní nálada',
                "status" => 'Hledáme',
                "description" => 'Zimní krajina, nejspíš z Rabasových Krušovic.                     ...',

            ],
            [
                "link" => "/pout-v-ronove-nad-doubravou/", "img" => "/templates/artopos/images/redac/thumbnail///757_121216.jpg",
                "author" => 'Filip Milinovský',
                "title" => 'Pouť v Ronově nad Doubravou',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/vysehradsky-pristav/", "img" => "/templates/artopos/images/redac/thumbnail///756_39425.jpg",
                "author" => 'Emil Artur Pittermann (Longen)',
                "title" => 'Vyšehradský přístav',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-mesto/", "img" => "/templates/artopos/images/redac/thumbnail///755_83762.jpg",
                "author" => 'Emil Artur Pittermann (Longen)',
                "title" => 'Pohled na město',
                "status" => 'Hledáme',
                "description" => 'Jedna z autorových typických prací, jimiž si vydělával na živobytí, prozrazující vliv van Goghova expresivního rukopisu, zobrazuje neznámé české město.       ...',

            ],
            [
                "link" => "/podzim-na-petrkove/", "img" => "/templates/artopos/images/redac/thumbnail///754_83702.jpg",
                "author" => 'František Kaván',
                "title" => 'Podzim na Petrkově',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/naves-1386/", "img" => "/templates/artopos/images/redac/thumbnail///753_83303.jpg",
                "author" => 'Otakar Kubín (Othon Coubine)',
                "title" => 'Náves',
                "status" => 'Hledáme',
                "description" => 'Motiv z nějaké vsi nedaleko Boskovic.                     ...',

            ],
            [
                "link" => "/u-olsan/", "img" => "/templates/artopos/images/redac/thumbnail///752_90611.jpg",
                "author" => 'František Kaván',
                "title" => 'U Olšan',
                "status" => 'Hledáme',
                "description" => 'Ojedinělá Kavánova pragensie, vystaveno na výstavě Jednoty umělců výtvarných v Praze v roce 1936.             ...',

            ],
            [
                "link" => "/tynska-ulicka/", "img" => "/templates/artopos/images/redac/thumbnail///751_5648.jpg",
                "author" => 'Ferdinand Kotvald',
                "title" => 'Týnská ulička',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/dolni-lhotka-u-ceskych-budejovic/", "img" => "/templates/artopos/images/redac/thumbnail///749_47309.jpg",
                "author" => 'Augustin Mervart',
                "title" => 'Dolní Lhotka u Českých Budějovic',
                "status" => 'Hledáme',
                "description" => 'Žádná Dolní Lhotka poblíž Budějovic neexistuje - snad se jedná u Lhotku nedaleko Křemže, vzdálenou od Budějovic 20 km.        ...',

            ],
            [
                "link" => "/omisalj-castel-muschio/", "img" => "/templates/artopos/images/redac/thumbnail///748_84045.jpg",
                "author" => 'Augustin Mervart',
                "title" => 'Omišalj (Castel Muschio)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ulice-ve-velkem-ujezde-u-lipnika/", "img" => "/templates/artopos/images/redac/thumbnail///747_mervart.jpg",
                "author" => 'Augustin Mervart',
                "title" => 'Ulice ve Velkém Újezdě u Lipníka',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/nad-lani-–-roznovem/", "img" => "/templates/artopos/images/redac/thumbnail///745_mervart2.jpg",
                "author" => 'Augustin Mervart',
                "title" => 'Nad Láni – Rožnovem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/certovka/", "img" => "/templates/artopos/images/redac/thumbnail///744_111514.jpg",
                "author" => 'Jan B. Minařík',
                "title" => 'Čertovka',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/linhartsky-placek/", "img" => "/templates/artopos/images/redac/thumbnail///743_114764.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Linhartský plácek',
                "status" => 'Hledáme',
                "description" => 'V Macounově odkazu shledáváme dva dominující celky - krajinu Českomoravské Vysočiny a pragensie. Macoun měl již jako student roku 1908 štěstí, že mohl mít stojan vedle ...',

            ],
            [
                "link" => "/cesta-1375/", "img" => "/templates/artopos/images/redac/thumbnail///742_143642.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Cesta',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/klaster-ve-francii-/", "img" => "/templates/artopos/images/redac/thumbnail///741_143647.jpg",
                "author" => 'Friedrich Feigl',
                "title" => 'Klášter ve Francii (?)',
                "status" => 'Hledáme',
                "description" => 'Se vší pravděpodobností nejde o autorský název a situování do Francie tudíž také není zcela jisté.           ...',

            ],
            [
                "link" => "/prejezd-v-dejvicich/", "img" => "/templates/artopos/images/redac/thumbnail///740_143655.jpg",
                "author" => 'František Hudeček',
                "title" => 'Přejezd v Dejvicích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/skuticko/", "img" => "/templates/artopos/images/redac/thumbnail///739_1skuticko.jpg",
                "author" => 'Gustav Porš',
                "title" => 'Skutíčko',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostel-1366/", "img" => "/templates/artopos/images/redac/thumbnail///737_143621.jpg",
                "author" => 'Václav Vojtěch Novák',
                "title" => 'Kostel',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/rybnik-v-kolovraetch/", "img" => "/templates/artopos/images/redac/thumbnail///736_143619.jpg",
                "author" => 'Karel Schauer',
                "title" => 'Rybník v Kolovratech',
                "status" => 'Nalezeno',
                "description" => 'Návesní rybník v Kolovratech.                       ...',

            ],
            [
                "link" => "/rameno-vltavy-v-podbabe/", "img" => "/templates/artopos/images/redac/thumbnail///735_143600.jpg",
                "author" => 'Karol Molnár',
                "title" => 'Rameno Vltavy v Podbabě',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/hrad-pernstejn/", "img" => "/templates/artopos/images/redac/thumbnail///734_143541.jpg",
                "author" => 'Josef Štolovský',
                "title" => 'Hrad Pernštejn',
                "status" => 'Nalezeno',
                "description" => 'Pernštejn je dnes tak ukryt, že není prakticky odnikud vidět.                 ...',

            ],
            [
                "link" => "/lesni-samota-stare-belidlo/", "img" => "/templates/artopos/images/redac/thumbnail///733_143539.jpg",
                "author" => 'Jaromír Seidl',
                "title" => 'Lesní samota (Staré Bělidlo)',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/krajina-s-mostem/", "img" => "/templates/artopos/images/redac/thumbnail///732_143586.jpg",
                "author" => 'Rudolf Kremlička',
                "title" => 'Krajina s mostem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ves-hamry-slovensko/", "img" => "/templates/artopos/images/redac/thumbnail///731_142766.jpg",
                "author" => 'Rudolf Kremlička',
                "title" => 'Ves Hamry, Slovensko',
                "status" => 'Hledáme',
                "description" => 'Z mnoh vsí podobného názvu se nejspíš jedná o ves Hámre, část Nálepkova, neboť nedaleko je Gelnica, kde tehdy Kremlička také maloval.     ...',

            ],
            [
                "link" => "/stanice-tramvaje/", "img" => "/templates/artopos/images/redac/thumbnail///730_primarymuktrus-zastavka.jpg",
                "author" => 'Josef Multrus',
                "title" => 'Stanice tramvaje',
                "status" => 'Hledáme',
                "description" => 'Jeden z nejzajímavějších obrazů k identifikaci: tramvajová zastávka, se vší pravděpodpobností v Praze, by mohla být určena podle výrazných motivů brány do parku (?) a budovy ...',

            ],
            [
                "link" => "/most-v-pisku/", "img" => "/templates/artopos/images/redac/thumbnail///728_konicek-msot-v-pisku-prague-auctions.jpg",
                "author" => 'Oldřich Koníček',
                "title" => 'Most v Písku',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/na-frantisku/", "img" => "/templates/artopos/images/redac/thumbnail///727_primary-maticka-na-frantisku.jpg",
                "author" => 'Josef Matička',
                "title" => 'Na Františku',
                "status" => 'Nalezeno',
                "description" => 'Pohled ke zdi kláštera na Františku na parčík, na jehož místě byla později vybudována hříště. Géza Včeliča ve své knize Pražské tajemství uvádí, že na tomto ...',

            ],
            [
                "link" => "/za-sv-hastalem/", "img" => "/templates/artopos/images/redac/thumbnail///726_primaryza-svatym-hastaem.jpg",
                "author" => 'Josef Matička',
                "title" => 'Za sv. Haštalem',
                "status" => 'Nalezeno',
                "description" => 'Obraz spojující naivizující stylizaci malebného staropražského zákoutí s několika reálnými detaily z doby svého vzniku, jako je popelnice vzadu u domu, žebřík u lucerny a dva ...',

            ],
            [
                "link" => "/hastalsky-placek/", "img" => "/templates/artopos/images/redac/thumbnail///725_primaryhastalsky-placek.jpg",
                "author" => 'Josef Matička',
                "title" => 'Haštalský plácek',
                "status" => 'Nalezeno',
                "description" => 'Dům s freskou ve štukovém rámci zcela vpravo je kostnice s barokní kaplí Nejsvětější Trojice z dob, kdy kostel sv. Haštala obklopoval hřbitov, zrušený v roce ...',

            ],
            [
                "link" => "/klasterni-ulicka/", "img" => "/templates/artopos/images/redac/thumbnail///721_karel-uher-50.jpg",
                "author" => 'Karel Uher',
                "title" => 'Klášterní ulička',
                "status" => 'Nalezeno',
                "description" => 'Klášterní ulička, blíže o motivu u obrazu na stejné téma od J. Matičky.              ...',

            ],
            [
                "link" => "/kostel/", "img" => "/templates/artopos/images/redac/thumbnail///720_karel-miller-litografie-podzim-na-kampe-21-x-40-cm.jpg",
                "author" => 'Karel Miller',
                "title" => 'Podzim na Kampě',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/motiv-z-amsterdamu/", "img" => "/templates/artopos/images/redac/thumbnail///719_multrus-motiv-z-amsetrdamu-134754.jpg",
                "author" => 'Josef Multrus',
                "title" => 'Motiv z Amsterdamu',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/hrad-zebrak/", "img" => "/templates/artopos/images/redac/thumbnail///718_olej-na-papire-375-x-30-cm.jpg",
                "author" => 'Eduard Stavinoha',
                "title" => 'Hrad Žebrák',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostelni-ulice-v-pardubicich/", "img" => "/templates/artopos/images/redac/thumbnail///717_grus-pardubice.jpg",
                "author" => 'Jaroslav Grus',
                "title" => 'Kostelní ulice v Pardubicích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/za-starych-pardubic/", "img" => "/templates/artopos/images/redac/thumbnail///716_39570.jpg",
                "author" => 'Antonín Hudeček',
                "title" => 'Za starých Pardubic',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/z-plzne/", "img" => "/templates/artopos/images/redac/thumbnail///715_121211.jpg",
                "author" => 'František Emler',
                "title" => 'Z Plzně',
                "status" => 'Nalezeno',
                "description" => 'Pohled na Masné krámy ještě před rekonstrukcí (1967-71, při ní zaniklo rozetové okno v průčelí) s Vodárenskou věží, vzadu vykukuje špička věže sv. Bartoloměje.   ...',

            ],
            [
                "link" => "/cementarna-v-podoli-1339/", "img" => "/templates/artopos/images/redac/thumbnail///714_102213.jpg",
                "author" => 'Vlastimil Beneš',
                "title" => 'Cementárna v Podolí I',
                "status" => 'Nalezeno',
                "description" => 'V roce 1945, kdy Beneš namaloval tento akvarel, už podolská cementárna nefungovala. Provoz se začal utlumovat již ve třicátých letech, kdy byl lom téměř vytěžen. Změnily ...',

            ],
            [
                "link" => "/karlovy-vary-drahovice-1338/", "img" => "/templates/artopos/images/redac/thumbnail///713_hauk-artplus.jpg",
                "author" => 'V. J. Hauk (Vilém František Staněk)',
                "title" => 'Karlovy Vary - Drahovice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/karlovy-vary-1335/", "img" => "/templates/artopos/images/redac/thumbnail///711_grus-karlovy-vary.jpg",
                "author" => 'Jaroslav Grus',
                "title" => 'Karlovy Vary',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/karlovy-vary-1334/", "img" => "/templates/artopos/images/redac/thumbnail///710_jira-karlovy-vary.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Karlovy Vary',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/jaro-pod-cernou-studnici/", "img" => "/templates/artopos/images/redac/thumbnail///709_38108.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Jaro pod Černou Studnicí',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/cervanky-na-periferii/", "img" => "/templates/artopos/images/redac/thumbnail///708_135994-waldhauser-artplus.jpg",
                "author" => 'Antonín Waldhauser',
                "title" => 'Červánek na periferii',
                "status" => 'Nalezeno',
                "description" => 'Nesignovaný obraz, který se  objevil na aukčním trhu připsaný Antonínu Waldhauserovi s datací do 80. let 19. st. Jedná se výtopnu a točnu pro lokomotivy, tvořící ...',

            ],
            [
                "link" => "/boskovice-/", "img" => "/templates/artopos/images/redac/thumbnail///707_otakar-kubin.jpg",
                "author" => 'Otakar Kubín (Othon Coubine)',
                "title" => 'Svitávka',
                "status" => 'Nalezeno',
                "description" => 'Svitávka ze Svitavského kopce, vlevo vzadu Kunštát.                    ...',

            ],
            [
                "link" => "/klasterni-ulicka-i/", "img" => "/templates/artopos/images/redac/thumbnail///706_primary-maa-klasterni.jpg",
                "author" => 'Josef Matička',
                "title" => 'Klášterní ulička I',
                "status" => 'Nalezeno',
                "description" => 'Klášterní ulička mezi Anežským klášterem a blokem dnes neexistujících domů podél nábřeží, na  konci zahybající doleva. Zanikla někdy v 60. nebo 70. letech a dnes je ...',

            ],
            [
                "link" => "/sv-barbora-v-kutne-hore/", "img" => "/templates/artopos/images/redac/thumbnail///704_70507.jpg",
                "author" => 'Oldřich Koníček',
                "title" => 'Sv. Barbora v Kutné Hoře',
                "status" => 'Hledáme',
                "description" => '"Asi 600 metrů jižně od proslulého gotického chrámu sv. Barbory u Vyšatovy skály (tzv. Vyšaťák) vede přes Vrchlici tříobloukový most z lomového kamene z první poloviny ...',

            ],
            [
                "link" => "/staroprazske-zakouti-1326/", "img" => "/templates/artopos/images/redac/thumbnail///703_11869-spacil.jpg",
                "author" => 'Jan Spáčil',
                "title" => 'Staropražské zákoutí',
                "status" => 'Nalezeno',
                "description" => 'Dvorek na jižní straně Vlašské ulice, při nároží krátké slepé uličky směřující k jihu - dnes již zastavěný dvěma domy, jedním ze 30. let, druhým z ...',

            ],
            [
                "link" => "/na-kampe-1325/", "img" => "/templates/artopos/images/redac/thumbnail///702_83935.jpg",
                "author" => 'Jaroslav Kotas',
                "title" => 'Na Kampě',
                "status" => 'Nalezeno',
                "description" => 'Říční ulice u Čertovky                       ...',

            ],
            [
                "link" => "/krajina-u-podebrad/", "img" => "/templates/artopos/images/redac/thumbnail///701_130582.jpg",
                "author" => 'Oldřich Kerhart',
                "title" => 'Krajina u Poděbrad',
                "status" => 'Hledáme',
                "description" => 'Vzadu by mělo být popsáno, takže název "Krajina u Poděbrad" nevychází jen z autorovy biografie. Podle výrazných motivů sochy světce a viaduktu v pozadí by nemělo ...',

            ],
            [
                "link" => "/u-sv-apolinare-jedova-chyse-1322/", "img" => "/templates/artopos/images/redac/thumbnail///750_86228.jpg",
                "author" => 'Ferdinand Kotvald',
                "title" => 'U sv. Apolináře - Jedová chýše',
                "status" => 'Nalezeno',
                "description" => 'Téměř stejný motiv zachytil na svém obraze z r 1915 E. A. Pittermann, proto zde můžeme téměř doslova převzít text od tohoto obrazu. Malíř zachytil prostranství ...',

            ],
            [
                "link" => "/domky-ve-vrsovicich-na-strani-se-zaloznou/", "img" => "/templates/artopos/images/redac/thumbnail///699_primary-simunek-vrsovice.jpg",
                "author" => 'Jaroslav Šimůnek',
                "title" => 'Domky ve Vršovicích na stráni se záložnou',
                "status" => 'Nalezeno',
                "description" => 'Dům ve Smolenské ulici ve Vršovicích. Bydlela v něm paní Drobná, z jejího rodu byl starosta Vršovic Václav Drobný. Z domu se dnes dochovala jenom portálová ...',

            ],
            [
                "link" => "/kostel-panny-marie-snezne/", "img" => "/templates/artopos/images/redac/thumbnail///698_primary-wiesner-richard.jpg",
                "author" => 'Richard Wiesner',
                "title" => 'Kostel Panny Marie Sněžné',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pod-barrandovem/", "img" => "/templates/artopos/images/redac/thumbnail///697_primary-turek-pod-barrandovem.jpg",
                "author" => 'František Turek',
                "title" => 'Pod Barrandovem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/jaromer-1314/", "img" => "/templates/artopos/images/redac/thumbnail///696_primary-1-jaromer.jpg",
                "author" => 'Josef Šíma',
                "title" => 'Jaroměř',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/silnice-v-hlubocepich/", "img" => "/templates/artopos/images/redac/thumbnail///686_primary-1-stavinoha.jpg",
                "author" => 'Eduard Stavinoha',
                "title" => 'Silnice v Hlubočepích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/nusle-pod-grebovkou/", "img" => "/templates/artopos/images/redac/thumbnail///685_primary-1-simunek.jpg",
                "author" => 'Jaroslav Šimůnek',
                "title" => 'Nusle pod Grébovkou',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kozi-hrbety-u-prahy/", "img" => "/templates/artopos/images/redac/thumbnail///684_primary-1-spala.jpg",
                "author" => 'Václav Špála',
                "title" => 'Kozí hřbety u Prahy',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/machovo-jezero/", "img" => "/templates/artopos/images/redac/thumbnail///683_alfred-justitz-.jpg",
                "author" => 'Alfréd Justitz',
                "title" => 'Máchovo jezero',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/emauzy-1947/", "img" => "/templates/artopos/images/redac/thumbnail///682_primary-rada-emauzy.jpg",
                "author" => 'Vlastimil Rada',
                "title" => 'Emauzy',
                "status" => 'Nalezeno',
                "description" => 'Zimní pohled na vybombardovaný kostel Panny Marie a kapli sv. Kosmy a Damiána před ním. Vlevo vzadu vykukuje dvojvěží kostela sv. Jana Nepomuckého na Skalce K. I. ...',

            ],
            [
                "link" => "/predjari-v-holesovicich/", "img" => "/templates/artopos/images/redac/thumbnail///679_336-rychlovsky.jpg",
                "author" => 'Antonín Rychlovský',
                "title" => 'Předjaří v Holešovicích',
                "status" => 'Nalezeno',
                "description" => 'Pohled Strojnickou ulicí na křižovatku s Dukelských hrdinů, rohový dům č. p. 568 (dnes v přízemí restaurace Valcha), vlevo železniční most.      ...',

            ],
            [
                "link" => "/zimni-vychazka-materske-skolky/", "img" => "/templates/artopos/images/redac/thumbnail///678_primary-rychlovsky.jpg",
                "author" => 'Antonín Rychlovský',
                "title" => 'Zimní vycházka mateřské školky',
                "status" => 'Nalezeno',
                "description" => 'Pohled za zeď na začátku Řásnovky do dvora domu U Roubíčků - bývalého klášterního hřbitova, nyní zahrady mateřské školky, těsně přiléhající k Anežskému klášteru. Školka vznikla ...',

            ],
            [
                "link" => "/novy-svet/", "img" => "/templates/artopos/images/redac/thumbnail///677_primary-posva.jpg",
                "author" => 'Josef Pošva',
                "title" => 'Nový Svět',
                "status" => 'Nalezeno',
                "description" => 'Pohled vzhůru Černínskou ulicí, vzadu je vidět kostel kapucínského kláštera Panny Marie Andělské a za ním vykukuje věž Lorety. Současná situace je téměř stejná jako v ...',

            ],
            [
                "link" => "/v-poledne-u-podolskeho-lomu/", "img" => "/templates/artopos/images/redac/thumbnail///676_primary-nemecek.jpg",
                "author" => 'Václav Němeček',
                "title" => 'V poledne u Podolského lomu',
                "status" => 'Nalezeno',
                "description" => 'Obraz vznikl před rokem 1947, kdy v prostoru bývalého vápencového lomu vyrostl evangelický kostel podle projektu architekta Pavla Bareše, který na obraze ještě není zobrazen.  ...',

            ],
            [
                "link" => "/karlovo-namesti/", "img" => "/templates/artopos/images/redac/thumbnail///675_kerhart-cmvu.jpg",
                "author" => 'Oldřich Kerhart',
                "title" => 'Karlovo náměstí',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostely-sv-salvatora-a-frantiska-v-zime/", "img" => "/templates/artopos/images/redac/thumbnail///674_primary-maticka.jpg",
                "author" => 'Josef Matička',
                "title" => 'Kostely sv. Salvátora a Františka v zimě',
                "status" => 'Nalezeno',
                "description" => 'Obraz, ve sbírkách GHMP vedený pod chybným názvem Kostel sv. Barbory, zpodobuje gotické závěry kostelů sv. Františka (vlevo) a sv. Salvátora v klášteře sv. Anežky České. ...',

            ],
            [
                "link" => "/statek-na-taborsku/", "img" => "/templates/artopos/images/redac/thumbnail///672_primary-richard-lauda-statek.jpg",
                "author" => 'Richard Lauda',
                "title" => 'Statek na Táborsku',
                "status" => 'Hledáme',
                "description" => 'Richard Lauda působil v té době v rodné Jistebnici u Tábora, takže nejspíš jde o motiv z okolí.         ...',

            ],
            [
                "link" => "/u-kovarny-v-losove/", "img" => "/templates/artopos/images/redac/thumbnail///671_primary-u-kovarny-losov.jpg",
                "author" => 'Jindřich Lenhart',
                "title" => 'U kovárny v Lošově',
                "status" => 'Hledáme',
                "description" => 'Lošov je velká vesnice, část krajského města Olomouc. Nachází se asi 5,5 km na východ od centra Olomouce. Lošov navazuje na olomouckou část sv. Kopeček  ...',

            ],
            [
                "link" => "/kostel-ve-slavonine/", "img" => "/templates/artopos/images/redac/thumbnail///670_primary-kostel-slavonin.jpg",
                "author" => 'Jindřich Lenhart',
                "title" => 'Kostel ve Slavoníně',
                "status" => 'Nalezeno',
                "description" => 'Kostel sv. Ondřeje ve Slavoníně, historické obci, v roce 1919 začleněné do Olomouce.              ...',

            ],
            [
                "link" => "/praha-1286/", "img" => "/templates/artopos/images/redac/thumbnail///669_o_374.jpg",
                "author" => 'Jan Spáčil',
                "title" => 'Sv. Vít od Nového Světa',
                "status" => 'Nalezeno',
                "description" => 'Obraz vedený pod názvem Praha zachycuje pohled na katedrálu přes malebné domky Nového Světa.             ...',

            ],
            [
                "link" => "/havlickuv-brod-1285/", "img" => "/templates/artopos/images/redac/thumbnail///668_jaroslav-kotas-havlickuv-brod-olej-na-platne-57x71-cm-signovano-vpravo-dole.jpg",
                "author" => 'Jaroslav Kotas',
                "title" => 'Havlíčkův Brod',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/cheb-panorama/", "img" => "/templates/artopos/images/redac/thumbnail///665_o_595.jpg",
                "author" => 'Jan Spáčil',
                "title" => 'Cheb - panoráma',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/cheb-opravne-prace/", "img" => "/templates/artopos/images/redac/thumbnail///664_o_464.jpg",
                "author" => 'Jan Spáčil',
                "title" => 'Cheb - opravné práce',
                "status" => 'Nalezeno',
                "description" => 'Malíř Jan Spáčil programově v letech 1953 - 1965 zobrazoval asanaci a rekonstrukci Chebu. Zde zobrazil ústí Kamenné ulice od kostela sv. Bartoloměje (zeď vpravo) s ...',

            ],
            [
                "link" => "/stary-smichov/", "img" => "/templates/artopos/images/redac/thumbnail///662_primary-kuba-stary-smichov.jpg",
                "author" => 'Ludvík Kuba',
                "title" => 'Starý Smíchov',
                "status" => 'Nalezeno',
                "description" => 'Vedle Petržilkovské vodárenské věže je budova Petržilkovských mlýnů, nahrazená někdy během 1. světové války dnešní budovou (ta sloužila původně Regulační komisi). Barokní budova vlevo je Dienzenhoferův ...',

            ],
            [
                "link" => "/olse-nad-rekou/", "img" => "/templates/artopos/images/redac/thumbnail///661_primary-konicek-cidlina.jpg",
                "author" => 'Oldřich Koníček',
                "title" => 'Olše nad řekou',
                "status" => 'Hledáme',
                "description" => 'Dvojici protějškových obrazů Oldřicha Koníčka stejného rázu ze stejného roku 1930 propojuje i stejný motiv - industriální dvoupodlazní bílé stavení. Zatímco první má identifikační název (Sány ...',

            ],
            [
                "link" => "/ceska-krajina-–-sany-u-podebrad/", "img" => "/templates/artopos/images/redac/thumbnail///660_primary-konicek-sany.jpg",
                "author" => 'Oldřich Koníček',
                "title" => 'Česká krajina – Sány u Poděbrad',
                "status" => 'Hledáme',
                "description" => 'Dvojici protějškových obrazů Oldřicha Koníčka stejného rázu ze stejného roku 1930 propojuje i stejný motiv - industriální dvoupodlazní bílé stavení. Zatímco první má identifikační název (Sány ...',

            ],
            [
                "link" => "/zima-pod-emauzy/", "img" => "/templates/artopos/images/redac/thumbnail///659_primary-kotvald-emauzy.jpg",
                "author" => 'Ferdinand Kotvald',
                "title" => 'Zima pod Emauzy',
                "status" => 'Nalezeno',
                "description" => 'Nedatovaný obraz (50. léta?) zachycuje pohled ulicí Pod Slovany a dále Václavskou. Dům vlevo (323, za ním je ještě skryt 324) již nestojí, stejně jako domy ...',

            ],
            [
                "link" => "/sv-matej/", "img" => "/templates/artopos/images/redac/thumbnail///658_primary-kohout-matej.jpg",
                "author" => 'Alois Kohout',
                "title" => 'Sv. Matěj',
                "status" => 'Hledáme',
                "description" => 'Ve sbírkách GHMP je obraz veden jako Sv. Matěj, ale nejedná se o známý kostel v Dejvicích, ani o kostelík stejného zasvěcení v Horšicích na Plzeňsku. ...',

            ],
            [
                "link" => "/u-lochovic/", "img" => "/templates/artopos/images/redac/thumbnail///657_josef-ullmann-1900.jpg",
                "author" => 'Josef Ullmann',
                "title" => 'U Lochovic',
                "status" => 'Hledáme',
                "description" => 'Krajina z plenérového pobytu Mařákovy školy v Lochovicích. Malíř stál někde na břehu Litavky, v pozadí charakteristická silueta Plešivce.        ...',

            ],
            [
                "link" => "/podebrady/", "img" => "/templates/artopos/images/redac/thumbnail///656_ludvik-kuba.jpg",
                "author" => 'Ludvík Kuba',
                "title" => 'Poděbrady',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/belohrad-v-zime/", "img" => "/templates/artopos/images/redac/thumbnail///655_139919.jpg",
                "author" => 'Josef Života',
                "title" => 'Bělohrad v zimě',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/privoz-v-troji-1264/", "img" => "/templates/artopos/images/redac/thumbnail///654_139922.jpg",
                "author" => 'Oldřich Kerhart',
                "title" => 'Přívoz v Troji',
                "status" => 'Nalezeno',
                "description" => 'Trojský přívoz mezi Císařským ostrovem a Trojou se nacházel v místě dnešní Trojské lávky. Poprvé je doložen 1892, naposledy byl v provozu 1966, kdy sloužil zejména ...',

            ],
            [
                "link" => "/pohled-na-decin/", "img" => "/templates/artopos/images/redac/thumbnail///653_139918.jpg",
                "author" => 'Josef Stegl',
                "title" => 'Pohled na Děčín',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/krajina-z-benesovska/", "img" => "/templates/artopos/images/redac/thumbnail///652_139908.jpg",
                "author" => 'Ladislav Šíma',
                "title" => 'Krajina z Benešovska',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-mladou-boleslav/", "img" => "/templates/artopos/images/redac/thumbnail///651_139914.jpg",
                "author" => 'Pavel Hložek',
                "title" => 'Pohled na Mladou Boleslav',
                "status" => 'Hledáme',
                "description" => 'Pohled na jihozápad, směrem k hradu.                     ...',

            ],
            [
                "link" => "/setkani-v-parku-hertlov-v-noci/", "img" => "/templates/artopos/images/redac/thumbnail///650_139947.jpg",
                "author" => 'Oldřich Kerhart',
                "title" => 'Setkání v parku (Hertlov v noci)',
                "status" => 'Hledáme',
                "description" => 'Zámeček Hertlov v Těšnovicích. "Když v roce 1860 Bedřich Hertl zakoupil zámeček pro svou dceru Klaudii, nechal kromě rotundy přistavět další obytné místnosti a hospodářské budovy, ...',

            ],
            [
                "link" => "/pohled-na-domazlice/", "img" => "/templates/artopos/images/redac/thumbnail///649_139941.jpg",
                "author" => 'František Michl',
                "title" => 'Pohled na Domažlice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostelik-na-pradle-1252/", "img" => "/templates/artopos/images/redac/thumbnail///648_primary-kohout-pradle.jpg",
                "author" => 'Alois Kohout',
                "title" => 'Kostelík Na prádle',
                "status" => 'Nalezeno',
                "description" => 'Vše podstatné o tomto motivu bylo řečeno u obrazu malovaného ze stejného místa od mnohem slavnějšího Karla Holana (viz záložka dole). Holan ho maloval v roce ...',

            ],
            [
                "link" => "/privoz-v-troji/", "img" => "/templates/artopos/images/redac/thumbnail///646_primary-kars-privoz-troja.jpg",
                "author" => 'Jiří (Georg) Kars',
                "title" => 'Přívoz v Troji',
                "status" => 'Hledáme',
                "description" => 'Obraz je nazvaný Přívoz v Troji a měl by tudíž znázorňovat osadu Rybáře sousedící se zahradou Trojského zámku, kde však byla situace změněna po stavbě lávky ...',

            ],
            [
                "link" => "/prejezd-na-maninach/", "img" => "/templates/artopos/images/redac/thumbnail///644_primary-viadukt-maniny.jpg",
                "author" => 'Petr Kameníček',
                "title" => 'Přejezd na Maninách',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/nemocnice-ve-stresovicich/", "img" => "/templates/artopos/images/redac/thumbnail///642_primary-nemocnice-ve-stresovicich.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Nemocnice ve Střešovicích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kluziste-na-letne/", "img" => "/templates/artopos/images/redac/thumbnail///641_primary-jira-letna.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Kluziště na Letné',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zima-v-lipanech/", "img" => "/templates/artopos/images/redac/thumbnail///632_primary-lipany.jpg",
                "author" => 'Rudolf Jindřich',
                "title" => 'Zima v Lipanech',
                "status" => 'Hledáme',
                "description" => 'Jedná se o Lipany - dnešní část Prahy, nikoliv ty slavné v okrese Kolín. V jejich okolí (Benice, Kolovraty) autor opakovaně maloval.     ...',

            ],
            [
                "link" => "/dejvicke-nadrazi/", "img" => "/templates/artopos/images/redac/thumbnail///628_primary-holub-dejviceke.jpg",
                "author" => 'Václav Holub',
                "title" => 'Dejvické nádraží',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/karlov/", "img" => "/templates/artopos/images/redac/thumbnail///627_primary-holan-karlov.jpg",
                "author" => 'Karel Holan',
                "title" => 'Karlov',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/divoka-orlice-v-potstyne/", "img" => "/templates/artopos/images/redac/thumbnail///625_primary-potstyn-holan.jpg",
                "author" => 'František Holan',
                "title" => 'Divoká Orlice v Potštýně',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ze-stare-prahy-1236/", "img" => "/templates/artopos/images/redac/thumbnail///624_primary-hlavin.jpg",
                "author" => 'Jindřich Hlavín',
                "title" => 'Ze staré Prahy',
                "status" => 'Hledáme',
                "description" => 'Mostek na špici Kampy, který se objevuje i na  obraze Jaroslava Šetelíka.               ...',

            ],
            [
                "link" => "/klinec/", "img" => "/templates/artopos/images/redac/thumbnail///623_92778-klinec.jpg",
                "author" => 'Bohumil (Bohuš) Čížek',
                "title" => 'Klínec',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/u-bulhara/", "img" => "/templates/artopos/images/redac/thumbnail///622_primary-cizek-u-bulkahara.jpg",
                "author" => 'Bohumil (Bohuš) Čížek',
                "title" => 'U Bulhara',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/u-libenskeho-pristavu/", "img" => "/templates/artopos/images/redac/thumbnail///621_primary-dolezel-liben.jpg",
                "author" => 'František Doležal',
                "title" => 'U libeňského přístavu',
                "status" => 'Nalezeno',
                "description" => 'Pohled na Vltavu z Bílé skály.                     ...',

            ],
            [
                "link" => "/anezsky-dvur/", "img" => "/templates/artopos/images/redac/thumbnail///620_primary-1-anezsky-dvur.jpg",
                "author" => 'Bohuslav Dvořák',
                "title" => 'Anežský dvůr',
                "status" => 'Nalezeno',
                "description" => 'Pohled na starou gotickou věž Anežského kláštera z druhého nádvoří. Pohled dnes zakrývá loď kostela sv. Františka, nově vztyčená při renovaci.      ...',

            ],
            [
                "link" => "/predjari-v-havlickovych-sadech/", "img" => "/templates/artopos/images/redac/thumbnail///618_primary-1-fucik-havlickovy-sady.jpg",
                "author" => 'Josef Fučík',
                "title" => 'Předjaří v Havlíčkových sadech',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zeleznicni-most-v-holesovicich/", "img" => "/templates/artopos/images/redac/thumbnail///617_primary-1-zeleznicni-most-hole.jpg",
                "author" => 'Arnošt Folprecht',
                "title" => 'Železniční most v Holešovicích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zavory-v-dejvicich/", "img" => "/templates/artopos/images/redac/thumbnail///616_primary-zavory-dejvice.jpg",
                "author" => 'Arnošt Folprecht',
                "title" => 'Závory v Dejvicích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/nabrezi-v-holesovicich/", "img" => "/templates/artopos/images/redac/thumbnail///614_primary-1-holesovice-folprecht.jpg",
                "author" => 'Arnošt Folprecht',
                "title" => 'Nábřeží v Holešovicích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ripska-ulice/", "img" => "/templates/artopos/images/redac/thumbnail///612_primary-1-ripska.jpg",
                "author" => 'Arnošt Folprecht',
                "title" => 'Řipská ulice',
                "status" => 'Nalezeno',
                "description" => 'Vlevo bývalé kino Slavie (Řipská 24), dnes billiardová herna.                  ...',

            ],
            [
                "link" => "/most-na-svamberku/", "img" => "/templates/artopos/images/redac/thumbnail///611_primary-opocno-most.jpg",
                "author" => 'Alois Fišárek',
                "title" => 'Most na Švamberku',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/opocno-svamberk/", "img" => "/templates/artopos/images/redac/thumbnail///610_primary-svamberk-opocno.jpg",
                "author" => 'Alois Fišárek',
                "title" => 'Opočno - Švamberk',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/nadvori-strakonickeho-hradu/", "img" => "/templates/artopos/images/redac/thumbnail///609_primary-strakonice.jpg",
                "author" => 'Stanislav Feikl',
                "title" => 'Nádvoří strakonického hradu',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/anezsky-klaster-1215/", "img" => "/templates/artopos/images/redac/thumbnail///607_primary-feikl-anezsky.jpg",
                "author" => 'Stanislav Feikl',
                "title" => 'Anežský klášter',
                "status" => 'Nalezeno',
                "description" => 'Feikl své obrazy nedatoval a tenhle mohl vzniknout v širokém časovém rozpětí někdy mezi lety 1910 - 1940. Jde o pohled Klášterní uličkou před jejím zabočením ...',

            ],
            [
                "link" => "/stara-praha-rasnovka/", "img" => "/templates/artopos/images/redac/thumbnail///606_primary-feikl-rasnovka.jpg",
                "author" => 'Stanislav Feikl',
                "title" => 'Stará Praha (Řásnovka)',
                "status" => 'Nalezeno',
                "description" => 'Branka domu U Roubíčků, který stál uvnitř dvora a byl zbořen 1940. Géza Včelička v knize Pražské tajemství uvádí, že zde pan Burda prodával uhlí a ...',

            ],
            [
                "link" => "/perucovka/", "img" => "/templates/artopos/images/redac/thumbnail///605_primary-perucovka.jpg",
                "author" => 'Emanuel Famíra',
                "title" => 'Perucovka',
                "status" => 'Nalezeno',
                "description" => 'Pohled na Strojní přádelnu, tkalcovnu a barvírnu Bratři Perutzové v Libni, která stávala na jižním okraji libeňského ghetta mezi Voctářovou a Vojenovou ulicí, od bývalého Dolního nádraží v ...',

            ],
            [
                "link" => "/krizovatka/", "img" => "/templates/artopos/images/redac/thumbnail///604_primary-famir-a-hlubocepy.jpg",
                "author" => 'Emanuel Famíra',
                "title" => 'Křižovatka',
                "status" => 'Nalezeno',
                "description" => 'Pohled na hlubočepský viadukt ze skály lomu.                    ...',

            ],
            [
                "link" => "/pohled-na-klimkovice/", "img" => "/templates/artopos/images/redac/thumbnail///603_primary-klimkovice.jpg",
                "author" => 'neznámý autor',
                "title" => 'Pohled na Klimkovice',
                "status" => 'Hledáme',
                "description" => 'Přestože ve sbírkách GHMP je obraz veden jako Pohled na Klinkovice, jedná se o Klimkovice, německy Königsberg, ležící při jihozápadním okraji Ostravy s dominantou kostela sv. ...',

            ],
            [
                "link" => "/karlovy-vary-drahovice/", "img" => "/templates/artopos/images/redac/thumbnail///602_135.jpg",
                "author" => 'Max Struppe',
                "title" => 'Karlovy Vary-Drahovice',
                "status" => 'Hledáme',
                "description" => 'Obraz zobrazující roh ulice Vrchlického a 5.května pochází z autorova ateliéru v Karlových Varech-Drahovicích.             ...',

            ],
            [
                "link" => "/zimni-krajina/", "img" => "/templates/artopos/images/redac/thumbnail///601_primary-zeezny-brod.jpg",
                "author" => 'Vlastimil Rada',
                "title" => 'Zimní krajina',
                "status" => 'Nalezeno',
                "description" => 'Kostel sv. Jakuba Většího v Železném Brodě přes řeku Jizeru. Stejný motiv ze stejného místa zachytil v roce 1972 Josef Jíra.      ...',

            ],
            [
                "link" => "/rasnovka/", "img" => "/templates/artopos/images/redac/thumbnail///600_primary-rasnovka.jpg",
                "author" => 'Jan Rambousek',
                "title" => 'Řásnovka',
                "status" => 'Nalezeno',
                "description" => 'Branka domu U Roubíčků, který stál uvnitř dvora a byl zbořen 1940. Géza Včelička v knize Pražské tajemství uvádí, že zde pan Burda prodával uhlí a ...',

            ],
            [
                "link" => "/modranska-periferie/", "img" => "/templates/artopos/images/redac/thumbnail///599_primary-rambousek-modrany.jpg",
                "author" => 'Jan Rambousek',
                "title" => 'Modřanská periferie',
                "status" => 'Hledáme',
                "description" => 'Modřany jsou součástí Prahy a podoba místa bude zcela jiná než na tomto obraze pocházejícím nejspíš z doby mezi dvěma válkami. Co to však je za ...',

            ],
            [
                "link" => "/zelena-lavka/", "img" => "/templates/artopos/images/redac/thumbnail///598_69734.jpg",
                "author" => 'Jan Rambousek',
                "title" => 'Železná lávka',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/den-detske-radosti/", "img" => "/templates/artopos/images/redac/thumbnail///597_primary-nemec-sidliste.jpg",
                "author" => 'Josef Němec',
                "title" => 'Den dětské radosti',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/na-vltave-u-branika/", "img" => "/templates/artopos/images/redac/thumbnail///596_primary-nejedly-branik.jpg",
                "author" => 'Otakar Nejedlý',
                "title" => 'Na Vltavě u Bráníka',
                "status" => 'Nalezeno',
                "description" => 'Motiv cementárny v Podolí na místě dnešního plaveckého stadionu. Ze stejného místa přes Vltavu ho zachytil František Charvát, drobný žlutý dům vlevo (ve skutečnosti má dva ...',

            ],
            [
                "link" => "/pohled-z-hlavni-tridy-na-budovu-stareho-pivovaru-v-braniku/", "img" => "/templates/artopos/images/redac/thumbnail///595_primary-smloik-rajsky.jpg",
                "author" => 'Emanuel Smolík - Rajský',
                "title" => 'Starý pivovar v Braníku',
                "status" => 'Nalezeno',
                "description" => 'Pohed do Branické ulice, vlevo Dominikánský dvůr se zvonicí, kde se nacházel tzv. Starý pivovar. Poloha byla zhruba určena, zbývá doplnit současné, případně historické foto.  ...',

            ],
            [
                "link" => "/most-pres-certovku/", "img" => "/templates/artopos/images/redac/thumbnail///594_primary-setelik-most-pres-certovku.jpg",
                "author" => 'Jaroslav Šetelík',
                "title" => 'Most přes Čertovku',
                "status" => 'Nalezeno',
                "description" => 'Původní podoba soutoku Čertovky a Vltavy; zahrada patřila k Michnovu paláci a následně tedy k c. k. zbrojnici, která v něm sídlila od roku 1767.  ...',

            ],
            [
                "link" => "/jablonne/", "img" => "/templates/artopos/images/redac/thumbnail///591_206sedlon.jpg",
                "author" => 'Otakar Sedloň',
                "title" => 'Jablonné',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/vrch-sv-krize-zidovske-pece/", "img" => "/templates/artopos/images/redac/thumbnail///590_primary-stretti-zidovske-pece.jpg",
                "author" => 'Viktor Stretti',
                "title" => 'Vrch sv. Kříže (Židovské pece)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/letenska-plan/", "img" => "/templates/artopos/images/redac/thumbnail///588_primary-gross-letenska.jpg",
                "author" => 'František Gross',
                "title" => 'Letenská  pláň',
                "status" => 'Hledáme',
                "description" => 'Monumentální obraz, na němž najdeme i průvod s rudým praporm směřující asi na nějakou demonstraci, jaké byly v té době součástí každodenního života, namaloval Gross v ...',

            ],
            [
                "link" => "/srni/", "img" => "/templates/artopos/images/redac/thumbnail///587_d_98785.jpg",
                "author" => 'Jaromír Seidl',
                "title" => 'Srní',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/molitorovsky-zamecek/", "img" => "/templates/artopos/images/redac/thumbnail///586_v_7716.jpg",
                "author" => 'Jaroslav Novák',
                "title" => 'Molitorovský zámeček',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostelec-nad-orlici-1181/", "img" => "/templates/artopos/images/redac/thumbnail///585_as_13877.jpg",
                "author" => 'Václav Formánek',
                "title" => 'Kostelec nad Orlicí',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/bardejov/", "img" => "/templates/artopos/images/redac/thumbnail///584_128029.jpg",
                "author" => 'Jan Pašek',
                "title" => 'Bardejov',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/liben-u-libere/", "img" => "/templates/artopos/images/redac/thumbnail///583_primary-liben-u-libere.jpg",
                "author" => 'Jan Pašek',
                "title" => 'Libeň u Jílového',
                "status" => 'Hledáme',
                "description" => 'Obraz z Paškovy rodné Libně u Libeře.                    ...',

            ],
            [
                "link" => "/malenice-na-sumave/", "img" => "/templates/artopos/images/redac/thumbnail///581_primary-malenice.jpg",
                "author" => 'Jaromír Jindra',
                "title" => 'Malenice na Šumavě',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zima-v-zabehlicich/", "img" => "/templates/artopos/images/redac/thumbnail///580_primary-xhravatt-zabehlice.jpg",
                "author" => 'František Charvát',
                "title" => 'Zima v Záběhlicích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zima-v-horanech/", "img" => "/templates/artopos/images/redac/thumbnail///579_primary-lander.jpg",
                "author" => 'Richard Lander',
                "title" => 'Zima v Hořanech',
                "status" => 'Hledáme',
                "description" => 'Existuje několik Hořan, tyhle leží nejspíš v okrese Nymburk.                  ...',

            ],
            [
                "link" => "/stara-vyton/", "img" => "/templates/artopos/images/redac/thumbnail///578_79-soter-voasek.jpg",
                "author" => 'Soter Vonášek',
                "title" => 'Stará Výtoň',
                "status" => 'Nalezeno',
                "description" => 'Scéna z pražského Podskalí, které téměř celé podlehlo asanaci. proto je s podivem, že někter domy na obraze dosud stojí. Dům s oknem ve štítu je ...',

            ],
            [
                "link" => "/nabrezi-u-seiny/", "img" => "/templates/artopos/images/redac/thumbnail///577_primary_web-soter-seiny.jpg",
                "author" => 'Soter Vonášek',
                "title" => 'Nábřeží u Seiny',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/brevnov-1166/", "img" => "/templates/artopos/images/redac/thumbnail///576_primary_web-benes-brevnov.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Břevnov',
                "status" => 'Nalezeno',
                "description" => 'Malíř stál v uličce mezi tehdejšími zdmi staré zástavby pod břevnovskou návsí. Obraz je zřejmě z 20.let. Vlevo je roh tehdejšího domu čp.18, zbořeného 1952, kdy ...',

            ],
            [
                "link" => "/kostelec-nad-orlici/", "img" => "/templates/artopos/images/redac/thumbnail///575_primary_web-longen-kostelec.jpg",
                "author" => 'Emil Artur Pittermann (Longen)',
                "title" => 'Kostelec nad Orlicí',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/brana-v-pachaticich/", "img" => "/templates/artopos/images/redac/thumbnail///574_primary-havelka.jpg",
                "author" => 'Roman Havelka',
                "title" => 'Brána v Prachaticích',
                "status" => 'Hledáme',
                "description" => 'Nedatovaný pohled na Dolní, tzv. Píseckou bránu monumentálních rozměrů.                  ...',

            ],
            [
                "link" => "/kostel-sv-frantiska/", "img" => "/templates/artopos/images/redac/thumbnail///573_primary-benes-frantisek.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Kostel sv. Františka',
                "status" => 'Nalezeno',
                "description" => 'Pohled na Anežský klášter od východu - správněji by měl název znít: Kostely sv. Salvátora (vpravo) a sv. Františka. Obraz z doby stavby (nebo spíše jejího ...',

            ],
            [
                "link" => "/zne-v-katovicich/", "img" => "/templates/artopos/images/redac/thumbnail///572_primary_web-ullyrch.jpg",
                "author" => 'Bohumil Ullrych',
                "title" => 'Žně v Katovicích',
                "status" => 'Nalezeno',
                "description" => 'Malíř si svůj stojan postavil u cesty cesty podél Otavy. Výhled dnes cloní novostavby z  doby po roce 2000, ale kostelní věž a střecha jeho lodi ...',

            ],
            [
                "link" => "/skola/", "img" => "/templates/artopos/images/redac/thumbnail///571_primary-vejrych.jpg",
                "author" => 'Rudolf Vejrych',
                "title" => 'Škola',
                "status" => 'Nalezeno',
                "description" => 'Rudolf Vejrych zde zobrazil funkcionalistickou budovu Veřejné obchodní školy, dnešní Obchodní akademie, v Praze na Vinohradech na rohu Vinohradské a Blanické ulice, postavenou v roce 1925 ...',

            ],
            [
                "link" => "/podolske-cementarny/", "img" => "/templates/artopos/images/redac/thumbnail///568_e44af5db32fd6c4acbc094327c34a99a.jpg",
                "author" => 'František Zikmund',
                "title" => 'Podolské cementárny',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/areal-klastera-sv-anezky/", "img" => "/templates/artopos/images/redac/thumbnail///570_primary-hejna.jpg",
                "author" => 'Václav Hejna',
                "title" => 'Areál kláštera sv. Anežky',
                "status" => 'Nalezeno',
                "description" => 'Obraz s motivem dnes neexistujícího bloku domů Na Františku zobrazuje dům č. 829 s obrysem zbořeného domu č. 828 na fasádě; vpravo je zeď Anežského kláštera ...',

            ],
            [
                "link" => "/libenske-ghetto/", "img" => "/templates/artopos/images/redac/thumbnail///569_primary-ghetto.jpg",
                "author" => 'František Zikmund',
                "title" => 'Libeňské ghetto',
                "status" => 'Nalezeno',
                "description" => 'Blok nejstarších židovských domů ve Voctářově ulici z 2. pol. 17. století a z roku 1840. Vpravo začíná dnešní Koželužská ul., dříve Riegrova.  Domy byly odstraněny ...',

            ],
            [
                "link" => "/vesnice-na-klatovsku/", "img" => "/templates/artopos/images/redac/thumbnail///565_screenshot-2014-08-19-19.jpg",
                "author" => 'František Zikmund',
                "title" => 'Vesnice na Klatovsku',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/most-sully-v-parizi/", "img" => "/templates/artopos/images/redac/thumbnail///564_screenshot-2014-08-19-19.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Most v Paříži',
                "status" => 'Hledáme',
                "description" => 'Obraz evidovaný v GHMP jako Most Suzzy v Paříži - ovšem žádný takový most tu neexistuje. Nabízelo by se, že jde o šptaně přečtený název Pont ...',

            ],
            [
                "link" => "/jaro-na-pelc-tyrolce/", "img" => "/templates/artopos/images/redac/thumbnail///563_screenshot-2014-08-19-19.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Jaro na Pelc-Tyrolce',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/motiv-ze-sarky/", "img" => "/templates/artopos/images/redac/thumbnail///562_screenshot-2014-08-19-18.jpg",
                "author" => 'Karel Holan',
                "title" => 'Motiv ze Šárky',
                "status" => 'Hledáme',
                "description" => 'Pohled na trafostanici (?) s kostelem sv. Matěje v Šárce.                 ...',

            ],
            [
                "link" => "/u-sv-apolinare/", "img" => "/templates/artopos/images/redac/thumbnail///561_screenshot-2014-08-19-17.jpg",
                "author" => 'Karel Holan',
                "title" => 'U sv. Apolináře',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostel-ve-stolmiri/", "img" => "/templates/artopos/images/redac/thumbnail///560_o_33_orez.jpg",
                "author" => 'Antonín Slavíček',
                "title" => 'Kostel ve Štolmíři',
                "status" => 'Nalezeno',
                "description" => 'Na jedné ze svých slavných dřevěných destiček ze sbírky Augusta Švagrovského, dnes uložených v roudnické galerii, zachytil Slavíček Kostel sv. Havla ve Štolmíři, odkud pocházela Slavíčkova ...',

            ],
            [
                "link" => "/prijezd-na-most-eliscin-most/", "img" => "/templates/artopos/images/redac/thumbnail///558_o_19_orez.jpg",
                "author" => 'Antonín Slavíček',
                "title" => 'Příjezd na most (Eliščin most)',
                "status" => 'Nalezeno',
                "description" => 'V roce 1906 Slavíček hned dvakrát zobrazil předpolí Mostu Františka Josefa I., lidově zvaného podle jeho manželky Albžběty Eliščin most (Alžběta=Elisabeth), z vyvýšeného místa - cesty ...',

            ],
            [
                "link" => "/karlin/", "img" => "/templates/artopos/images/redac/thumbnail///556_27508.jpg",
                "author" => 'Vlastimil Beneš',
                "title" => 'Viadukty na Žižkově',
                "status" => 'Nalezeno',
                "description" => 'Pohled do Husitské ulice na Žižkově na dva železniční viadukty, tzv. Velkou  a Malou Hrabovku. Spodní byl součástí tzv. Hrabovské spojky a byl odstraněn v roce ...',

            ],
            [
                "link" => "/zampassky-viadukt/", "img" => "/templates/artopos/images/redac/thumbnail///555_47316.jpg",
                "author" => 'Jan Slavíček',
                "title" => 'Žampašský viadukt',
                "status" => 'Nalezeno',
                "description" => 'Jan Slavíček tu zobrazil nejvyšší kamenný železniční most v Čechách, zprovozněný v roce 1900. Nejspíš stál na protější straně Kocourovského údolí, než je zobrazená část mostu, které ...',

            ],
            [
                "link" => "/na-stvanici/", "img" => "/templates/artopos/images/redac/thumbnail///554_o_21_orez.jpg",
                "author" => 'Antonín Slavíček',
                "title" => 'Na Štvanici',
                "status" => 'Nalezeno',
                "description" => 'Na jednom ze svých skvostných drobných olejů malovaných na dřevěné destičky, na nichž skicovitým způsobem dokázal vystihnout atmosféru často pochmurného počasí, zobrazil Slavíček dřevěný most na ...',

            ],
            [
                "link" => "/z-male-strany/", "img" => "/templates/artopos/images/redac/thumbnail///553_spala-z-male-starny.jpg",
                "author" => 'Václav Špála',
                "title" => 'Z Malé Strany',
                "status" => 'Hledáme',
                "description" => 'Motiv dle názvu "z Malé Strany" může být zábavným rébusem a bylo by zajímavé porovnat, jak si Špála přizpůsobil realitu svému kubistickému krajinářství. Ale mezi architektonickými ...',

            ],
            [
                "link" => "/kostel-ve-fecamp/", "img" => "/templates/artopos/images/redac/thumbnail///552_o_34_orez.jpg",
                "author" => 'Antonín Slavíček',
                "title" => 'Kostel ve Fécamp',
                "status" => 'Nalezeno',
                "description" => 'V roce 1907, kdy mu bylo již 37 let, odjel Slavíček na svou první významnější studijní cestu do Francie v rámci stipendia K. Mohr-Piepenhagenové. Z cesty ...',

            ],
            [
                "link" => "/krajina-v-srbsku/", "img" => "/templates/artopos/images/redac/thumbnail///549_spala-krajina-v-srbsku.jpg",
                "author" => 'Václav Špála',
                "title" => 'Krajina v Srbsku',
                "status" => 'Nalezeno',
                "description" => 'Pohled na Lom Srbsko Pražská železářská ze sbírek Alšovy jihočeské galerie (název, pod nímž je ve sbírkách, je poněkud zavádějící - lépe asi Krajina u Srbska); ...',

            ],
            [
                "link" => "/zbraslavska-silnice/", "img" => "/templates/artopos/images/redac/thumbnail///548_o_634_benes.jpg",
                "author" => 'Vlastimil Beneš',
                "title" => 'Zbraslavská silnice',
                "status" => 'Nalezeno',
                "description" => 'Dnes již neexistující silnice a topolová alej mezi Lahovicemi a Zbraslaví (s průhledem na věž tamního zámku) zanikla vybudováním výpadovky na Strakonice.     ...',

            ],
            [
                "link" => "/mestecko-u-reky/", "img" => "/templates/artopos/images/redac/thumbnail///547_6ub084092.jpg",
                "author" => 'A. Posselt',
                "title" => 'Městečko u řeky',
                "status" => 'Hledáme',
                "description" => 'Městečko či vesnice, nejspíš v severních Čechách, by mělo být lehce identifikovatelné podle řeky a barokního kostelíku.          ...',

            ],
            [
                "link" => "/motiv-z-krkokonos/", "img" => "/templates/artopos/images/redac/thumbnail///545_a-55162-1.jpg",
                "author" => 'A. Posselt',
                "title" => 'Motiv z Krkokonoš',
                "status" => 'Nalezeno',
                "description" => 'Na obrazu je chalupa čp. 115 v Obřím dole (Pec pod Sněžkou, osada Obří Důl), místním známá jako Rubena (bývalo to rekreační zařízení tohoto podniku). V ...',

            ],
            [
                "link" => "/pecinsky-kostelik/", "img" => "/templates/artopos/images/redac/thumbnail///544_o_280_trampota.jpg",
                "author" => 'Jan Trampota',
                "title" => 'Pěčínský kostelík',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/kromeriz-cukrovar-v-kampani/", "img" => "/templates/artopos/images/redac/thumbnail///543_zezula055.jpg",
                "author" => 'Oldřich Zezula',
                "title" => 'Kroměříž - Cukrovar v kampani',
                "status" => 'Hledáme',
                "description" => 'Dílo už v duchu poválečné sorely, byť teprve z roku 1948.                ...',

            ],
            [
                "link" => "/vitkovice/", "img" => "/templates/artopos/images/redac/thumbnail///542_baran054.jpg",
                "author" => 'Otakar Baran',
                "title" => 'Vysoká pec v Třinci',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/prerov-namesti/", "img" => "/templates/artopos/images/redac/thumbnail///541_137790.jpg",
                "author" => 'Antonín Kubát',
                "title" => 'Přerov - náměstí',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zakouti-na-klarove/", "img" => "/templates/artopos/images/redac/thumbnail///540_69612.jpg",
                "author" => 'Josef Tomáš Blažek',
                "title" => 'Zákoutí na Klárově',
                "status" => 'Hledáme',
                "description" => 'Staropražské zákoutí na dnes zcela přestavěném Klárově.                    ...',

            ],
            [
                "link" => "/vesnice-1109/", "img" => "/templates/artopos/images/redac/thumbnail///539_48063.jpg",
                "author" => 'Robert Piesen',
                "title" => 'Vesnice s mostem',
                "status" => 'Hledáme',
                "description" => 'Neznámou vesnici by mělo jít určit podle výrazného klenutého mostu.                 ...',

            ],
            [
                "link" => "/bastia/", "img" => "/templates/artopos/images/redac/thumbnail///538_dsc_7653.jpg",
                "author" => 'Ludwig Püschel',
                "title" => 'Bastia',
                "status" => 'Nalezeno',
                "description" => 'Bastia byla dlouho hlavním městem Korsiky, teprve v roce 181 ho Napoleon přenesl do svého rodiště Ajaccia. Dominantou města založeného Janovany je citadela nad starým přístavem.
Obraz ...',

            ],
            [
                "link" => "/podolska-cementarna/", "img" => "/templates/artopos/images/redac/thumbnail///537_495ad86cfae6dcf72c76304c19e90a19.jpg",
                "author" => 'Otakar Marvánek',
                "title" => 'Podolská cementárna',
                "status" => 'Nalezeno',
                "description" => 'Pohled na dnes neexistující Podolskou cementárnu na místě dnešního plaveckého stadionu. V centru Marvánkova obrazu je budova kruhové pece s komínem na polygonálním půdorysu. Na historické ...',

            ],
            [
                "link" => "/u-sv-apolinare-jedova-chyse/", "img" => "/templates/artopos/images/redac/thumbnail///536_bb94a55dbe0f9f9c7244b17923a339f1.jpg",
                "author" => 'Emil Artur Pittermann (Longen)',
                "title" => 'U sv. Apolináře - Jedová chýše',
                "status" => 'Nalezeno',
                "description" => 'Jeden z malířů Osmy Emil Artur Pittermann - Longen tu zobrazil prostranství na křížení ulic Apolinářská a Viničná se svatovojtěšským sloupem a v pozadí s legendární ...',

            ],
            [
                "link" => "/ronde-lutherse-kerk-v-amsterdamu/", "img" => "/templates/artopos/images/redac/thumbnail///535_135710.jpg",
                "author" => 'František Tavík Šimon',
                "title" => 'Ronde Lutherse Kerk v Amsterdamu',
                "status" => 'Nalezeno',
                "description" => 'Ronde Lutherse Kerk je bývalý luteránský kostel u kanálu Singel. Vyznačuje se svou výraznou kupolí pokrytou měděným plechem. Od roku 1935 slouží pouze jako koncertní síň. ...',

            ],
            [
                "link" => "/vesnice-s-baroknim-kostelikem/", "img" => "/templates/artopos/images/redac/thumbnail///534_34205.jpg",
                "author" => 'Pavel Šimon',
                "title" => 'Vesnice s barokním kostelíkem',
                "status" => 'Hledáme',
                "description" => 'Neznámá vesnice s barokním kostelem.                      ...',

            ],
            [
                "link" => "/trebon-nadrazi/", "img" => "/templates/artopos/images/redac/thumbnail///533_87407.jpg",
                "author" => 'Pavel Šimon',
                "title" => 'Třeboň - nádraží',
                "status" => 'Nalezeno',
                "description" => 'Strážní domek u vlakového nádraží v Třeboni (pohled na východ). Patrová budova stále stojí, budovy vlevo jsou přestavěné.         ...',

            ],
            [
                "link" => "/prazska-ulicka-1093/", "img" => "/templates/artopos/images/redac/thumbnail///532_100.jpg",
                "author" => 'Bohumil Stanislav Urban',
                "title" => 'Pražská ulička',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/z-hlubocep/", "img" => "/templates/artopos/images/redac/thumbnail///531_291novak.jpg",
                "author" => 'Václav Vojtěch Novák',
                "title" => 'Z Hlubočep',
                "status" => 'Hledáme',
                "description" => 'Obraz nejspíš vznikl ve 40. letech a koresponduje tak s historickou fotografií z roku 1949; na ní je patrná ona dlouhá zeď s domem vlevo, stejně ...',

            ],
            [
                "link" => "/barrandov-prazsky-semmering/", "img" => "/templates/artopos/images/redac/thumbnail///530_117863.jpg",
                "author" => 'Václav Vojtěch Novák',
                "title" => 'Barrandov (Pražský Semmering)',
                "status" => 'Hledáme',
                "description" => 'Monumentální obraz, jehož název je narážkou na známý viadukt proslulé rakouské horské železnice.              ...',

            ],
            [
                "link" => "/pohled-na-caslavdadas/", "img" => "/templates/artopos/images/redac/thumbnail///529_88939.jpg",
                "author" => 'Václav Vojtěch Novák',
                "title" => 'Pohled na Čáslav',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/skleniky-budiskovice/", "img" => "/templates/artopos/images/redac/thumbnail///527_27563.jpg",
                "author" => 'Jánuš Kubíček',
                "title" => 'Skleníky (Budíškovice)',
                "status" => 'Hledáme',
                "description" => 'Copak to je za zvláštní skleníky? A co vůbec dělal Jánuš Kubíček v roce 1945 v jihočeských Budíškovicích?         ...',

            ],
            [
                "link" => "/kunstat-na-morave/", "img" => "/templates/artopos/images/redac/thumbnail///526_38976.jpg",
                "author" => 'Jánuš Kubíček',
                "title" => 'Kunštát na Moravě',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/benzinova-pumpa/", "img" => "/templates/artopos/images/redac/thumbnail///524_114853.jpg",
                "author" => 'Dobroslav Foll',
                "title" => 'Benzínová pumpa',
                "status" => 'Hledáme',
                "description" => 'Kdepak stála tahle krásná benzínka...                      ...',

            ],
            [
                "link" => "/pohled-na-pisek/", "img" => "/templates/artopos/images/redac/thumbnail///522_46756.jpg",
                "author" => 'Václav Jansa',
                "title" => 'Pohled na Písek',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/dioklecianuv-palac-ve-splitu/", "img" => "/templates/artopos/images/redac/thumbnail///521_83566.jpg",
                "author" => 'Václav Jansa',
                "title" => 'Diokleciánův palác ve Splitu',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/malostransky-dvorek/", "img" => "/templates/artopos/images/redac/thumbnail///520_116571.jpg",
                "author" => 'Václav Jansa',
                "title" => 'Malostranský dvorek',
                "status" => 'Hledáme',
                "description" => 'Monumentální olej z pozůstalosti autora, identifikace motivu by neměla být pro znalce staré Prahy problém.            ...',

            ],
            [
                "link" => "/krajina-s-mostem-ronov-nad-sazavou/", "img" => "/templates/artopos/images/redac/thumbnail///519_49902.jpg",
                "author" => 'Antonín Chittussi',
                "title" => 'Krajina s mostem (Ronov nad Sázavou)',
                "status" => 'Nalezeno',
                "description" => 'Obraz byl v roce 2009 prodán v aukci pod nic neříkajícím názvem Cesta do vsi s údajem, že pochází z významné sbírky Bohuslava Duška. Roman Prahl ...',

            ],
            [
                "link" => "/zamek-v-jindrichove-hradci-1076/", "img" => "/templates/artopos/images/redac/thumbnail///518_134931.jpg",
                "author" => 'Antonín Chittussi',
                "title" => 'Zámek v Jindřichově Hradci',
                "status" => 'Nalezeno',
                "description" => 'Jeden ze série Chittussiho poněkud akademických pohledů na česká města.                 ...',

            ],
            [
                "link" => "/orechov-ronov/", "img" => "/templates/artopos/images/redac/thumbnail///516_84035.jpg",
                "author" => 'František Foltýn',
                "title" => 'Ořechov (Ronov)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/krajina-v-braniku/", "img" => "/templates/artopos/images/redac/thumbnail///515_muzika_domy_a_stromy_v_podvecer.jpg",
                "author" => 'František Muzika',
                "title" => 'Krajina v Braníku',
                "status" => 'Hledáme',
                "description" => 'Obyčejné domy, byť názvem situované do Braníku, bude velmi těžké identifikovat.                ...',

            ],
            [
                "link" => "/paysage-tossa-de-mar/", "img" => "/templates/artopos/images/redac/thumbnail///514_50031.jpg",
                "author" => 'Jiří (Georg) Kars',
                "title" => 'Paysage - Tossa de Mar',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ze-stare-prahy-10681212/", "img" => "/templates/artopos/images/redac/thumbnail///512_132804.jpg",
                "author" => 'Jiří (Georg) Kars',
                "title" => 'Ze staré Prahy',
                "status" => 'Hledáme',
                "description" => 'Něco pro znalce staré Prahy - nádvoří domu na Starém Městě nebo Malé Straně.             ...',

            ],
            [
                "link" => "/nabytek/", "img" => "/templates/artopos/images/redac/thumbnail///511_cermakova.jpg",
                "author" => 'Alena Čermáková',
                "title" => 'Nábytek',
                "status" => 'Hledáme',
                "description" => 'Autorka spojovaná zejména ze sorelou 50. let zde v polovině 60. let zobrazila novou výstavbu - nejspíš někde v Praze...       ...',

            ],
            [
                "link" => "/vesnice/", "img" => "/templates/artopos/images/redac/thumbnail///510_6uo111183.jpg",
                "author" => 'Miroslav Pokluda',
                "title" => 'Vesnice',
                "status" => 'Hledáme',
                "description" => 'Vesnice, nejspíš z jižních Čech...                      ...',

            ],
            [
                "link" => "/cesta-do-hronova/", "img" => "/templates/artopos/images/redac/thumbnail///508_6uo111109.jpg",
                "author" => 'Zdeňka Heritesová',
                "title" => 'Cesta do Hronova',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/cementarna-v-braniku/", "img" => "/templates/artopos/images/redac/thumbnail///507_96110391683d2436967ebbc26ecbd9e5.jpg",
                "author" => 'Bohumil Kubišta',
                "title" => 'Cementárna v Braníku',
                "status" => 'Nalezeno',
                "description" => 'Na místě dnešního plaveckého stadionu v Podolí stála jedna z prvních cementáren v českých zemích, jež využívala vápenec z nedalekých branických lomů. Obě dvě lokality zachytil Bohumil Kubišta ...',

            ],
            [
                "link" => "/na-kampe/", "img" => "/templates/artopos/images/redac/thumbnail///505_dscn0881.jpg",
                "author" => 'Václav Sivko',
                "title" => 'Na Kampě',
                "status" => 'Nalezeno',
                "description" => 'Václav Sivko tu lehkou tušovou kresbou zachytil stejný motiv, který najdeme i na realistickém obraze "malíře Kampy" Jaro Procházky. Od 50. let do revoluce měl ve ...',

            ],
            [
                "link" => "/z-florencie/", "img" => "/templates/artopos/images/redac/thumbnail///503_screenshot-2014-08-06-13.jpg",
                "author" => ' ',
                "title" => 'Z Florencie',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/park-ve-florencii/", "img" => "/templates/artopos/images/redac/thumbnail///502_83569.jpg",
                "author" => 'Bohumil Kubišta',
                "title" => 'Park ve Florencii',
                "status" => 'Hledáme',
                "description" => 'Nedávno objevený pastel z florentského pobytu (1906 - 7) zobrazuje park s několika charakteristickými prvky se siluetou kostela v pozadí; nejspíš stejného, který se objevuje na ...',

            ],
            [
                "link" => "/kampa/", "img" => "/templates/artopos/images/redac/thumbnail///506_kampa.jpg",
                "author" => 'Václav Sivko',
                "title" => 'Kampa',
                "status" => 'Nalezeno',
                "description" => 'Ve čtyřicátých letech 20. století docházel do malostranského ateliéru Josefa Sudka mladý Václav Sivko. Podobně jako mnohé další i jeho podporoval slavný fotograf v uměleckém rozvoji. ...',

            ],
            [
                "link" => "/otava-v-pisku/", "img" => "/templates/artopos/images/redac/thumbnail///499_10330281_1572910959603171_8758806789909730664_n.jpg",
                "author" => 'Jaroslav Král',
                "title" => 'Otava v Písku',
                "status" => 'Hledáme',
                "description" => 'Realisticky pojatý obraz namaloval Král nejspíš ještě na akademii.                  ...',

            ],
            [
                "link" => "/vyhlidka-nad-mestem/", "img" => "/templates/artopos/images/redac/thumbnail///496_45470.jpg",
                "author" => 'Cyril Bouda',
                "title" => 'Vyhlídka nad městem',
                "status" => 'Hledáme',
                "description" => 'Pohled na neznámé české městečko.                      ...',

            ],
            [
                "link" => "/zamek-kopidlno/", "img" => "/templates/artopos/images/redac/thumbnail///495_88970.jpg",
                "author" => 'Cyril Bouda',
                "title" => 'Zámek Kopidlno',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/petrov-z-vodni-ulice/", "img" => "/templates/artopos/images/redac/thumbnail///494_48846.jpg",
                "author" => 'Josef Vaic',
                "title" => 'Petrov z Vodní ulice',
                "status" => 'Hledáme',
                "description" => 'Místo díky přesné lokalizaci přímo na grafice nebude těžké najít - jak to tam asi vypadá dnes? A kdy byla asi grafika vytvořena?    ...',

            ],
            [
                "link" => "/skola-v-hermanove-mestci/", "img" => "/templates/artopos/images/redac/thumbnail///493_27010.jpg",
                "author" => 'Willy Horný',
                "title" => 'Škola v Heřmanově Městci',
                "status" => 'Hledáme',
                "description" => 'Raná kubizující krajina, i když datace (dle signatury), podle níž by ji malíř namaloval ve věku 15 let, se jeví jako příliš časná...    ...',

            ],
            [
                "link" => "/parizsky-parlament/", "img" => "/templates/artopos/images/redac/thumbnail///492_75235.jpg",
                "author" => 'František Tichý',
                "title" => 'Pařížský parlament',
                "status" => 'Nalezeno',
                "description" => 'Tichý zde přes Seinu zobrazil pravou část průčelí Palais Bourbon, sídla francouzského parlamentu s fragmentem Pont de la Concorde, který byl jen krátce před tím (1930-32) ...',

            ],
            [
                "link" => "/v-zatacce-1038/", "img" => "/templates/artopos/images/redac/thumbnail///491_v-zatacce-debrece_55x66cm-temperapapir-1988.jpg",
                "author" => 'Vladimír Hanuš',
                "title" => 'V zatáčce',
                "status" => 'Nalezeno',
                "description" => 'Raný obraz Vladimíra Hanuše z roku, kdy byl už věku 27 let  po několika pokusech konečně přijat na pražskou akademii. Předtím si v druhé polovině 80. let ...',

            ],
            [
                "link" => "/most-v-gelnici/", "img" => "/templates/artopos/images/redac/thumbnail///490_112224.jpg",
                "author" => 'Rudolf Kremlička',
                "title" => 'Most v Gelnici',
                "status" => 'Nalezeno',
                "description" => 'V roce 1919 podnikl Rudolf Kremlička jako člen umělecké výpravy, vyslané tehdejším Památníkem odboje, cestu na slovenský venkov do oblasti kolem Spišské Nové Vsi. Zde bylo ...',

            ],
            [
                "link" => "/zricenina-hradu-1036/", "img" => "/templates/artopos/images/redac/thumbnail///489_30689.jpg",
                "author" => 'Jiří Bouda',
                "title" => 'Okoř',
                "status" => 'Nalezeno',
                "description" => 'Zřícenina hradu Okoř. Pohledu od západu do hradního jádra dominuje nezaměnitelná podoba torza věže se starší gotickou kaplí v přízemí.       ...',

            ],
            [
                "link" => "/volynska-tvrz/", "img" => "/templates/artopos/images/redac/thumbnail///488_28815.jpg",
                "author" => 'Jiří Bouda',
                "title" => 'Volyňská tvrz',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ischia-ponte-aragonsky-hrad/", "img" => "/templates/artopos/images/redac/thumbnail///487_128439.jpg",
                "author" => 'Antonín Hudeček',
                "title" => 'Ischia - Ponte, Aragonský hrad',
                "status" => 'Hledáme',
                "description" => 'Na ostrově Ischia pobýval Antonín Hudeček od února do května 1927 spolu se svým synem syn Jiřím. Namaloval zde i tuto skicu olejem na karton, podle ...',

            ],
            [
                "link" => "/jaromer/", "img" => "/templates/artopos/images/redac/thumbnail///485_a-60110-2.jpg",
                "author" => 'Václav Rytíř',
                "title" => 'Jaroměř',
                "status" => 'Hledáme',
                "description" => 'Motiv z městskou branou od známého malíře, autora knihy Malířské signatury. Vzadu štítek výstavy v Nové Pace v r. 1942 a další s datací 1937.  ...',

            ],
            [
                "link" => "/nachodsky-zamek/", "img" => "/templates/artopos/images/redac/thumbnail///484_img-1792.jpg",
                "author" => 'Jan Streer',
                "title" => 'Náchodský zámek',
                "status" => 'Hledáme',
                "description" => 'Pohled na náchodský zámek z cesty nad Novým světem v Náchodě od neznámého místního krajináře Jana Streera. Místo asi nebude těžké určit, jak však vypadá dnes...? ...',

            ],
            [
                "link" => "/veleslavin-1028/", "img" => "/templates/artopos/images/redac/thumbnail///483_img-6776.jpg",
                "author" => 'Emil Tylš',
                "title" => 'Veleslavín',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/beloveska-ulice-v-nachode/", "img" => "/templates/artopos/images/redac/thumbnail///480_img-6566-2.jpg",
                "author" => 'Bohumír Španiel',
                "title" => 'Běloveská ulice v Náchodě',
                "status" => 'Hledáme',
                "description" => 'Náchodský malíř Bohumír Španiel tu v době zuřící normalizace zachytil novou bytovou výstavbu, což díky bytostné "nemalebnosti" paneláků nebylo ani v 70. letech příliš časté téma. ...',

            ],
            [
                "link" => "/karlovo-namesti-v-nachode/", "img" => "/templates/artopos/images/redac/thumbnail///479_img-6375.jpg",
                "author" => 'Karel Šafář',
                "title" => 'Karlovo náměstí v Náchodě',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/nachod-beloves/", "img" => "/templates/artopos/images/redac/thumbnail///477_img-626a6.jpg",
                "author" => 'Emil Tylš',
                "title" => 'Náchod - Běloves',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/novy-cheb/", "img" => "/templates/artopos/images/redac/thumbnail///476_o_78.jpg",
                "author" => 'Jaroslav Pokorný',
                "title" => 'Nový Cheb',
                "status" => 'Nalezeno',
                "description" => 'Malíř, spjatý s oficiální sorelou 50. let, tu zobrazil křižovnický kostel sv. Bartoloměje rok po jeho rekonstrukci, která však souvisela i s nešťastnou sanací jeho okolí ...',

            ],
            [
                "link" => "/kostelik-na-pradle/", "img" => "/templates/artopos/images/redac/thumbnail///475_o_208_holan.jpg",
                "author" => 'Karel Holan',
                "title" => 'Kostelík Na prádle',
                "status" => 'Nalezeno',
                "description" => 'Kostelík sv. Jana Křtitele Na prádle získal své přízvisko poté, co byl za josefínských reforem zrušen a stala se z něj prádelna, později mechanická čistírna koberců. ...',

            ],
            [
                "link" => "/karolina/", "img" => "/templates/artopos/images/redac/thumbnail///471_6uo114010.jpg",
                "author" => 'Karel Říhovský',
                "title" => 'Karolina',
                "status" => 'Nalezeno',
                "description" => 'Pohled na dnes již neexistující koksovnu Karolina. Zcela vlevo funkcionalistická budova uhelného prádla, stržená na konci 80. let.
Jáma Karolina byla nejstarší ostravskou šachtou. Hloubena byla od ...',

            ],
            [
                "link" => "/vojenska-plovarna-v-pisku/", "img" => "/templates/artopos/images/redac/thumbnail///470_a-55341-2-1.jpg",
                "author" => 'Jaroslav Král',
                "title" => 'Vojenská plovárna v Písku',
                "status" => 'Nalezeno',
                "description" => 'Otec Jaroslava Krále si v Písku pronajal hospodářský dvůr a malíř zde střídavě pobýval až do roku 1916, kdy se rodina odstěhovala do Brna. Tento expresivně ...',

            ],
            [
                "link" => "/svycarsky-dum-u-luahcovic/", "img" => "/templates/artopos/images/redac/thumbnail///469_70992.jpg",
                "author" => 'Jaroslav Král',
                "title" => 'Švýcarský dům u Luhačovic',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/naves-v-hajske/", "img" => "/templates/artopos/images/redac/thumbnail///467_87105.jpg",
                "author" => 'Bohumil Ullrych',
                "title" => 'Náves v Hajské',
                "status" => 'Nalezeno',
                "description" => 'Kaplička v Hajské, malé vesnici, která je součástí Strakonic a kde mj. měl chalupu František Ronovský, který odtud čerpal své motivy z vesnického života. Ullrych se ...',

            ],
            [
                "link" => "/etretat-1006/", "img" => "/templates/artopos/images/redac/thumbnail///466_88016.jpg",
                "author" => 'Jan Trampota',
                "title" => 'Étretat',
                "status" => 'Nalezeno',
                "description" => 'Pláž v normandském městečku Étretat se skalním obloukem vybíhajícím do moře Porte d&#039;Amont. Trampota zde pobýval spolu s Rudolfem Kremličkou, který tento slavný motiv také zachytil. ...',

            ],
            [
                "link" => "/nove-hrady/", "img" => "/templates/artopos/images/redac/thumbnail///465_132834.jpg",
                "author" => 'Jan Trampota',
                "title" => 'Nové Hrady',
                "status" => 'Hledáme',
                "description" => 'Obraz z období, kdy zajížděl k sochaři Josefu Kubíčkovi do Nových Hradů u Vysokého Mýta.            ...',

            ],
            [
                "link" => "/krajina-s-lomem/", "img" => "/templates/artopos/images/redac/thumbnail///464_105_resize.jpg",
                "author" => 'Oldřich Kerhart',
                "title" => 'Krajina s lomem',
                "status" => 'Hledáme',
                "description" => 'Obraz byl vystaven na výstavě Národ svým výtvarným umělcům v roce 1942 v Praze.             ...',

            ],
            [
                "link" => "/vesnicky-kostel/", "img" => "/templates/artopos/images/redac/thumbnail///463_nejedly_church_in_the_countryside.jpg",
                "author" => 'Otakar Nejedlý',
                "title" => 'Vesnický kostel',
                "status" => 'Hledáme',
                "description" => 'Neznámý venkovský kostelík...                        ...',

            ],
            [
                "link" => "/z-palerma/", "img" => "/templates/artopos/images/redac/thumbnail///462_h0436-l09460890.jpg",
                "author" => 'Oldřich Kerhart',
                "title" => 'Z Palerma',
                "status" => 'Nalezeno',
                "description" => 'Obraz z Kerhartovy cesty na Sicílii v roce 1927 zobrazuje kostel San Giovanni degli Eremiti s výraznými červenými kupolemi v duchu normansko-arabské architetury 12. st. Vznikl ...',

            ],
            [
                "link" => "/krajina-u-zelezneho-brodu/", "img" => "/templates/artopos/images/redac/thumbnail///461_o_160_rada.jpg",
                "author" => 'Vlastimil Rada',
                "title" => 'Krajina u Železného Brodu',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pod-slovany/", "img" => "/templates/artopos/images/redac/thumbnail///460_10357594_436261796515939_7782674528504409307_n.jpg",
                "author" => 'František Vobecký',
                "title" => 'Pod Slovany',
                "status" => 'Nalezeno',
                "description" => 'Pohled do Trojické ulici s kostelem Nejsvětější Trojice je dnes takřka stejný jako na akvarelu Fr. Vobeckého z 50. let (?). Pouze zmizel funkcionalistický portálek se ...',

            ],
            [
                "link" => "/ulicka-pod-karlovem/", "img" => "/templates/artopos/images/redac/thumbnail///459_id-8212.jpg",
                "author" => 'František Vobecký',
                "title" => 'Ulička pod Karlovem',
                "status" => 'Nalezeno',
                "description" => 'Krásný obraz, krásné místo, stranou ruchu města a turismu. Klasický kandelábr z obrazu už tu sice nenajdeme a místo něj stojí moderní lampa, ozdobné zábradlí nahradilo ...',

            ],
            [
                "link" => "/silnice-od-zelezneho-brodu/", "img" => "/templates/artopos/images/redac/thumbnail///457_47931.jpg",
                "author" => 'Vlastimil Rada',
                "title" => 'Silnice od Železného Brodu',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/mustek/", "img" => "/templates/artopos/images/redac/thumbnail///456_40145.jpg",
                "author" => 'Jan Smetana',
                "title" => 'Můstek',
                "status" => 'Nalezeno',
                "description" => 'Kresba z konce 40. let ještě v duchu Smetanovy tvorby z období Skupiny 42 zobrazuje spodní stranu Václavského náměstí s pohledem do ulice 28. října.  ...',

            ],
            [
                "link" => "/kostelik-a-zvonice-v-nepreveci/", "img" => "/templates/artopos/images/redac/thumbnail///455_16611.jpg",
                "author" => 'Jan Smetana',
                "title" => 'Kostelík a zvonice v Nepřívěci',
                "status" => 'Nalezeno',
                "description" => 'V 50. letech maloval Jan Smetana krajiny v pseudoimpresionistické manýře, jako je tato krajina z Nepřívěce ve východních Čechách s barokním průčelím kostela Nalezení svatého Kříže ...',

            ],
            [
                "link" => "/alej-v-luhacovicich/", "img" => "/templates/artopos/images/redac/thumbnail///454_135968.jpg",
                "author" => 'Stanislav Lolek',
                "title" => 'Alej v Luhačovicích',
                "status" => 'Nalezeno',
                "description" => 'Tato Alej v Luhačovicích je alej vedoucí k Lázeňskému divadlu,místo se myslím moc nezměnilo,i když v lázních proběhly rozsáhlé sadové úpravy v minulých letech.   ...',

            ],
            [
                "link" => "/na-rudaku-pred-bellevue/", "img" => "/templates/artopos/images/redac/thumbnail///453_71022.jpg",
                "author" => 'Bořek Bayer',
                "title" => 'Na Ruďáku před Bellevue',
                "status" => 'Hledáme',
                "description" => 'Dynamicky pojatý pohled na Moravské náměstí, kdsi nám. Rudé armády, od kavárny Bellevue.              ...',

            ],
            [
                "link" => "/vltavsky-breh/", "img" => "/templates/artopos/images/redac/thumbnail///451_116088.jpg",
                "author" => 'František Viktor Mokrý',
                "title" => 'Vltavský břeh',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/belcicka-humna/", "img" => "/templates/artopos/images/redac/thumbnail///450_86221.jpg",
                "author" => 'František Viktor Mokrý',
                "title" => 'Bělčická humna',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/stary-suchdol/", "img" => "/templates/artopos/images/redac/thumbnail///449_48969.jpg",
                "author" => 'František Viktor Mokrý',
                "title" => 'Starý Suchdol',
                "status" => 'Hledáme',
                "description" => 'Malíř se evidentně nebál vpustit do obrazu realitu a namixoval nám tu venkov na periferii Prahy někdy v 60. letech... Máme tu koně i bagr, vedle ...',

            ],
            [
                "link" => "/cejc/", "img" => "/templates/artopos/images/redac/thumbnail///448_70987.jpg",
                "author" => 'František Myslivec',
                "title" => 'Čejč',
                "status" => 'Hledáme',
                "description" => 'Pohled na humna malé vesnice nedaleko Brna.                    ...',

            ],
            [
                "link" => "/pohled-na-vltavu-z-letne/", "img" => "/templates/artopos/images/redac/thumbnail///447_41344.jpg",
                "author" => 'Václav Hejna',
                "title" => 'Pohled na Vltavu z Letné',
                "status" => 'Nalezeno',
                "description" => 'V 50. letech maloval Hejna svým expresivním stylem krajiny pro obživu, nejčastěji z Prahy a blízkého okolí. Zde je zobrazen pohled na Vltavu z Letné zhruba ...',

            ],
            [
                "link" => "/moje-ceska-chaloupka-na-krivoklate-vecerni-oblaka/", "img" => "/templates/artopos/images/redac/thumbnail///446_o_618_kalvoda.jpg",
                "author" => 'Alois Kalvoda',
                "title" => 'Moje česká chaloupka na Křivoklátě',
                "status" => 'Nalezeno',
                "description" => 'Alois Kalvoda zde namaloval svůj domek, tzv. chaloupku, na Křivoklátě v sousedství domku houslového virtuoza Ferdinanda Lauba, který r. 1905 zakoupil a kde v roce 1912 ...',

            ],
            [
                "link" => "/posledni-klekani-972/", "img" => "/templates/artopos/images/redac/thumbnail///445_o_397_hutta.jpg",
                "author" => 'Bojmír Hutta',
                "title" => 'Poslední klekání',
                "status" => 'Hledáme',
                "description" => 'Krajina se zvláštním symbolickým názvem má sice v evidenci chebské galerie dataci 1944, ale špatně rozeznatelná signatura je zde evidentně špatně přečtená. Podle hrázděných domů se ...',

            ],
            [
                "link" => "/kutna-hora-969/", "img" => "/templates/artopos/images/redac/thumbnail///444_d_99529.jpg",
                "author" => 'Karel Oberthor',
                "title" => 'Kutná Hora',
                "status" => 'Hledáme',
                "description" => 'Krajinu jsme zařadili spíše kvůli scenérii v popředí obrazu, věrně tlumočící celkovou situaci, než kvůli konvenčnímu pohledu na chrám sv. Barbory...      ...',

            ],
            [
                "link" => "/rusna-ulice-v-brne/", "img" => "/templates/artopos/images/redac/thumbnail///443_1159-frantisek-bilkovsky-rusna-ulice-v-brne-1159.jpg",
                "author" => 'František Bílkovský',
                "title" => 'Rušná ulice v Brně',
                "status" => 'Hledáme',
                "description" => 'Obraz nejasné datace zachycuje nějaké místo v centru Brna - jak místo, tak i datace asi lze rozorem scény blíže určit.      ...',

            ],
            [
                "link" => "/z-poohri/", "img" => "/templates/artopos/images/redac/thumbnail///442_16072014558.jpg",
                "author" => 'Boris Soukup',
                "title" => 'Z Poohří',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/krajina-s-viaduktem/", "img" => "/templates/artopos/images/redac/thumbnail///441_78330.jpg",
                "author" => 'Emil Filla',
                "title" => 'Krajina s viaduktem',
                "status" => 'Nalezeno',
                "description" => 'Poněkud atypická Fillova krajina s hlubočepským viaduktem se před časem objevila na aukčním trhu. V nabídce se odkazovalo na na výstavu Neznámý Emil Filla v roce ...',

            ],
            [
                "link" => "/krajina/", "img" => "/templates/artopos/images/redac/thumbnail///440_130262.jpg",
                "author" => 'Emil Filla',
                "title" => 'Krajina',
                "status" => 'Hledáme',
                "description" => 'Expresivní ranná krajina Emila Filly.                      ...',

            ],
            [
                "link" => "/rokytnice/", "img" => "/templates/artopos/images/redac/thumbnail///439_o_319_trampota_rokytnice.jpg",
                "author" => 'Jan Trampota',
                "title" => 'Rokytnice',
                "status" => 'Hledáme',
                "description" => 'Krajina z meziválečného období - v roce 1923 se Trampota usadil v nedalekém Pěčíně...             ...',

            ],
            [
                "link" => "/spalicek-v-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///473_o_394.jpg",
                "author" => 'Viktor Stretti',
                "title" => 'Špalíček v Chebu',
                "status" => 'Nalezeno',
                "description" => 'V roce 1955, dva roky před svou smrtí, namaloval stařičký Viktor Stretti v duchu své topografické malby i chebský Špalíček. Nevíme, co ho k tomu vedlo ...',

            ],
            [
                "link" => "/park-ve-veltrusich/", "img" => "/templates/artopos/images/redac/thumbnail///437_o_382_spala.jpg",
                "author" => 'Václav Špála',
                "title" => 'Park ve Veltrusích',
                "status" => 'Nalezeno',
                "description" => 'Drobnější, skicovitě pojatý obrázek zobrazuje zadní stranu zámku otevřenou dlouhému průhledu v anglickém parku až k malému rybníčku.         ...',

            ],
            [
                "link" => "/na-rece-otave/", "img" => "/templates/artopos/images/redac/thumbnail///434_o_211.jpg",
                "author" => 'Václav Špála',
                "title" => 'Na řece Otavě',
                "status" => 'Nalezeno',
                "description" => 'Další ze Špálových slavných pohledů na Otavu. Obraz byl pořízen z levého břehu Otavy cca 6 km pod Pískem v lokalitě chatové osady Krkáčov (http://krkacov.euweb.cz/), která ...',

            ],
            [
                "link" => "/marseille/", "img" => "/templates/artopos/images/redac/thumbnail///433_5278.jpg",
                "author" => 'Václav Špála',
                "title" => 'Marseille',
                "status" => 'Hledáme',
                "description" => 'Akvarel z pobřeží s výraznými architektonickými motivy by nemělo být těžké určit.               ...',

            ],
            [
                "link" => "/mestecko-na-jadranskem-pobrezi/", "img" => "/templates/artopos/images/redac/thumbnail///431_46687.jpg",
                "author" => 'Václav Špála',
                "title" => 'Městečko na jadranském pobřeží',
                "status" => 'Hledáme',
                "description" => 'Městčko v Dalmácii, zachycené na Špálu až v nezvykle "vedutovém" rázu.                ...',

            ],
            [
                "link" => "/na-komornim-hradku/", "img" => "/templates/artopos/images/redac/thumbnail///430_60387.jpg",
                "author" => 'Václav Špála',
                "title" => 'Na Komorním Hrádku',
                "status" => 'Hledáme',
                "description" => 'Jedno z okruhu děl z okolí Vysoké Lhoty v Posázaví, kde Špála koupil dům pro letní pobyty zobrazuje motiv ze zámeckého parku.     ...',

            ],
            [
                "link" => "/mlyn-na-otave/", "img" => "/templates/artopos/images/redac/thumbnail///429_58824.jpg",
                "author" => 'Václav Špála',
                "title" => 'Mlýn na Otavě',
                "status" => 'Nalezeno',
                "description" => 'Jez stále existuje, mlýn už ne, byl zbořen při napouštění přehrady v roce 1960. Nejde však o Panský mlýn, ale o Tlučkův mlýn, a v době ...',

            ],
            [
                "link" => "/pysely-od-vily-ing-heraina/", "img" => "/templates/artopos/images/redac/thumbnail///428_98433.jpg",
                "author" => 'Václav Špála',
                "title" => 'Pyšely od vily Ing. Heraina',
                "status" => 'Hledáme',
                "description" => 'Špála do okolí Pyšel zajížděl, našel zde motivy pro řadu obrazů a ve čtyřicátých letech zde i bydlel - v nedaleké Vysoké Lhotě, kde si koupil ...',

            ],
            [
                "link" => "/vltava-u-chvaterub/", "img" => "/templates/artopos/images/redac/thumbnail///426_90234.jpg",
                "author" => 'Václav Špála',
                "title" => 'Vltava u Chvatěrub',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/lomy-u-srbska/", "img" => "/templates/artopos/images/redac/thumbnail///425_112253.jpg",
                "author" => 'Václav Špála',
                "title" => 'Lomy u Srbska',
                "status" => 'Nalezeno',
                "description" => 'Ve dvacátých letech Špála opakovaně maloval údolí Berounky u Srbska, včetně zdejších lomů. Tento je asi největší, přesně se nazývá Lom Srbsko Pražská železářská.   ...',

            ],
            [
                "link" => "/u-reky-938/", "img" => "/templates/artopos/images/redac/thumbnail///424_121188.jpg",
                "author" => 'Václav Špála',
                "title" => 'U řeky (Berounka u Srbska)',
                "status" => 'Hledáme',
                "description" => 'Evidentně jde o Berounku u Srbska. Dílo pochází původně z majetku mlynáře Haranta ze Žeretic u Vysokého Veselí.         ...',

            ],
            [
                "link" => "/heydluv-dum-a-kostel-sv-jakuba-v-prachaticich/", "img" => "/templates/artopos/images/redac/thumbnail///423_josef-krejsa-widok-na-kamienice-heydlow-i-kosciol-sw-jakuba-w-prachaticach-_928.jpg",
                "author" => 'Josef Krejsa',
                "title" => 'Heydlův dům v Prachaticích',
                "status" => 'Hledáme',
                "description" => 'Pohled Dolní branou na historický Heydlův dům (Kostelní náměstí 29) s renesančními sgrafity. Dům s gotickým jádrem byl po roce 1540 renesančně přestavěn.     ...',

            ],
            [
                "link" => "/pohled-na-slany/", "img" => "/templates/artopos/images/redac/thumbnail///422_54154.jpg",
                "author" => 'Josef Krejsa',
                "title" => 'Pohled na Slaný',
                "status" => 'Hledáme',
                "description" => 'Krejsa, který většinu života strávil v Husinci, měl k Slanému velmi specifický vztah. Popsal ho v dopise místnímu organizátorovi výtvarného živta Josefu Čížkovi z 10. 12. ...',

            ],
            [
                "link" => "/husuv-kraj/", "img" => "/templates/artopos/images/redac/thumbnail///421_67735.jpg",
                "author" => 'Josef Krejsa',
                "title" => 'Husův kraj',
                "status" => 'Hledáme',
                "description" => 'Šumavská krajina se vsí, jak název napovídá nejspíš z okolí Husince, kde se Krejsa narodil a kde působil po první světové válce až do své předčasné ...',

            ],
            [
                "link" => "/hvar/", "img" => "/templates/artopos/images/redac/thumbnail///420_dsc_7651.jpg",
                "author" => 'Kurt Halleger',
                "title" => 'Hvar',
                "status" => 'Nalezeno',
                "description" => 'Hvar je historické město na stejnojmenném ostrově. Hallegger zde zobrazil pohled od jedné z dominant města - Vévodského paláce (Kneževa palača, dnes hotel Palace) přes chráněný ...',

            ],
            [
                "link" => "/u-plzenskeho-pivovaru/", "img" => "/templates/artopos/images/redac/thumbnail///419_81241-1.jpg",
                "author" => 'Svatopluk Janke',
                "title" => 'U Plzeňského pivovaru',
                "status" => 'Nalezeno',
                "description" => 'Drobná popisná veduta zobrazuje zasněženou cestu k soutoku Radbuzy a Mže podél stadionu Viktorie Plzeň, tzv. Štruncovy sady. Přes řeku jsou vidět budovy pivovaru.   ...',

            ],
            [
                "link" => "/split/", "img" => "/templates/artopos/images/redac/thumbnail///416_dsc_7647.jpg",
                "author" => 'Kurt Halleger',
                "title" => 'Split',
                "status" => 'Nalezeno',
                "description" => 'Hallegger zde v expresivním kokoschkovském stylu zobrazil splitské nábřeží (Riva) s Monumentální fontánou z 80. let 19. st. (Nejen že byla monumentální, také se tak oficiálně ...',

            ],
            [
                "link" => "/piskari-na-labi/", "img" => "/templates/artopos/images/redac/thumbnail///415_162.jpg",
                "author" => 'Felix Bibus',
                "title" => 'Pískaři na Labi',
                "status" => 'Nalezeno',
                "description" => 'V knize Malíři Litoměřicka, Oldřich Doskočil, svazek 5, Okresní vlastivědné muzeum Litoměřice, 2000, s. 69, kde je uveřejněna skica k obrazu, se píše: "Podle Kletzlova popisu ...',

            ],
            [
                "link" => "/dubrovnik-od-jihu/", "img" => "/templates/artopos/images/redac/thumbnail///414_dsc_7641.jpg",
                "author" => 'Felix Bibus',
                "title" => 'Dubrovník od jihu',
                "status" => 'Hledáme',
                "description" => 'Pohled na moře u Dubrovniku.                      ...',

            ],
            [
                "link" => "/pohled-na-jested/", "img" => "/templates/artopos/images/redac/thumbnail///413_dsc_7606.jpg",
                "author" => 'Heinrich Hönich',
                "title" => 'Pohled na Ještěd',
                "status" => 'Hledáme',
                "description" => 'V roce 1912 byla zprovozněna tramvajová trať do Dolního a Horního Hanychova, tj. té části Liberce ležící na úpatí Ještědu. Hönich, rodák z Dolního Hanychova, vytvořil ...',

            ],
            [
                "link" => "/kapucinsky-klaster-ve-znojme/", "img" => "/templates/artopos/images/redac/thumbnail///412_dsc_7620.jpg",
                "author" => 'Anton Bruder',
                "title" => 'Kapucínský klášter ve Znojmě',
                "status" => 'Nalezeno',
                "description" => 'Dvě historické dominanty Znojma: průčelí kapucínského kostela sv. Jana Křtitele a Vlkova věž v pozadí, poslední zbytek městského opevnění. Obraz v naivizující stylizaci výborně vystihuje atmosféru ...',

            ],
            [
                "link" => "/trapani/", "img" => "/templates/artopos/images/redac/thumbnail///411_dsc_7643.jpg",
                "author" => 'Maxim Kopf',
                "title" => 'Trapani',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/praha-od-krizovniku/", "img" => "/templates/artopos/images/redac/thumbnail///410_dsc_7622.jpg",
                "author" => 'Oskar Kokoschka',
                "title" => 'Praha od Křižovníků',
                "status" => 'Hledáme',
                "description" => 'Pohled od kláštera Křižovníků na Karlův most a Malou Stranu.                 ...',

            ],
            [
                "link" => "/loketske-podhradi/", "img" => "/templates/artopos/images/redac/thumbnail///409_2.jpg",
                "author" => 'Petr Jaroš',
                "title" => 'Loketské podhradí',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/ze-stareho-bubence/", "img" => "/templates/artopos/images/redac/thumbnail///408_havelka___z_bubence_1923.jpg",
                "author" => 'Roman Havelka',
                "title" => 'Ze starého Bubenče',
                "status" => 'Nalezeno',
                "description" => 'Stará Bubeneč se proměnila a také tyhle domy už neexistují. Místo lze určit jen přibližně podle věže kostela, kterou dnes je možné vidět pouze v průhledu ...',

            ],
            [
                "link" => "/corstyn-na-dyji/", "img" => "/templates/artopos/images/redac/thumbnail///406_38421.jpg",
                "author" => 'Roman Havelka',
                "title" => 'Corštýn na Dyji',
                "status" => 'Hledáme',
                "description" => 'Další z Havelkových pohledů na Cornštejn, tenokrát se zákrutem  Dyje.                 ...',

            ],
            [
                "link" => "/hrad-cornstejn-903/", "img" => "/templates/artopos/images/redac/thumbnail///405_66434.jpg",
                "author" => 'Roman Havelka',
                "title" => 'Hrad Cornštejn',
                "status" => 'Hledáme',
                "description" => 'Oblíbený Havelkův motiv, tentokrát z větší dálky.                    ...',

            ],
            [
                "link" => "/na-zeletavce/", "img" => "/templates/artopos/images/redac/thumbnail///404_65873.jpg",
                "author" => 'Roman Havelka',
                "title" => 'Na Želetavce',
                "status" => 'Hledáme',
                "description" => 'Želetavka pod Bítovem je dalším oblíbeným Havelkovým motivem.  Tuto krajinnou partii, byť podle autorovy zvyklosti není členěna žádným výraznějším krajinným motivem, který by ji blíže určoval, ...',

            ],
            [
                "link" => "/hrad-bitov-900/", "img" => "/templates/artopos/images/redac/thumbnail///403_109655.jpg",
                "author" => 'Roman Havelka',
                "title" => 'Hrad Cornštejn',
                "status" => 'Nalezeno',
                "description" => 'Na aukčním trhu se obraz objevil pod názvem Hrad Bítov, ve skutečnosti jde ovšem o pohled na Cornštejn, který se nachází kilometr a půl od něj ...',

            ],
            [
                "link" => "/chalupy/", "img" => "/templates/artopos/images/redac/thumbnail///402_1.jpg",
                "author" => 'Vladimír Hroch',
                "title" => 'Chalupy',
                "status" => 'Hledáme',
                "description" => 'Venkovský motiv nejspíš ze Zlínska, kde autor působil.                   ...',

            ],
            [
                "link" => "/venkovska-idyla/", "img" => "/templates/artopos/images/redac/thumbnail///401_44742.jpg",
                "author" => 'Vladimír Hroch',
                "title" => 'Venkovská idyla',
                "status" => 'Hledáme',
                "description" => 'Neznámá vesnice nejspíš odněkud ze Zlínského kraje, kde působil.                  ...',

            ],
            [
                "link" => "/zelezarny-v-moravske-ostrave/", "img" => "/templates/artopos/images/redac/thumbnail///400_78357.jpg",
                "author" => 'František Čihák',
                "title" => 'Železárny na Kladně',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-dul-zofinka-v-ostrave/", "img" => "/templates/artopos/images/redac/thumbnail///399_94083.jpg",
                "author" => 'Jaroslav Vacek',
                "title" => 'Pohled na důl Žofinka v Ostravě',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/dul-na-ostravsku/", "img" => "/templates/artopos/images/redac/thumbnail///398_93725.jpg",
                "author" => 'neznámý autor',
                "title" => 'Důl na Ostravsku',
                "status" => 'Hledáme',
                "description" => 'Neznámý autor, také určení, pod nímž se objevilo na aukčním trhu, nemusí být přesné.             ...',

            ],
            [
                "link" => "/dedovice-u-pisku/", "img" => "/templates/artopos/images/redac/thumbnail///397_239_foto1.jpg",
                "author" => 'Alfréd Šnábl',
                "title" => 'Dědovice u Písku',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-reku/", "img" => "/templates/artopos/images/redac/thumbnail///396_79483.jpg",
                "author" => 'Alfréd Šnábl',
                "title" => 'Pohled na řeku',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/rataje-nad-sazavou/", "img" => "/templates/artopos/images/redac/thumbnail///395_28560.jpg",
                "author" => 'Karel Formánek',
                "title" => 'Rataje nad Sázavou',
                "status" => 'Hledáme',
                "description" => 'Pohled na Rataje nad Sázavou (okres Kutná Hora) od jihu. Vlevo nahoře je kostel sv. Matouše, uprostřed bývalý hrad Pirkštejn, později fara pronajatá nyní soukromému uživateli, ...',

            ],
            [
                "link" => "/z-tisnova/", "img" => "/templates/artopos/images/redac/thumbnail///394_28709.jpg",
                "author" => 'Karel Formánek',
                "title" => 'Z Tišnova',
                "status" => 'Nalezeno',
                "description" => 'Nedatovaný motiv s tišnovským klášterem.                      ...',

            ],
            [
                "link" => "/vesnice-s-kostelikem/", "img" => "/templates/artopos/images/redac/thumbnail///393_35184.jpg",
                "author" => 'Karel Formánek',
                "title" => 'Vítochov',
                "status" => 'Nalezeno',
                "description" => 'Pohled na kostel sv. Michaela ve Vítochově.                    ...',

            ],
            [
                "link" => "/tovarna/", "img" => "/templates/artopos/images/redac/thumbnail///392_28707.jpg",
                "author" => 'Karel Formánek',
                "title" => 'Továrna',
                "status" => 'Hledáme',
                "description" => 'Neznámý motiv, nejspíš z Brna či Tišnova.                    ...',

            ],
            [
                "link" => "/brno-petrov/", "img" => "/templates/artopos/images/redac/thumbnail///391_28706.jpg",
                "author" => 'Karel Formánek',
                "title" => 'Brno - Petrov',
                "status" => 'Hledáme',
                "description" => 'Panorama Petrova, zajímavější je však situace v popředí, a to i díky přesné dataci do roku 1951.          ...',

            ],
            [
                "link" => "/z-grandu/", "img" => "/templates/artopos/images/redac/thumbnail///390_92069.jpg",
                "author" => 'Bohumír Matal',
                "title" => 'Z Grandu',
                "status" => 'Hledáme',
                "description" => 'Kresba připsaná Bohumilu Matalovi pochází z pozůstalosti historika umění Jana Marii Tomeše. Není jasné, zda název je odvozen z přípisu na kresbě.     ...',

            ],
            [
                "link" => "/strakov/", "img" => "/templates/artopos/images/redac/thumbnail///389_screenshot-2014-07-04-22.jpg",
                "author" => 'Josef Matička',
                "title" => 'Strakov',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/lubna/", "img" => "/templates/artopos/images/redac/thumbnail///388_screenshot-2014-07-04-22.jpg",
                "author" => 'Josef Matička',
                "title" => 'Lubná',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/partie-z-rakovnika/", "img" => "/templates/artopos/images/redac/thumbnail///387_38729.jpg",
                "author" => 'Josef Král',
                "title" => 'Partie z Rakovníka',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/partie-z-liberce/", "img" => "/templates/artopos/images/redac/thumbnail///386_61372.jpg",
                "author" => 'Franz Xaver Weidinger',
                "title" => 'Partie z Liberce',
                "status" => 'Hledáme',
                "description" => 'Lidové sady v Liberci u zoologické zahrady.                    ...',

            ],
            [
                "link" => "/cesta-k-samotam-babylon-na-sumave/", "img" => "/templates/artopos/images/redac/thumbnail///385_98729.jpg",
                "author" => 'Petr Jaroš',
                "title" => 'Cesta k samotám - Babylon na Šumavě',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/z-prazskych-vinohrad/", "img" => "/templates/artopos/images/redac/thumbnail///382_61156.jpg",
                "author" => 'Jaroslav Šimůnek',
                "title" => 'Z pražských Vinohrad',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-kunetickou-horu/", "img" => "/templates/artopos/images/redac/thumbnail///381_83955.jpg",
                "author" => 'Jaroslav Šimůnek',
                "title" => 'Pohled na Kunětickou horu',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/z-periferiedsfsdfs/", "img" => "/templates/artopos/images/redac/thumbnail///380_75892.jpg",
                "author" => 'Jaroslav Šimůnek',
                "title" => 'Z pražské periferie',
                "status" => 'Hledáme',
                "description" => 'Pražské předměstí s michelskou vodárenskou věží na obzoru, nejspíš viadukt v Nuslích přes Botič.             ...',

            ],
            [
                "link" => "/venkovsky-motiv-chodsko/", "img" => "/templates/artopos/images/redac/thumbnail///377_117897.jpg",
                "author" => 'Václav Malý',
                "title" => 'Venkovský motiv (Chodsko?)',
                "status" => 'Hledáme',
                "description" => 'Neznámý a dnes asi těžko identifikovatelný venkovský motiv, nejspíš z Chodska, kde Malý působil.             ...',

            ],
            [
                "link" => "/kamenicky-u-trhove-kamenice/", "img" => "/templates/artopos/images/redac/thumbnail///376_1.jpg",
                "author" => 'Václav Malý',
                "title" => 'Kameničky (u Trhové Kamenice)',
                "status" => 'Hledáme',
                "description" => 'Také malíř Chodska Václav Malý maloval v Kameničkách,                   ...',

            ],
            [
                "link" => "/blatna/", "img" => "/templates/artopos/images/redac/thumbnail///375_screenshot-2014-06-30-15.jpg",
                "author" => 'Lev Šimák',
                "title" => 'Blatná',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/stanice-lhotka-u-melnika/", "img" => "/templates/artopos/images/redac/thumbnail///374_screenshot-2014-06-30-15.jpg",
                "author" => 'Lev Šimák',
                "title" => 'Stanice Lhotka u Mělnika',
                "status" => 'Nalezeno',
                "description" => 'Stanici se podařilo určit, už jen doplnit současné foto z pohledu malíře. Podle fotografií, které jsou na webu, se zdá, že menší budovy vlevo od stanice ...',

            ],
            [
                "link" => "/kostelik-u-susice/", "img" => "/templates/artopos/images/redac/thumbnail///373_screenshot-2014-06-30-15.jpg",
                "author" => 'Lev Šimák',
                "title" => 'Kostelík u Sušice',
                "status" => 'Nalezeno',
                "description" => 'Pohled na kostelík sv. Vavřince ve Zdouni u Tedražic.                  ...',

            ],
            [
                "link" => "/motiv-z-jizni-francie/", "img" => "/templates/artopos/images/redac/thumbnail///372_47273.jpg",
                "author" => 'Lev Šimák',
                "title" => 'Motiv z Toskánska',
                "status" => 'Hledáme',
                "description" => 'Typická toskánská krajina, poměrně velkého formátu.                     ...',

            ],
            [
                "link" => "/motiv-z-italie/", "img" => "/templates/artopos/images/redac/thumbnail///370_126925.jpg",
                "author" => 'Lev Šimák',
                "title" => 'Motiv z Itálie',
                "status" => 'Hledáme',
                "description" => 'Neznámé městečko v Itálii, nicméně pro znalce by neměl být problém je určit podle historických budov.           ...',

            ],
            [
                "link" => "/motiv-z-tabora/", "img" => "/templates/artopos/images/redac/thumbnail///369_134803.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Motiv z Tábora',
                "status" => 'Hledáme',
                "description" => 'Zajímavý výškový pohled (z okna?) na motiv z historického centra Tábora.                ...',

            ],
            [
                "link" => "/burgas/", "img" => "/templates/artopos/images/redac/thumbnail///368_137930.jpg",
                "author" => 'Vilém K. Hanuš',
                "title" => 'Burgas',
                "status" => 'Hledáme',
                "description" => 'Motiv z jedné z přímořských destinací, kam Češi za normalizace mohli.                ...',

            ],
            [
                "link" => "/motiv-z-pribora/", "img" => "/templates/artopos/images/redac/thumbnail///367_137947.jpg",
                "author" => 'Vilém K. Hanuš',
                "title" => 'Motiv z Příbora',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/klatovy/", "img" => "/templates/artopos/images/redac/thumbnail///366_81332.jpg",
                "author" => 'neznámý autor',
                "title" => 'Klatovy',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/klatovyaaa/", "img" => "/templates/artopos/images/redac/thumbnail///365_81482.jpg",
                "author" => 'Jan Charles Vondrouš',
                "title" => 'Klatovy',
                "status" => 'Hledáme',
                "description" => 'Panoramatický pohled, který pak vyšel i na pohlednici.                   ...',

            ],
            [
                "link" => "/letni-den-rozdalovice/", "img" => "/templates/artopos/images/redac/thumbnail///363_103589-1.jpg",
                "author" => 'Antonín Majer',
                "title" => 'Letní den (Rožďalovice)',
                "status" => 'Nalezeno',
                "description" => 'Letní scéna u říčky Mrliny. V pozadí lobkovický zámek a kostel sv. Havla.              ...',

            ],
            [
                "link" => "/josefovska-ulice/", "img" => "/templates/artopos/images/redac/thumbnail///362_112785.jpg",
                "author" => 'Adolf Kohn',
                "title" => 'Josefovská ulice',
                "status" => 'Nalezeno',
                "description" => 'Josefovská ul. vedla na místě dnešní Široké ulice.                   ...',

            ],
            [
                "link" => "/prazsky-motiv/", "img" => "/templates/artopos/images/redac/thumbnail///361_126107.jpg",
                "author" => 'Adolf Kohn',
                "title" => 'Pražský motiv',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/prazska-ulicka/", "img" => "/templates/artopos/images/redac/thumbnail///360_136698.jpg",
                "author" => 'Emil Artur Pittermann (Longen)',
                "title" => 'Pražská ulička',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/mala-skala-812/", "img" => "/templates/artopos/images/redac/thumbnail///359_137444.jpg",
                "author" => 'Friedrich Feigl',
                "title" => 'Malá Skála',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pristav-ve-wismaru/", "img" => "/templates/artopos/images/redac/thumbnail///358_44919.jpg",
                "author" => 'Friedrich Feigl',
                "title" => 'Přístav ve Wismaru',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/wismar-rozkvetla-zahrada/", "img" => "/templates/artopos/images/redac/thumbnail///357_137341.jpg",
                "author" => 'Friedrich Feigl',
                "title" => 'Wismar (Rozkvetlá zahrada)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/sokolovska-ulice-c-13-karlin/", "img" => "/templates/artopos/images/redac/thumbnail///355_68914.jpg",
                "author" => 'Josef Matička',
                "title" => 'Sokolovská ulice č. 13, Karlín',
                "status" => 'Nalezeno',
                "description" => 'Sokolovská 13 označuje jeden z domů v přední části obrazu, který se po povodni zřítil a byl s celým blokem zbořen. Dnes je na jeho místě ...',

            ],
            [
                "link" => "/namesti-v-hall-in-tirol/", "img" => "/templates/artopos/images/redac/thumbnail///354_52442.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Náměstí v Hall in Tirol',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zimni-pohled-z-meho-okna/", "img" => "/templates/artopos/images/redac/thumbnail///353_54145.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Zimní pohled z mého okna',
                "status" => 'Hledáme',
                "description" => 'Motiv z předměstí Prahy - Smíchov? -, i díky přesné dataci je z topografického hlediska velmi zajímavý.          ...',

            ],
            [
                "link" => "/pohled-na-hrady-zebrak-a-tocnik/", "img" => "/templates/artopos/images/redac/thumbnail///352_61120.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Pohled na hrady Žebrák a Točník',
                "status" => 'Hledáme',
                "description" => 'Místo, odkud autor zachytil pohled na dvojici známých hradů, nebude jistě těžké najít.              ...',

            ],
            [
                "link" => "/krizova-chodba-klastera-zlata-koruna/", "img" => "/templates/artopos/images/redac/thumbnail///351_82226.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Zlatá Koruna',
                "status" => 'Nalezeno',
                "description" => 'Pohled do křížové chodby kláštera Zlatá Koruna.                    ...',

            ],
            [
                "link" => "/rybnik-u-noveho-veseli/", "img" => "/templates/artopos/images/redac/thumbnail///350_99054.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Rybník u Nového Veselí',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/dravce-na-slovensku/", "img" => "/templates/artopos/images/redac/thumbnail///349_114765.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Dravce na Slovensku',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zima-na-periferii-prahy/", "img" => "/templates/artopos/images/redac/thumbnail///348_115175.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Zima na periferii Prahy',
                "status" => 'Hledáme',
                "description" => 'Nápis na obraze ("Podhájem") snad označuje hostinec v pozadí. Není jasné, zda jde o ulici Pod hájem v  Praze na Klamovce.      ...',

            ],
            [
                "link" => "/posledni-snih-v-rozne/", "img" => "/templates/artopos/images/redac/thumbnail///347_116578.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Poslední sníh v Rožné',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-radcana-v-pabenicich/", "img" => "/templates/artopos/images/redac/thumbnail///346_121202.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Pohled na Radčana v Paběnicích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/vzpominka-na-radesin/", "img" => "/templates/artopos/images/redac/thumbnail///345_122894.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Vzpomínka na Radešín',
                "status" => 'Hledáme',
                "description" => 'V Radešínské Svratce nedaleko Radešína se narodila Blažíčkova matka. Na obraze společnost - možná zámecká, v Radešíně zámek byl - při plavbě na lodičce.   ...',

            ],
            [
                "link" => "/krajina-s-kostelikem-792/", "img" => "/templates/artopos/images/redac/thumbnail///343_109376.jpg",
                "author" => 'Otakar Nejedlý',
                "title" => 'Krajina s kostelíkem',
                "status" => 'Hledáme',
                "description" => 'Neznámý kostelík s farou v sousedství má natolik výraznou siluetu a pozici v krajině, že by nemělo být nemožné ho určit. Zde jsem opravdu zvědavý...  ...',

            ],
            [
                "link" => "/na-novem-svete/", "img" => "/templates/artopos/images/redac/thumbnail///342_117343.jpg",
                "author" => 'Jan B. Minařík',
                "title" => 'Na Novém Světě',
                "status" => 'Nalezeno',
                "description" => 'Pražský motiv z Nového Světa; domy dodnes zůstaly téměř přesně v tom stavu jako v době vzniku obrazu.         ...',

            ],
            [
                "link" => "/ulicka-v-plavu/", "img" => "/templates/artopos/images/redac/thumbnail///341_117378.jpg",
                "author" => 'Josef Ullmann',
                "title" => 'Ulička v Plavu',
                "status" => 'Hledáme',
                "description" => 'Vesnický motiv; lze očekávat, že scenérie už dnes bude zcela proměněná, nicméně díky tomu, že známe jméno vesnice, by nemuselo být až tak obtížné ji identifikovat. ...',

            ],
            [
                "link" => "/pohled-do-krajiny/", "img" => "/templates/artopos/images/redac/thumbnail///340_117398.jpg",
                "author" => 'Alois Kalvoda',
                "title" => 'Pohled do krajiny',
                "status" => 'Hledáme',
                "description" => 'Monumentální krajinná panoramata jsou pro Kalvodu typická. Motiv dosud neurčen, dle svého charakteru snad z jižní Moravy.          ...',

            ],
            [
                "link" => "/zima-v-krusovicich/", "img" => "/templates/artopos/images/redac/thumbnail///339_116944.jpg",
                "author" => 'Václav Rabas',
                "title" => 'Zima v Krušovicích',
                "status" => 'Hledáme',
                "description" => 'Známý a často pubikovaný obaz z Rabasova oblíbeného kraje se nacházel v majetku Karla Čapka. V soupisu díla - Václav Rabas, Jiří Kotalík, Státní nakladatelství krásné ...',

            ],
            [
                "link" => "/tovarna-v-semilech/", "img" => "/templates/artopos/images/redac/thumbnail///338_127594.jpg",
                "author" => 'Vladimír Komárek',
                "title" => 'Továrna v Semilech',
                "status" => 'Hledáme',
                "description" => 'Krajina nepatří k autorovým typickým žánrům a industriální motivy už vůbec ne. Továrna u řeky Jizery je však laděna do jeho charakteristických lyrických tónů, zároveň asi ...',

            ],
            [
                "link" => "/pohled-na-kostel-v-pabenicich/", "img" => "/templates/artopos/images/redac/thumbnail///337_127578.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Pohled na kostel v Paběnicích',
                "status" => 'Hledáme',
                "description" => 'Scénu lze dobře identifikovat podle hřbitovního kostelíku sv. Jakuba většího.                 ...',

            ],
            [
                "link" => "/pohled-na-prachatice/", "img" => "/templates/artopos/images/redac/thumbnail///336_127509.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Pohled na Prachatice',
                "status" => 'Hledáme',
                "description" => 'Jedna ze studií obrazu, který malíř vytvořil na objednávku Ministerstva školství a národní osvěty.             ...',

            ],
            [
                "link" => "/cesta/", "img" => "/templates/artopos/images/redac/thumbnail///335_spala_cesta.jpg",
                "author" => 'Václav Špála',
                "title" => 'Cesta',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/prazske-strechy/", "img" => "/templates/artopos/images/redac/thumbnail///334_50134.jpg",
                "author" => 'Jan Slavíček',
                "title" => 'Pražské střechy',
                "status" => 'Hledáme',
                "description" => 'Pod tímto názvem se obraz před časem objevil na aukčním trhu  a asi je pravděpodobné, že půjde o motiv z Prahy a konkrétně z Malé Strany. ...',

            ],
            [
                "link" => "/torbole/", "img" => "/templates/artopos/images/redac/thumbnail///333_79321.jpg",
                "author" => 'Maxim Kopf',
                "title" => 'Torbole',
                "status" => 'Hledáme',
                "description" => 'Ivo Habán ve své diplomové práci uvádí, že v Torbole u Lago di Garda pobýval kopf v roce 1932. Vytvořil zde obrazy, které byly následujícího roku ...',

            ],
            [
                "link" => "/biarritz/", "img" => "/templates/artopos/images/redac/thumbnail///332_4_kv.jpg",
                "author" => 'Ernest Neuschul',
                "title" => 'Biarritz',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/nadrazi-v-plzni/", "img" => "/templates/artopos/images/redac/thumbnail///331_137789.jpg",
                "author" => 'Jaroslav Šetelík',
                "title" => 'Nádraží v Plzni',
                "status" => 'Nalezeno',
                "description" => 'Pohled na nádraží skrz dnešní Americkou třídu; chalupy vpravo stojí na místě Mrakodrapu, který byl postaven krátce po vzniku kresby (1924), za ním Wilsonův most (1913). ...',

            ],
            [
                "link" => "/lamani-ledu-pod-bazilikou-v-trebici/", "img" => "/templates/artopos/images/redac/thumbnail///330_137909.jpg",
                "author" => 'Karel Minář',
                "title" => 'Lámání ledu pod bazilikou v Třebíči',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/otava-u-pisku/", "img" => "/templates/artopos/images/redac/thumbnail///329_122896.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Otava u Písku',
                "status" => 'Hledáme',
                "description" => 'Motiv asi nebude těžké určit -  jde o jeden z jezů na Otavě u Písku.            ...',

            ],
            [
                "link" => "/nabrezi-se-stitkovskou-vezi/", "img" => "/templates/artopos/images/redac/thumbnail///328_123497.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Šítkovská vodárenská  věž',
                "status" => 'Hledáme',
                "description" => 'Jedna z vzácných raných krajin, kde je ještě patrný doznívající kubismus. Zcela zjevně jde o Šítkovskou věž se střechou Národního divadla v pozadí. Ovšem vysoká regulace ...',

            ],
            [
                "link" => "/pisek-759/", "img" => "/templates/artopos/images/redac/thumbnail///327_123494.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Písek',
                "status" => 'Hledáme',
                "description" => 'Pohled na Písek, respektive asi na jednu z jeho okrajových částí,  v pozdním "pestrém" Benešově stylu. Určit přesné místo nebude až tak snadné, neboť se zde ...',

            ],
            [
                "link" => "/krajina-u-male-skaly/", "img" => "/templates/artopos/images/redac/thumbnail///326_132761.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Krajina u Malé Skály',
                "status" => 'Hledáme',
                "description" => 'Stejný motiv najdeme i u Friedricha Feigla.                    ...',

            ],
            [
                "link" => "/ze-starych-stresovic-norbertov/", "img" => "/templates/artopos/images/redac/thumbnail///325_131296.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Ze starých Střešovic (Norbertov)',
                "status" => 'Nalezeno',
                "description" => 'Pohled z ulice Střešovické ke kostelu Sv. Norberta přes budovu střešovické školy.               ...',

            ],
            [
                "link" => "/reka-pod-skalami/", "img" => "/templates/artopos/images/redac/thumbnail///324_46031.jpg",
                "author" => 'Friedrich Feigl',
                "title" => 'Jizera u Malé Skály',
                "status" => 'Hledáme',
                "description" => 'Motivy z Malé Skály se u Feigla pravidelně opakují. Zobrazení motivu ze stejného místa najdeme i u Vicence Beneše.        ...',

            ],
            [
                "link" => "/pohled-na-hradcany/", "img" => "/templates/artopos/images/redac/thumbnail///323_46030.jpg",
                "author" => 'Friedrich Feigl',
                "title" => 'Pohled na Hradčany',
                "status" => 'Nalezeno',
                "description" => 'Pohled na Hradčany je pravděpodobně zobrazený z Vlašské ulice vedoucí na Petřín. Střecha za zdí pak s největší pravděpodobností patří do dnešního areálů nemocnice pod Petřínem ...',

            ],
            [
                "link" => "/dubrovnik/", "img" => "/templates/artopos/images/redac/thumbnail///322_67708.jpg",
                "author" => 'Friedrich Feigl',
                "title" => 'Dubrovník',
                "status" => 'Hledáme',
                "description" => 'Feigl byl v Dubrovníku už v roce 1908, tenhle obraz však pochází z jiného pobytu - určitě tu byl v roce 1932, z toho roku pochází ...',

            ],
            [
                "link" => "/motiv-z-kampy/", "img" => "/templates/artopos/images/redac/thumbnail///321_92271.jpg",
                "author" => 'Friedrich Feigl',
                "title" => 'Motiv z Kampy',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/vyhled-do-kraje-747/", "img" => "/templates/artopos/images/redac/thumbnail///320_1-1.jpg",
                "author" => 'Josef Jambor',
                "title" => 'Výhled do kraje',
                "status" => 'Hledáme',
                "description" => 'Krajinné panorama, snad z okolí Blatin na Vysočině, kde měl chalupu.                ...',

            ],
            [
                "link" => "/emauzy/", "img" => "/templates/artopos/images/redac/thumbnail///319_1.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Emauzy',
                "status" => 'Nalezeno',
                "description" => 'Porovnáme-li tehdejší situaci se současným stavem, motiv se změnil ve dvou etapách. V té první se proměnila urbanistická situace - vzniklo náměstí Pod Emauzy architekta Bohumila ...',

            ],
            [
                "link" => "/horazdovice-z-prachne/", "img" => "/templates/artopos/images/redac/thumbnail///317_ec-4836.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Horažďovice',
                "status" => 'Nalezeno',
                "description" => 'Přestože obraz má přípis Prácheň, jedná se o pohled na Horažďovice; historický vrch Prácheň s hradem a kostelem sv. Klimenta se nachází více vpravo, mimo záběr ...',

            ],
            [
                "link" => "/silnice-z-porici-nad-sazavou-do-cercan/", "img" => "/templates/artopos/images/redac/thumbnail///316_82222.jpg",
                "author" => 'Václav Pavlík',
                "title" => 'Kostel sv. Havla v Poříčí n. S.',
                "status" => 'Nalezeno',
                "description" => 'Místní malíř Václav Pavlík tu zobrazil románský kostelík sv. Havla - jeho podoba se dodnes nezměnila, pouze apsida je jinak barevná, kostel je víc zarostlý stromy ...',

            ],
            [
                "link" => "/v-zatacce/", "img" => "/templates/artopos/images/redac/thumbnail///315_132807.jpg",
                "author" => 'Václav Pavlík',
                "title" => 'V zatáčce',
                "status" => 'Hledáme',
                "description" => 'Motiv nejspíš z okolí Poříčí nad Sázavou.                    ...',

            ],
            [
                "link" => "/pohled-z-hurky/", "img" => "/templates/artopos/images/redac/thumbnail///314_mracky.jpg",
                "author" => 'František Mracký',
                "title" => 'Pohled z Hůrky',
                "status" => 'Hledáme',
                "description" => 'Zdá se, že jde o pohled z vrchu Hůrky do širokého údolí Úhlavy směrem k Beňovům.           ...',

            ],
            [
                "link" => "/motiv-z-vratkova/", "img" => "/templates/artopos/images/redac/thumbnail///313_093.jpg",
                "author" => 'Otakar Moravec',
                "title" => 'Motiv z Vrátkova',
                "status" => 'Hledáme',
                "description" => 'Vesnická scenérie Otakara Moravce.                       ...',

            ],
            [
                "link" => "/cementarna-v-podoli-734/", "img" => "/templates/artopos/images/redac/thumbnail///312_171.jpg",
                "author" => 'František Charvát',
                "title" => 'Cementárna v Podolí',
                "status" => 'Nalezeno',
                "description" => 'Známý motiv cementárny, zachycený hned několika malíři.                    ...',

            ],
            [
                "link" => "/marianske-lazne/", "img" => "/templates/artopos/images/redac/thumbnail///311_167.jpg",
                "author" => ' ',
                "title" => 'Mariánské Lázně',
                "status" => 'Hledáme',
                "description" => 'Litografie ze  souboru osmačtyřiceti litografií pod názvem "Malerisch-historisches Album des Königreichs Böhmen", který vydal nakladatel Eduard Hölzel v roce 1864 ve spolupráci s malíři, kreslíři a ...',

            ],
            [
                "link" => "/sobotni-rano-v-brocne/", "img" => "/templates/artopos/images/redac/thumbnail///310_144.jpg",
                "author" => 'Emil Orlik',
                "title" => 'Nedělní ráno v Brocně',
                "status" => 'Hledáme',
                "description" => 'Žánrový obrázek z Brocna (Brotzen) u Štětí zobrazuje tehdejší německé obyvatele cestou na ranní mši. Emi Orlik zde zobrazil jednu z roubených chalup, které se zde ...',

            ],
            [
                "link" => "/mestsky-kostel-v-roudnici/", "img" => "/templates/artopos/images/redac/thumbnail///309_001.jpg",
                "author" => ' ',
                "title" => 'Městský kostel v Roudnici',
                "status" => 'Hledáme',
                "description" => 'Autor předlohy a litografie R. Bürger.                     ...',

            ],
            [
                "link" => "/hrad-orlik-u-humpolce/", "img" => "/templates/artopos/images/redac/thumbnail///308_1.jpg",
                "author" => 'Jaroslav Panuška',
                "title" => 'Hrad Orlík u Humpolce',
                "status" => 'Hledáme',
                "description" => 'Nedatované zobrazení  zříceniny hradu Orlík u Humpolce. Dnes je kopec mnohem více porostlý lesem.             ...',

            ],
            [
                "link" => "/kostel-sv-havla-v-porici-nad-sazavou/", "img" => "/templates/artopos/images/redac/thumbnail///307_134830.jpg",
                "author" => 'Václav Pavlík',
                "title" => 'Kostel sv. Petra a Pavla v Poříčí n. S.',
                "status" => 'Nalezeno',
                "description" => 'Pohled na starobylý kostelík sv. Petra a Pavla z 11. století. Zamračená krajina, hnědavá pole, rozmoklá cesta a stromy bez listí evokují atmosféru konce zimy, kdy ...',

            ],
            [
                "link" => "/porici-nad-sazavou-zernovka/", "img" => "/templates/artopos/images/redac/thumbnail///306_136725.jpg",
                "author" => 'Václav Pavlík',
                "title" => 'Žernovka za jitra',
                "status" => 'Hledáme',
                "description" => 'Žernovka je kopec nedaleko Poříčí nad Sázavou. Obraz byl publikován v autorově monografii od Vojtěcha Volavky (Galerie XIII., Praha 1941). Na rubu přípis: Žernovka za jitra. ...',

            ],
            [
                "link" => "/pohled-na-znojmo/", "img" => "/templates/artopos/images/redac/thumbnail///304_60832-0175-5.jpg",
                "author" => 'Otmar Hillitzer',
                "title" => 'Pohled na Znojmo',
                "status" => 'Hledáme',
                "description" => 'Panoramatický pohled na Znojmo.                       ...',

            ],
            [
                "link" => "/freyung/", "img" => "/templates/artopos/images/redac/thumbnail///303_18761-0009-1.jpg",
                "author" => 'Paul Kaspar',
                "title" => 'Freyung',
                "status" => 'Nalezeno',
                "description" => 'Náměstí Freyung s klášterem Skotů (Schottenstift) v centru Vídně.                  ...',

            ],
            [
                "link" => "/moravske-namesti/", "img" => "/templates/artopos/images/redac/thumbnail///301_132719.jpg",
                "author" => 'Paul Kaspar',
                "title" => 'Moravské náměstí',
                "status" => 'Hledáme',
                "description" => 'Pohled na Lažanského náměstí již po odstranění sochy císaře Josefa II. (1919). Náměstí, největší v Brně, vzniklo na místě středověkého opevnění; podobně jako v jiných městech ...',

            ],
            [
                "link" => "/kapucinske-namesti/", "img" => "/templates/artopos/images/redac/thumbnail///299_132718.jpg",
                "author" => 'Paul Kaspar',
                "title" => 'Zelný trh',
                "status" => 'Nalezeno',
                "description" => 'Motiv z centra Brna, situace se zde už zcela proměnila.                 ...',

            ],
            [
                "link" => "/bratislavska-radnice/", "img" => "/templates/artopos/images/redac/thumbnail///298_132716.jpg",
                "author" => 'Paul Kaspar',
                "title" => 'Bratislavská radnice',
                "status" => 'Nalezeno',
                "description" => 'Drobnopisná veduta v autorově typickém stylu zobrazuje jeden z nejvýraznějších a nejpopulárnějších motivů historického jádra Bratislavy. Přesto je cenná díky přesné dataci i pečlivému zachycení detailů ...',

            ],
            [
                "link" => "/staroboleslavska-brana/", "img" => "/templates/artopos/images/redac/thumbnail///297_028_a3790c.jpg",
                "author" => 'Karel Votlučka',
                "title" => 'Staroboleslavská brána',
                "status" => 'Nalezeno',
                "description" => 'Kresba zachycuje Staroboleslavskou bránu ve Staré Boleslavi, pozůstatek původního opevnění města z doby před rokem 1378. Nedatovaná kresba musela vzniknout před rokem 1943, kdy byly zbořeny ...',

            ],
            [
                "link" => "/bez-nazvu-pariz/", "img" => "/templates/artopos/images/redac/thumbnail///295_114778.jpg",
                "author" => 'Friedrich Feigl',
                "title" => 'Bez názvu (Paříž?)',
                "status" => 'Hledáme',
                "description" => 'Městská scenérie v expresivním autorově stylu, dle charakteru architektury z Francie, nejspíš přímo z Paříže, s výrazným motivem kupole a věže neznámého chrámu.    ...',

            ],
            [
                "link" => "/odpoledne-na-zamku-detenice/", "img" => "/templates/artopos/images/redac/thumbnail///294_134954.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Odpoledne na zámku Dětenice',
                "status" => 'Nalezeno',
                "description" => 'Průčelí zámku, který se dnes stal populární turistickou destinací, lze snadno identifikovat a na webu dohledat četné fotografie téměř přesně ze stejného místa, které dokládají proměnu ...',

            ],
            [
                "link" => "/chalupy-v-pabenicich/", "img" => "/templates/artopos/images/redac/thumbnail///293_135807.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Chalupy v Paběnicích',
                "status" => 'Hledáme',
                "description" => 'Vesnická scéna monumentálního formátu. Do Paběnic autor často zajížděl. Skica k obrazu reprodukována v monografii Antonína Matějčka z roku 1941 pod číslem 73 a na přebalu ...',

            ],
            [
                "link" => "/ceske-mestecko-krucemburk/", "img" => "/templates/artopos/images/redac/thumbnail///292_135722.jpg",
                "author" => 'Jan Zrzavý',
                "title" => 'České městečko (Krucemburk)',
                "status" => 'Hledáme',
                "description" => 'Přes obecný autorský název na zadní straně obrazu je zjevné, že se jedná o Krucemburk. Obraz se nacházel ve sbírce Ondřeje Sekory.     ...',

            ],
            [
                "link" => "/zamek-v-jindrichove-hradci/", "img" => "/templates/artopos/images/redac/thumbnail///291_136136.jpg",
                "author" => 'Jaroslav Panuška',
                "title" => 'Zámek v Jindřichově Hradci',
                "status" => 'Hledáme',
                "description" => 'Zimní městská veduta, na Panušku až v nezvykle dokumentaristickém podání.                 ...',

            ],
            [
                "link" => "/krajina-u-jicina/", "img" => "/templates/artopos/images/redac/thumbnail///290_136162.jpg",
                "author" => 'Cyril Bouda',
                "title" => 'Krajina u Jičína',
                "status" => 'Nalezeno',
                "description" => 'Panorama s vrchy a hrady Českého středohoří. Kopce na horizontu jsou zprava Kumburk, Bradlec, Tábor (za ním Kozlov), Trosky, před nimi vrch Železný u Železnice. Na ...',

            ],
            [
                "link" => "/vyhled-na-trosky/", "img" => "/templates/artopos/images/redac/thumbnail///289_136138.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Výhled na Trosky',
                "status" => 'Hledáme',
                "description" => 'Krásný dálkový pohled - odkud byl asi zachycen...? A jak se asi krajina změnila od té doby? (Zatím místo malíře z neznalosti lokální topografie umísťujeme kamsi ...',

            ],
            [
                "link" => "/hard-pirkstejn/", "img" => "/templates/artopos/images/redac/thumbnail///288_136140.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Hrad Pirkštejn',
                "status" => 'Hledáme',
                "description" => 'Pohled na hrad, pořízený z protějšího svahu - vodítkem k určení přesného místa je kamenná zídka.           ...',

            ],
            [
                "link" => "/zamek-v-ratbori-689/", "img" => "/templates/artopos/images/redac/thumbnail///287_135892.jpg",
                "author" => 'Václav Radimský',
                "title" => 'Starý zámek v Ratboři',
                "status" => 'Nalezeno',
                "description" => 'Tzv. starý zámek v Ratboři,  sousedící s Radimského Pašinkou, už po Kotěrově přestavbě z let 1912-15.  Radimský se v roce 1918 vrátil do Čech; obraz tedy ...',

            ],
            [
                "link" => "/tvrz-v-mecholupich/", "img" => "/templates/artopos/images/redac/thumbnail///286_136224.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Zámek v Měcholupech',
                "status" => 'Nalezeno',
                "description" => 'Měcholup je několik, včetně těch, jež jsou dnes součásti Prahy, zde se však jedná o Měcholupy v okrese Klatovy, kde se nachází zámek přestavěný ze starší ...',

            ],
            [
                "link" => "/na-jezu-684/", "img" => "/templates/artopos/images/redac/thumbnail///285_136132.jpg",
                "author" => 'Karel Langer',
                "title" => 'Na jezu',
                "status" => 'Hledáme',
                "description" => 'Neznámý jez, snad v okolí Jaroměři, kam se Langer vracel.                 ...',

            ],
            [
                "link" => "/plitvicka-jezera/", "img" => "/templates/artopos/images/redac/thumbnail///284_28213.jpg",
                "author" => 'Karel Langer',
                "title" => 'Plitvická jezera',
                "status" => 'Hledáme',
                "description" => 'Atypický pohled na komplex Plitvických jezer z nějakého okolního kopce.                 ...',

            ],
            [
                "link" => "/karlovarska-promenada/", "img" => "/templates/artopos/images/redac/thumbnail///283_127413.jpg",
                "author" => 'Wilhelm Jankowski',
                "title" => 'Karlovarská promenáda',
                "status" => 'Nalezeno',
                "description" => 'Precizně provedená veduta zachycuje původní empírovou vřídelní kolonádu pod děkanským kostelem sv. Máří Magdalény na pravém břehu říčky Teplé. Motiv byl dohledán jak na mapě, tak ...',

            ],
            [
                "link" => "/lichnice-v-maji-676/", "img" => "/templates/artopos/images/redac/thumbnail///282_127399.jpg",
                "author" => 'Jindřich Prucha',
                "title" => 'Lichnice v máji',
                "status" => 'Hledáme',
                "description" => 'Obraz z cyklu Jaro v Železných horách byl vystaven v Mánesu v roce 1941 a 1943 (razítka na zadní straně). Reprodukováno v monografii Malíř předjaří a ...',

            ],
            [
                "link" => "/zamek-v-novych-dvorech/", "img" => "/templates/artopos/images/redac/thumbnail///281_127335.jpg",
                "author" => 'Miloš Jiránek',
                "title" => 'Zámek v Nových Dvorech',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/z-dolniho-briste-u-humpolce/", "img" => "/templates/artopos/images/redac/thumbnail///280_1-3.jpg",
                "author" => 'František Hladík',
                "title" => 'Z Dolního Bříště u Humpolce',
                "status" => 'Hledáme',
                "description" => 'Název odkazuje k vsi "Dolní Bříště", u Humpolce jsou však jen Mladé a Staré Bříště v těsném sousedství.         ...',

            ],
            [
                "link" => "/mlyn-u-pece-pod-cerchovem/", "img" => "/templates/artopos/images/redac/thumbnail///279_1-1.jpg",
                "author" => 'Václav Malý',
                "title" => 'Mlýn u Pece pod Čerchovem',
                "status" => 'Hledáme',
                "description" => 'Realisticky přesně zobrazený motiv, určený přípisem autora na zadní straně.                 ...',

            ],
            [
                "link" => "/ungelt-s-kostelem-sv-jakuba/", "img" => "/templates/artopos/images/redac/thumbnail///278_1-1.jpg",
                "author" => 'Karel Votlučka',
                "title" => 'Ungelt s kostelem sv. Jakuba',
                "status" => 'Nalezeno',
                "description" => 'Obrázek, byť motiv je zcela jasný, jsme do databáze zařadili z dokumentárního hlediska, neboť obsahuje mnoho zajímavých reálií. Vpravo Dům U Černého medvěda s nárožním domovním ...',

            ],
            [
                "link" => "/zlata-koruna/", "img" => "/templates/artopos/images/redac/thumbnail///277_1.jpg",
                "author" => 'Karel Zahrádka',
                "title" => 'Zlatá Koruna',
                "status" => 'Hledáme',
                "description" => 'Motiv z klášterního areálu od méně známého reprezentanta pozdního impresionismu.                 ...',

            ],
            [
                "link" => "/omis/", "img" => "/templates/artopos/images/redac/thumbnail///276_img_5561.jpg",
                "author" => 'Pravoslava Stefanová',
                "title" => 'Omiš',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kdyne/", "img" => "/templates/artopos/images/redac/thumbnail///275_img_5566.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Kdyně',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zricenina-hradu-oseka-s-pohledem-na-klaster-osek/", "img" => "/templates/artopos/images/redac/thumbnail///274_126168-1.jpg",
                "author" => 'Ernst Gustav Doerell',
                "title" => 'Zřícenina hradu Oseka s pohledem na klášter Osek',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-doubravku-ze-zameckeho-parku-v-teplicich/", "img" => "/templates/artopos/images/redac/thumbnail///271_126169.jpg",
                "author" => 'Ernst Gustav Doerell',
                "title" => 'Pohled na Doubravku ze zámeckého parku v Teplicích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-labe/", "img" => "/templates/artopos/images/redac/thumbnail///270_1-1.jpg",
                "author" => 'Ernst Gustav Doerell',
                "title" => 'Pohled na Labe',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-strekov/", "img" => "/templates/artopos/images/redac/thumbnail///269_1.jpg",
                "author" => 'Ernst Gustav Doerell',
                "title" => 'Pohled na Střekov',
                "status" => 'Hledáme',
                "description" => 'Krajinná scenérie  s Labem z  okolí Ústí nad Labem z hloubi 19. st.              ...',

            ],
            [
                "link" => "/krajina-po-destikrajina-po-desti-rotnek-u-kysperka/", "img" => "/templates/artopos/images/redac/thumbnail///267_121174.jpg",
                "author" => 'Jan Honsa',
                "title" => 'Krajina po dešti, Rotnek u Kyšperka',
                "status" => 'Hledáme',
                "description" => 'Pohled na obec Rotnek, dnes Červená.                     ...',

            ],
            [
                "link" => "/motiv-z-benatek/", "img" => "/templates/artopos/images/redac/thumbnail///268_285_531_51a36a11d9f31__dsc4639.jpg",
                "author" => 'Antonie Brandeisová',
                "title" => 'Motiv z Benátek',
                "status" => 'Nalezeno',
                "description" => 'Malířka stála na Fondamenta Ognissanti v místě, kde je mostek a kanál se láme v pravém úhlu doprava. Chrám v pozadí se zvonicí je Chiesa San ...',

            ],
            [
                "link" => "/krajina-z-etretatu/", "img" => "/templates/artopos/images/redac/thumbnail///266_136539.jpg",
                "author" => 'Rudolf Kremlička',
                "title" => 'Krajina z Étretatu',
                "status" => 'Nalezeno',
                "description" => 'Známý motiv hlavní pláže v normandském městečku Étretat, proslulém díky svým bílým útesům, které zobrazilo i mnoho slavných malířů (Courbet, Boudin, Monet). Pláž je vymezena dvěma ...',

            ],
            [
                "link" => "/vysoka-pec-v-trinci/", "img" => "/templates/artopos/images/redac/thumbnail///262_136544.jpg",
                "author" => 'Josef Štolovský',
                "title" => 'Vysoká pec v Třinci',
                "status" => 'Hledáme',
                "description" => 'Motiv z Třineckých železáren, přesné určení motivu a jeho stavu v době vzniku obrazu je úkolem pro znalce dějin lokálního průmyslu a průmyslové architektury.   ...',

            ],
            [
                "link" => "/ze-stozic/", "img" => "/templates/artopos/images/redac/thumbnail///260_136479.jpg",
                "author" => 'Vratislav Hlava (Rudolf Bém)',
                "title" => 'Ze Stožic',
                "status" => 'Nalezeno',
                "description" => 'Pohled na širokou cestu procházející Stožicemi se strouhou místo kanalizace uprostřed. Nízký domek vlevo zůstal dodnes beze změn, dva domy vpravo také odpovídají těm na obraze. ...',

            ],
            [
                "link" => "/stare-domy-v-pelhrimove/", "img" => "/templates/artopos/images/redac/thumbnail///259_136203.jpg",
                "author" => 'Jan Honsa',
                "title" => 'Staré domy v Pelhřimově',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/jeleni-prikop-s-prazskym-hradem/", "img" => "/templates/artopos/images/redac/thumbnail///258_135691.jpg",
                "author" => 'Jan B. Minařík',
                "title" => 'Jelení příkop s Pražským hradem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-horu-ostas/", "img" => "/templates/artopos/images/redac/thumbnail///257_135663.jpg",
                "author" => 'Antonín Hudeček',
                "title" => 'Pohled na horu Ostaš',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostel-637/", "img" => "/templates/artopos/images/redac/thumbnail///256_2.jpg",
                "author" => 'Jindřich Bubeníček',
                "title" => 'Kostel sv. Ondřeje v Kolovratech',
                "status" => 'Nalezeno',
                "description" => 'Místo bylo určeno jako kostel sv. Ondřeje v Kolovratech v Praze 10, Bubeníček působil v nedaleké Uhříněvsi. Dnes kolem vede frekventovaná silnice.
      ...',

            ],
            [
                "link" => "/pastva/", "img" => "/templates/artopos/images/redac/thumbnail///255_screenshot-2014-05-16-10.jpg",
                "author" => 'Stanislav Lolek',
                "title" => 'Pastva',
                "status" => 'Hledáme',
                "description" => 'Krajina z okolí Uherského  Hradiště v jeho typickém vervním rukopisu pozdního období.               ...',

            ],
            [
                "link" => "/slunnny-den/", "img" => "/templates/artopos/images/redac/thumbnail///253_screenshot-2014-05-16-10.jpg",
                "author" => 'Stanislav Lolek',
                "title" => 'Slunnný den',
                "status" => 'Hledáme',
                "description" => 'Pohled na hospodářský dvůr s figurální stafáží s výrazným motivem kapličky, snad z Moravského Slovácka.            ...',

            ],
            [
                "link" => "/predvanocni-trh/", "img" => "/templates/artopos/images/redac/thumbnail///252_1.jpg",
                "author" => 'Stanislav Lolek',
                "title" => 'Předvánoční trh',
                "status" => 'Hledáme',
                "description" => 'Zimní trh je zasaten do prostředí moravského či českého městečka.                 ...',

            ],
            [
                "link" => "/pohled-na-uherske-hradiste/", "img" => "/templates/artopos/images/redac/thumbnail///251_2.jpg",
                "author" => 'Stanislav Lolek',
                "title" => 'Pohled na Uherské Hradiště',
                "status" => 'Nalezeno',
                "description" => 'Přestože obraz se před časem objevil na aukčním trhu pod názvem Pohled na Přerov, jedná se o pohed na Uherské Hradiště z lokality velkomoravského kostela Uherské ...',

            ],
            [
                "link" => "/vyhled-do-kraje-628/", "img" => "/templates/artopos/images/redac/thumbnail///250_1-1.jpg",
                "author" => 'Stanislav Lolek',
                "title" => 'Výhled do kraje',
                "status" => 'Hledáme',
                "description" => 'Krajina z okolí Uherského Hradiště v autorově charakteristickém pozdním rukopisu.                 ...',

            ],
            [
                "link" => "/sedici-divky-vzhlizejici-do-krajiny/", "img" => "/templates/artopos/images/redac/thumbnail///249_29919.jpg",
                "author" => 'Stanislav Lolek',
                "title" => 'Na vyhlídce',
                "status" => 'Hledáme',
                "description" => 'Krajina z okolí Uherského Hradiště (?) z autorova závěečného období.                 ...',

            ],
            [
                "link" => "/krajina-v-podhuri/", "img" => "/templates/artopos/images/redac/thumbnail///248_66907.jpg",
                "author" => 'Stanislav Lolek',
                "title" => 'Krajina v podhůří',
                "status" => 'Hledáme',
                "description" => 'Blíže neidentifikovaná krajina s kopci na obzoru.                    ...',

            ],
            [
                "link" => "/radejov-u-straznice/", "img" => "/templates/artopos/images/redac/thumbnail///247_70983.jpg",
                "author" => 'Stanislav Lolek',
                "title" => 'Radějov u Strážnice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/krajina-na-vysocine/", "img" => "/templates/artopos/images/redac/thumbnail///246_91364.jpg",
                "author" => 'Stanislav Lolek',
                "title" => 'Krajina na Vysočině',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/podtatranska-krajina/", "img" => "/templates/artopos/images/redac/thumbnail///245_135853.jpg",
                "author" => 'Stanislav Lolek',
                "title" => 'Podtatranská krajina',
                "status" => 'Hledáme',
                "description" => 'Motiv z Nízkých Tater (?).                      ...',

            ],
            [
                "link" => "/u-hermanova-mestce/", "img" => "/templates/artopos/images/redac/thumbnail///244_sigmund_k_j_u_hermanova_mestce.jpg",
                "author" => 'Karel Jan Sigmund',
                "title" => 'U Heřmanova Městce',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zatopeny-lom/", "img" => "/templates/artopos/images/redac/thumbnail///243_70390.jpg",
                "author" => 'Karel Jan Sigmund',
                "title" => 'Zatopený lom',
                "status" => 'Hledáme',
                "description" => 'Obraz byl na trhu nabízen pod tímto názvem, ale vůbec není jisté, zda jde opravdu o lom. Není to náhoduo údolí Chrudimky, případně nemá to něco ...',

            ],
            [
                "link" => "/pole-ke-kostelci/", "img" => "/templates/artopos/images/redac/thumbnail///242_98736.jpg",
                "author" => 'Karel Jan Sigmund',
                "title" => 'Pole ke Kostelci',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/motiv-z-kostelce/", "img" => "/templates/artopos/images/redac/thumbnail///241_116945.jpg",
                "author" => 'Karel Jan Sigmund',
                "title" => 'Motiv z Kostelce',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/chalupy-na-secskem-predmesti/", "img" => "/templates/artopos/images/redac/thumbnail///240_133317.jpg",
                "author" => 'Karel Jan Sigmund',
                "title" => 'Chalupy na sečském předměstí',
                "status" => 'Hledáme',
                "description" => 'Zdá se, že narozíl od názvu obrazu při prodeji v aukci by mělo být "sečské" s malým písmenem, tj. předměstí v Seči - žádné takové předměstí ...',

            ],
            [
                "link" => "/krajina-kostelec-u-hermanova-mestce/", "img" => "/templates/artopos/images/redac/thumbnail///238_135341.jpg",
                "author" => 'Karel Jan Sigmund',
                "title" => 'Krajina (Kostelec u Heřmanova Městce?)',
                "status" => 'Hledáme',
                "description" => 'Existuje obraz s podobným námětem lokalizovaný přípisem "Kostelec u Heřmanova Městce", kde ostatně malíř působil. V Kostelci se nachází pískovna, dnes už uzavřená.    ...',

            ],
            [
                "link" => "/z-kokorinsko/", "img" => "/templates/artopos/images/redac/thumbnail///239_90625.jpg",
                "author" => 'Jaroslav Panuška',
                "title" => 'Z Kokořínska',
                "status" => 'Hledáme',
                "description" => 'Monumentální krajinné panorama z Českého středohoří.                     ...',

            ],
            [
                "link" => "/hrad-trosky-609/", "img" => "/templates/artopos/images/redac/thumbnail///237_92142.jpg",
                "author" => 'Jaroslav Panuška',
                "title" => 'Hrad Trosky',
                "status" => 'Hledáme',
                "description" => 'Trosky jsou jedním z nejmalebnějších a tudíž nejčastějších motivů českých krajinářů. Zde stačí dohledat přesné místo v podhradí, odkud byl záběr pořízen, o mnoho víc zajímavých ...',

            ],
            [
                "link" => "/hrad-v-udoli/", "img" => "/templates/artopos/images/redac/thumbnail///235_98663.jpg",
                "author" => 'Jaroslav Panuška',
                "title" => 'Hrad Lipnice',
                "status" => 'Hledáme',
                "description" => 'Hrad Lipnice malovaný někde z okolí Dolního Města, možná z kopce Melechov západně od Dolního Města. Hrad na obraze má charakteristickou siluetu, stejně tak kopce a ...',

            ],
            [
                "link" => "/caslav-606/", "img" => "/templates/artopos/images/redac/thumbnail///234_101146.jpg",
                "author" => 'Jaroslav Panuška',
                "title" => 'Čáslav',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zricenina-hradu/", "img" => "/templates/artopos/images/redac/thumbnail///233_123461.jpg",
                "author" => 'Jaroslav Panuška',
                "title" => 'Zřícenina hradu',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zricenina/", "img" => "/templates/artopos/images/redac/thumbnail///232_131924.jpg",
                "author" => 'Jaroslav Panuška',
                "title" => 'Zřícenina',
                "status" => 'Hledáme',
                "description" => 'Drobný obrázek s neznámou zříceninou.                      ...',

            ],
            [
                "link" => "/ceka-krajina/", "img" => "/templates/artopos/images/redac/thumbnail///231_117839.jpg",
                "author" => 'Jaroslav Panuška',
                "title" => 'Česká krajina',
                "status" => 'Hledáme',
                "description" => 'Výrazný architektonický a krajinný motiv asi nebude problém určit...                  ...',

            ],
            [
                "link" => "/pohled-na-sobotku/", "img" => "/templates/artopos/images/redac/thumbnail///228_31327.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Pohled na Sobotku',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kamenicky/", "img" => "/templates/artopos/images/redac/thumbnail///227_27049.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Kameničky',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/hornata-krajina/", "img" => "/templates/artopos/images/redac/thumbnail///226_46890.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Hornatá krajina',
                "status" => 'Hledáme',
                "description" => 'Monumentální krajina s výrazným vrcholem kopce, na trhu nabízená pod obecným názvem "Hornatá krajna"...             ...',

            ],
            [
                "link" => "/letni-krajina-s-rekou/", "img" => "/templates/artopos/images/redac/thumbnail///225_61103.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Letní krajina s řekou',
                "status" => 'Hledáme',
                "description" => 'Zajímavá otázka: copak to je za krajinu - Pošumaví s Otavou? (Na Šumavu Macoun zajížděl...)            ...',

            ],
            [
                "link" => "/kostel-sv-jakuba-v-jaromeri/", "img" => "/templates/artopos/images/redac/thumbnail///224_69348.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Kostel sv. Jakuba v Jaroměři',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/staroprazske-zakoutidsdadsa/", "img" => "/templates/artopos/images/redac/thumbnail///223_86245.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Staropražské zákoutí',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/chalupy-pod-troskami/", "img" => "/templates/artopos/images/redac/thumbnail///222_96777.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Chalupy pod Troskami',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/z-vysociny/", "img" => "/templates/artopos/images/redac/thumbnail///220_123380.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Z Vysočiny',
                "status" => 'Hledáme',
                "description" => 'Neznámá vesnička na Vysočině.                       ...',

            ],
            [
                "link" => "/krajina-s-labem-a-horou-rip/", "img" => "/templates/artopos/images/redac/thumbnail///218_131017.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Krajina s Labem a horou Říp',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/silnicni-most/", "img" => "/templates/artopos/images/redac/thumbnail///217_40069.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Bílý most ve Špindlerově Mlýně',
                "status" => 'Nalezeno',
                "description" => '"Bílý most" ve Špindlerově Mlýnu. Pohled z pravého břehu přes Labe na náměstí.              ...',

            ],
            [
                "link" => "/holesovicka-cihelna/", "img" => "/templates/artopos/images/redac/thumbnail///216_12019.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Holešovická cihelna',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/z-hrdlorez/", "img" => "/templates/artopos/images/redac/thumbnail///215_44938.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Z Hrdlořez',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/mala-skala-585/", "img" => "/templates/artopos/images/redac/thumbnail///214_27539.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Malá Skála',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/mala-skala/", "img" => "/templates/artopos/images/redac/thumbnail///213_46075.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Malá Skála',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/namesti-ve-vysokem-nad-jizerou/", "img" => "/templates/artopos/images/redac/thumbnail///212_66483.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Náměstí ve Vysokém nad Jizerou',
                "status" => 'Hledáme',
                "description" => 'Název je snad odvozen z přípisu na zadní straně.                  ...',

            ],
            [
                "link" => "/namesti/", "img" => "/templates/artopos/images/redac/thumbnail///211_95175.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Náměstí menšího města',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kostelik-na-navsi/", "img" => "/templates/artopos/images/redac/thumbnail///210_126527.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Kostelík na návsi',
                "status" => 'Hledáme',
                "description" => 'Neznámý kostelík, snad někde v okolí Malé Skály.                   ...',

            ],
            [
                "link" => "/predjari/", "img" => "/templates/artopos/images/redac/thumbnail///209_134039.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Předjaří',
                "status" => 'Hledáme',
                "description" => 'Neznámá venkovská krajina, identifikovatelná dle výrazného motivu kopců v pozadí.                 ...',

            ],
            [
                "link" => "/svatecni-ulice/", "img" => "/templates/artopos/images/redac/thumbnail///208_127472-1.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Sváteční ulice',
                "status" => 'Hledáme',
                "description" => 'Zatím neurčený motiv z Prahy (od roku 1957 měl ateliér na Letné) nebo snad ze severních Čech? Vodítkem by mohla být budova v zatáčce - ...',

            ],
            [
                "link" => "/motiv-z-dalmacie/", "img" => "/templates/artopos/images/redac/thumbnail///206_1.jpg",
                "author" => 'Alois Janeček Pardubský',
                "title" => 'Motiv z Dalmácie',
                "status" => 'Hledáme',
                "description" => 'Neznámý motiv z pobřeží Dalmácie.                      ...',

            ],
            [
                "link" => "/pohled-na-kozakov/", "img" => "/templates/artopos/images/redac/thumbnail///205_036.jpg",
                "author" => 'Alois Janeček Pardubský',
                "title" => 'Pohled na Kozákov',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/malse-v-ceskych-budejovicich/", "img" => "/templates/artopos/images/redac/thumbnail///204_038.jpg",
                "author" => 'Karel Štěch',
                "title" => 'Malše v Českých Budějovicích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/zahouruv-mlyn/", "img" => "/templates/artopos/images/redac/thumbnail///196_f618.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Žahourův mlýn',
                "status" => 'Nalezeno',
                "description" => '',

            ],
            [
                "link" => "/mlada-vozice-s-noskovske-silnice/", "img" => "/templates/artopos/images/redac/thumbnail///195_f624.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Mladá Vožice z noskovské silnice',
                "status" => 'Hledáme',
                "description" => 'V Památníku Mladovožicka pod názvem Mladá Vožice ve žních, odpovídá však obrazu M. V. z noskovské silnice, vystavenému na Souborné výstavě děl u příležitosti 80. narozenin ...',

            ],
            [
                "link" => "/u-husovy-studanky/", "img" => "/templates/artopos/images/redac/thumbnail///194_f616.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'U Husovy studánky',
                "status" => 'Hledáme',
                "description" => 'Toto pomístní jméno zatím nedohledáno.                      ...',

            ],
            [
                "link" => "/vozice-od-panskeho-rybnika/", "img" => "/templates/artopos/images/redac/thumbnail///193_f623.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Vožice od Panského rybníka',
                "status" => 'Hledáme',
                "description" => 'Obraz vystaven na ystaveno na 64. výstavě JUV v Praze, Obecní dům, 5 až 27. 11. 1932 a na Souborné výstavě děl u příležitosti 80. narozenin ...',

            ],
            [
                "link" => "/selmberk-od-mikulase/", "img" => "/templates/artopos/images/redac/thumbnail///192_f614.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Šelmberk od Mikuláše',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/krajina-u-jesetic/", "img" => "/templates/artopos/images/redac/thumbnail///191_f1010.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Krajina u Ješetic',
                "status" => 'Hledáme',
                "description" => 'Obraz z prvního pobytu na České Sibiři v roce 1910. „V roce 1910 usadil jsem se ve mlýně "Na Jarši" u Heřmaniček. Na tento koutek jižních ...',

            ],
            [
                "link" => "/oustejovska-kaplicka-pod-starou-lipou/", "img" => "/templates/artopos/images/redac/thumbnail///190_1-3.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Oustějovská kaplička pod starou lípou',
                "status" => 'Nalezeno',
                "description" => 'Vystaveno na Souborné výstavě děl u příležitosti 80. narozenin, Mladá Vožice, červenec - srpen 1951, č. 159 s datací 1942. Při prodeji obrazu datace 1949 z ...',

            ],
            [
                "link" => "/vez-selmberku-na-podzim/", "img" => "/templates/artopos/images/redac/thumbnail///189_1.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Věž Šelmberku na podzim',
                "status" => 'Hledáme',
                "description" => 'Výjimečně datovaná krajina z Mladovožicka s hláskou hradu Šelmberku                  ...',

            ],
            [
                "link" => "/vyhled-do-kraje/", "img" => "/templates/artopos/images/redac/thumbnail///188_1-2.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Výhled do kraje',
                "status" => 'Hledáme',
                "description" => 'Motiv s mlýnem u rybníka, snad z Mladovožicka, název evidentně dodatečný,                ...',

            ],
            [
                "link" => "/vozicke-kopce/", "img" => "/templates/artopos/images/redac/thumbnail///187_1-1.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Vožické kopce',
                "status" => 'Hledáme',
                "description" => 'Pohled na vrch Stehlovy, oblíbený Bubeníčkův motiv.                    ...',

            ],
            [
                "link" => "/za-vsi/", "img" => "/templates/artopos/images/redac/thumbnail///186_1.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Za vsí',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/velky-a-maly-blanik-z-predbori/", "img" => "/templates/artopos/images/redac/thumbnail///185_blanik-predbori.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Velký a Malý Blaník z Předbořic',
                "status" => 'Hledáme',
                "description" => 'Obraz se kdysi objevil na aukčním trhu pod názvem "... z Předboří", jde o Předbořice v okrese Benešov. Obraz se stejným názvem většího formátu datovaný 1943 ...',

            ],
            [
                "link" => "/pohle-na-blanik/", "img" => "/templates/artopos/images/redac/thumbnail///184_38081.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Blaník z údolí Blanice u Louňovic',
                "status" => 'Hledáme',
                "description" => 'K určení místa, odkud autor Blaník maloval, napomohl motiv mostu přes Blanici. Jedná se o starobylý most z Louňovic směrem na Vlašim. Obraz lze ztotožnit s ...',

            ],
            [
                "link" => "/z-mladovozicka/", "img" => "/templates/artopos/images/redac/thumbnail///183_41148.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Z Mladovožicka',
                "status" => 'Hledáme',
                "description" => 'Dle kostelíku nejspíš Janov u Mladé Vožice.                    ...',

            ],
            [
                "link" => "/vozicky-hrad/", "img" => "/templates/artopos/images/redac/thumbnail///182_68384.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Vožický hrad',
                "status" => 'Hledáme',
                "description" => 'Královský hrad v Mladé Vožici se nacházel na vršku nad městem, kde dnes stojí kaple Nanebevzetí Panny Marie. Byl zbořen během husitských válek v r. 1425. ...',

            ],
            [
                "link" => "/vetrny-jenikov/", "img" => "/templates/artopos/images/redac/thumbnail///180_vetrny-jenikov.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Janov',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/motiv-z-jiznich-cech/", "img" => "/templates/artopos/images/redac/thumbnail///179_73142.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Motiv z jižních Čech',
                "status" => 'Hledáme',
                "description" => 'Typický jihočeský statek nejspíš z okolí Mladé Vožice.                   ...',

            ],
            [
                "link" => "/nad-panskym-rybnikem/", "img" => "/templates/artopos/images/redac/thumbnail///178_90772.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Nad Panským rybníkem',
                "status" => 'Hledáme',
                "description" => 'Motiv z Mladé Vožice, v pozadí hláska zříceniny hradu Šelmberka.                 ...',

            ],
            [
                "link" => "/vesnicka-u-mlade-vozice/", "img" => "/templates/artopos/images/redac/thumbnail///181_90292.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Janov',
                "status" => 'Hledáme',
                "description" => 'Kdysi se objevilo na trhu pod názvem Vesnička u Mladé Vožice; konkrétně jde o Janov, který Bubeníček maloval několikrát, s výrazným motivem kostelíka Všech svatých; z ...',

            ],
            [
                "link" => "/jaro-v-hornich-koutech-u-mlade-vozice/", "img" => "/templates/artopos/images/redac/thumbnail///177_109325.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Jaro v Horních Koutech u Mladé Vožice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-mladou-vozici/", "img" => "/templates/artopos/images/redac/thumbnail///176_104680.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Pohled na Mladou Vožici',
                "status" => 'Hledáme',
                "description" => 'Pohled na Mladou Vožici s dominantami kaple Nanebevzetí Panny Marie na vršku nad městem na místě původního královského hradu, zbořeného během husitských válek v r. 1425. ...',

            ],
            [
                "link" => "/pod-cerchovem/", "img" => "/templates/artopos/images/redac/thumbnail///175_116626.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Pod Čerchovem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/svratecky-kraj/", "img" => "/templates/artopos/images/redac/thumbnail///174_117606.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Svratecký kraj',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pavlov-u-mlade-vozice/", "img" => "/templates/artopos/images/redac/thumbnail///173_121758.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Pavlov u Mladé Vožice',
                "status" => 'Hledáme',
                "description" => 'Pavlov u Mladé Vožice s Podhradním rybníkem a Blaníkem v pozadí.                ...',

            ],
            [
                "link" => "/vrch-stehlovy-u-mlade-vozice/", "img" => "/templates/artopos/images/redac/thumbnail///172_133131.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Vrch Stehlovy u Mladé Vožice',
                "status" => 'Hledáme',
                "description" => '

Oblíbený Bubeníčkův motiv z blízkého okolí Mladé Vožice - vrch Stehlovy, vesnice Pavlov, před ní Podhradnm rybník, v pozadí monumentální Blaník.


     ...',

            ],
            [
                "link" => "/samota-s-rybnikem/", "img" => "/templates/artopos/images/redac/thumbnail///170_117594.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Samota s rybníkem',
                "status" => 'Hledáme',
                "description" => 'Blíže neurčený motiv nejspíš z okolí Mladé Vožice.                   ...',

            ],
            [
                "link" => "/vrchotovy-janovice/", "img" => "/templates/artopos/images/redac/thumbnail///169_121185.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Vrchotovy Janovice',
                "status" => 'Hledáme',
                "description" => 'V Bubeníčkově díle patří k ranějším, ještě předválečným malbám, tomu odpovídá formální pojetí, založené na dekorativním rytmu štětcového duktu.        ...',

            ],
            [
                "link" => "/krajina-s-usedlosti/", "img" => "/templates/artopos/images/redac/thumbnail///168_134194.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Krajina s usedlostí',
                "status" => 'Hledáme',
                "description" => 'Krajina z Mladovožicka?                        ...',

            ],
            [
                "link" => "/zvonicka-v-jezove/", "img" => "/templates/artopos/images/redac/thumbnail///166_45151.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Zvonička v Ježově',
                "status" => 'Nalezeno',
                "description" => 'Zvonice na návsi v Malém Ježově nedaleko Mladé Vožice. V roce 1942 malval náves v Malém Ježově (33 x 50 cm) - jde o stejný motiv? ...',

            ],
            [
                "link" => "/krajina-s-kostelikem/", "img" => "/templates/artopos/images/redac/thumbnail///165_108734.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Krajina s kostelíkem',
                "status" => 'Hledáme',
                "description" => 'V aukci Galerie Art Praha datováno 1930 (?); pokud vychází z nějakého údaje na obraze, pak by šlo nejšéíš o krajinu v okolí Svratky, kam malíř jezdil ...',

            ],
            [
                "link" => "/oustejovska-kaplicka/", "img" => "/templates/artopos/images/redac/thumbnail///163_119185.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Oustějovská kaplička',
                "status" => 'Nalezeno',
                "description" => 'Kaple Panny Marie v malé osadě Ústějov u Mladé Vožice při polní cestě do Radvánova, pochází z roku 1812, kdy byla postavena Danielem Krchem na místě ...',

            ],
            [
                "link" => "/bystricka-u-martina/", "img" => "/templates/artopos/images/redac/thumbnail///162_129958.jpg",
                "author" => 'Josef Soukup',
                "title" => 'Bystrička u Martina',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/hrad-lipnice-527/", "img" => "/templates/artopos/images/redac/thumbnail///229_133719.jpg",
                "author" => 'Jaroslav Panuška',
                "title" => 'Hrad Lipnice',
                "status" => 'Hledáme',
                "description" => '1944, olej, tempera na kartonu, 50 x 66 cm, značeno vlevo dole: JARO PANUŠKA 1944            ...',

            ],
            [
                "link" => "/zelezny-brod/", "img" => "/templates/artopos/images/redac/thumbnail///160_1-15.jpg",
                "author" => 'Josef Jíra',
                "title" => 'Železný Brod',
                "status" => 'Nalezeno',
                "description" => 'Ve svém charakteristickém stylu tu autor zachytil pohled na kostel sv. Jakuba Většího s dřevěnou zvonicí u zákrutu Jizery. Stejný motiv téměř přesně ze stejné pozice ...',

            ],
            [
                "link" => "/mlynek-na-rokytce/", "img" => "/templates/artopos/images/redac/thumbnail///159_1-14.jpg",
                "author" => 'Josef Soukup',
                "title" => 'Malý mlýn (Mlejnek) na Rokytce',
                "status" => 'Nalezeno',
                "description" => 'Malý mlýn, též zvaný "Mlejnek" č. p. 30 a 31 stával na pravém břehu Rokytky v zaniklé Mlýnské ulici. Ta se ústila do dnešní Zenklovy ulice ...',

            ],
            [
                "link" => "/bosilec-blata/", "img" => "/templates/artopos/images/redac/thumbnail///158_1-13.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Bošilec - Blata',
                "status" => 'Nalezeno',
                "description" => 'Pohled na kostelík sv. Martina přes Bošilecký rybník, jeden z nejstarších u nás (doložen již 1355). Cibuli věže si malíř oproti realitě poněkud prodloužil... Domky v ...',

            ],
            [
                "link" => "/lsten-u-hostomic/", "img" => "/templates/artopos/images/redac/thumbnail///157_1-12.jpg",
                "author" => 'Josef Soukup',
                "title" => 'Lštěň u Hostomic',
                "status" => 'Hledáme',
                "description" => 'Pohled na malou vesnici na Berounsku.                     ...',

            ],
            [
                "link" => "/kostel-na-budci/", "img" => "/templates/artopos/images/redac/thumbnail///156_1-11.jpg",
                "author" => 'Gustav Macoun',
                "title" => 'Kostel na Budči',
                "status" => 'Hledáme',
                "description" => 'Panoramatický pohled na Budeč, bude zajímavé vidět, jak se krajina za sto let proměnila.             ...',

            ],
            [
                "link" => "/u-brevnova/", "img" => "/templates/artopos/images/redac/thumbnail///155_1-10.jpg",
                "author" => 'Josef Soukup',
                "title" => 'U Břevnova',
                "status" => 'Nalezeno',
                "description" => 'Malíř stál na hraně pláně mezi Ladronkou a Spiritkou a zachytil situaci směrem na východ. Vidět je Spiritka, za ní schovány Kneislovka a Hybšmanka. Vlevo od ...',

            ],
            [
                "link" => "/matej-od-zapadu/", "img" => "/templates/artopos/images/redac/thumbnail///154_1-8.jpg",
                "author" => 'Josef Soukup',
                "title" => 'Matěj (od západu)',
                "status" => 'Hledáme',
                "description" => 'Kostel sv. Matěje v Dejvicích na vysokém ostrohu v šáreckém údolí. S přesnou lokalizací a zachycením současného stavu by neměl být problém...     ...',

            ],
            [
                "link" => "/lasenice/", "img" => "/templates/artopos/images/redac/thumbnail///153_1-7.jpg",
                "author" => 'Josef Soukup',
                "title" => 'Lásenice',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/trh-ve-svratce/", "img" => "/templates/artopos/images/redac/thumbnail///152_1-6.jpg",
                "author" => 'Oldřich Blažíček',
                "title" => 'Trh ve Svratce',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/svratecky-kostel/", "img" => "/templates/artopos/images/redac/thumbnail///151_svratecky-kostel.jpg",
                "author" => 'Ota Bubeníček',
                "title" => 'Svratecký kostel',
                "status" => 'Nalezeno',
                "description" => 'Hřbitovní kostel sv. Jana Křtitele ve Svratce. Zdá se, že polohu se podařilo určit přesně, chybí jen doplnit současný pohled, případně další zajímavé údaje. Vystaveno na ...',

            ],
            [
                "link" => "/zima-na-baste-sv-jiri/", "img" => "/templates/artopos/images/redac/thumbnail///150_1-4.jpg",
                "author" => 'Jaroslav Malínský',
                "title" => 'Zima na baště sv. Jiří',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/krskova-chalupa-v-nemecke-rybne/", "img" => "/templates/artopos/images/redac/thumbnail///149_1-3.jpg",
                "author" => 'Oldřich Hlavsa',
                "title" => 'Krskova chalupa v Německé Rybné',
                "status" => 'Hledáme',
                "description" => 'Obec Rybná nad Zdobnicí se do roku 1947 nazývala Německá Rybná (německy Deutsch Rybna).             ...',

            ],
            [
                "link" => "/posta-ve-starem-bitove/", "img" => "/templates/artopos/images/redac/thumbnail///148_1-2.jpg",
                "author" => 'Roman Havelka',
                "title" => 'Pošta ve starém Bítově',
                "status" => 'Nalezeno',
                "description" => 'Bítov - poddanské městečko, později vesnice pod hradem Bítov - zanikl s napuštěním vranovské přehrady a Nový Bítov byl vybudován o něco výše. O stavbě přehrady ...',

            ],
            [
                "link" => "/kostel-v-malenicich/", "img" => "/templates/artopos/images/redac/thumbnail///147_85136.jpg",
                "author" => 'Václav Haise',
                "title" => 'Kostel v Malenicích',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/minice-zjara/", "img" => "/templates/artopos/images/redac/thumbnail///145_6uo112106r.jpg",
                "author" => 'Václav Haise',
                "title" => 'Minice zjara',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/pohled-na-ostravu/", "img" => "/templates/artopos/images/redac/thumbnail///144_6uo112034.jpg",
                "author" => 'Ferdyš Duša',
                "title" => 'Pohled na Ostravu',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/jama-petr-cingr/", "img" => "/templates/artopos/images/redac/thumbnail///142_6uo112024r.jpg",
                "author" => 'Vladimír Kristin',
                "title" => 'Jáma Petr Cingr',
                "status" => 'Hledáme',
                "description" => 'Pohled na Důl Michal v Ostravě na zač. 50. let.                 ...',

            ],
            [
                "link" => "/jarni-snih-poruba/", "img" => "/templates/artopos/images/redac/thumbnail///141_6uo113175r.jpg",
                "author" => 'Luboš Synecký',
                "title" => 'Jarní sníh - Poruba',
                "status" => 'Hledáme',
                "description" => 'Obraz z autorova ostravského období (1954 - 1964).                   ...',

            ],
            [
                "link" => "/prazsky-motiv-488/", "img" => "/templates/artopos/images/redac/thumbnail///140_119181.jpg",
                "author" => 'Jan Jáchym',
                "title" => 'Pražský motiv',
                "status" => 'Hledáme',
                "description" => 'Drobný obrázek průchodu, nejspíš na Starém Městě - zajímavé by bylo pokusit se i o dataci dle dobře rozeznatelných nápisů.       ...',

            ],
            [
                "link" => "/krajina-s-rickou-a-kostelikem/", "img" => "/templates/artopos/images/redac/thumbnail///138_47830.jpg",
                "author" => 'Jan Jáchym',
                "title" => 'Krajina s říčkou a kostelíkem',
                "status" => 'Hledáme',
                "description" => 'Výrazný motiv, snad odněkud ze západních Čech, kde působil...                  ...',

            ],
            [
                "link" => "/most-pod-slovanem/", "img" => "/templates/artopos/images/redac/thumbnail///136_6ub085162.jpg",
                "author" => 'Jan Jáchym',
                "title" => 'Most pod Slovanem',
                "status" => 'Hledáme',
                "description" => 'Pohled na most - nejspíš však ne v Plzni, kde Jáchym působil a kde také pod Slovany žádný takový most nestál a hotel Slovan byl jinde... ...',

            ],
            [
                "link" => "/prodavacky-kvetin/", "img" => "/templates/artopos/images/redac/thumbnail///135_6ub085014.jpg",
                "author" => 'Zdeněk Ostrčil',
                "title" => 'Prodavačky květin',
                "status" => 'Hledáme',
                "description" => 'Obraz filmového výtvarníka a režiséra působícího ve Zlíně situuje scénu nejspíš někam do této části České republiky. Hezká scéna plná dobových reálií 60. let. Kdysi byl ...',

            ],
            [
                "link" => "/pardubicky-gigant/", "img" => "/templates/artopos/images/redac/thumbnail///134_6uo112262.jpg",
                "author" => ' ',
                "title" => 'Pardubický gigant',
                "status" => 'Hledáme',
                "description" => 'Obraz s tímto záhadným názvem byl v majetku ministerstva zahraničí, kam byl zakoupen v roce 1953 a pak se nacházel na zastupitelstvu  Helsinkách.  V roce 2013 ...',

            ],
            [
                "link" => "/hradni-ulice/", "img" => "/templates/artopos/images/redac/thumbnail///133_6uo111149r.jpg",
                "author" => 'Jarmila Lipenská',
                "title" => 'Hradní ulice v Chebu',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/trh-na-kladne/", "img" => "/templates/artopos/images/redac/thumbnail///132_131939.jpg",
                "author" => 'Jaro Procházka',
                "title" => 'Trh na Kladně',
                "status" => 'Hledáme',
                "description" => 'Pohled na trh s barokním mariánským sloupem.                    ...',

            ],
            [
                "link" => "/namesti-maleho-mesta/", "img" => "/templates/artopos/images/redac/thumbnail///131_7633.jpg",
                "author" => 'Jaro Procházka',
                "title" => 'Náměstí v Ratajích nad Sázavou',
                "status" => 'Nalezeno',
                "description" => 'Obraz ukazuje v pohledu od západu svažité náměstí v Ratajích nad Sázavou (okres Kutná Hora). Domy zachycené na levé (severní) straně náměstí byly v průběhu 20. ...',

            ],
            [
                "link" => "/casne-rano-v-bruggach/", "img" => "/templates/artopos/images/redac/thumbnail///129_61125.jpg",
                "author" => 'Jaro Procházka',
                "title" => 'Časně ráno v Bruggách',
                "status" => 'Hledáme',
                "description" => 'Studie k obrazu, jehož definitivní verze je vidět na fotografii v umělcově ateliéru.              ...',

            ],
            [
                "link" => "/ze-stare-prahy/", "img" => "/templates/artopos/images/redac/thumbnail///125_96730.jpg",
                "author" => 'Jaro Procházka',
                "title" => 'Motiv z Kampy',
                "status" => 'Hledáme',
                "description" => 'Motiv z Kampy od Čertvoky, kde se ve dvoře ancházel sklad a prodejna keramiky. Od 50. let do revoluce měl ve dvoře ateliér sochař Miroslav Vystrčil. ...',

            ],
            [
                "link" => "/pohled-na-gruz/", "img" => "/templates/artopos/images/redac/thumbnail///123_emil-filla-czech-1882-1953-view-over-gruz-1908.jpg",
                "author" => 'Emil Filla',
                "title" => 'Pohled na Gruž',
                "status" => 'Nalezeno',
                "description" => 'Jeden z několika málo známých Fillových obrazů z Gruže ve fauvistickém duchu. Filla ho namaloval během letního malířova pobytu v dalmatském Dubrovníku, kam cestoval se svým přítelem spisovatelem ...',

            ],
            [
                "link" => "/veznice/", "img" => "/templates/artopos/images/redac/thumbnail///122_screenshot-2014-04-27-21.jpg",
                "author" => 'Otakar Nejedlý',
                "title" => 'Věznice',
                "status" => 'Hledáme',
                "description" => 'Neznámou budovu Nejedlý zobrazil v letech první světové války. Obraz vzbuzuje celou řadu otázek, ať se již týkají určení budovy, zvláštního válcového předmětu před branou nebo ...',

            ],
            [
                "link" => "/cernosice-s-prapory/", "img" => "/templates/artopos/images/redac/thumbnail///115_screenshot-2014-04-27-20.jpg",
                "author" => 'Otakar Nejedlý',
                "title" => 'Černošice s prapory',
                "status" => 'Hledáme',
                "description" => 'Symbolika praporů v českých barvách v roce 1915 by žádala objasnění, stejně jako je třeba určit nejen pozici, odkud byl obraz malován, a Nejedlého vztah k ...',

            ],
            [
                "link" => "/zeleznicni-nadrazi/", "img" => "/templates/artopos/images/redac/thumbnail///114_screenshot-2014-04-27-20.jpg",
                "author" => 'Zdenek Rykr',
                "title" => 'Železniční most',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/kamenny-most-v-krsicich/", "img" => "/templates/artopos/images/redac/thumbnail///113_screenshot-2014-04-27-21.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Kamenný most v Krsicích',
                "status" => 'Nalezeno',
                "description" => 'Klasicistní most přes řeku Skalici. Stejný motiv zachytil v roce 1930 Miloslav Holý. Zdá se, že Beneš most zobrazil z druhé strany, nicméně charakteristické stavení s ...',

            ],
            [
                "link" => "/jatecni-vez-v-holesovicich/", "img" => "/templates/artopos/images/redac/thumbnail///112_screenshot-2014-04-27-17.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Jateční věž v Holešovicích',
                "status" => 'Hledáme',
                "description" => 'V monografii J. Baleky je k stejnému roku připsán obraz Jateční ulice v Holešovičkách.             ...',

            ],
            [
                "link" => "/ulice-v-libni/", "img" => "/templates/artopos/images/redac/thumbnail///111_screenshot-2014-04-27-17.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Ulice v Libni',
                "status" => 'Hledáme',
                "description" => 'Jeden z nejslavnějších obrazů autorova sociálního období. Libeňská periferie je zde jen na značena a je vůbec otázkou, zda jde o motiv odvozený z reality. Navíc ...',

            ],
            [
                "link" => "/vecer-u-kozlovskeho-krizku/", "img" => "/templates/artopos/images/redac/thumbnail///110_gen.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Večer u kozlovského křížku',
                "status" => 'Nalezeno',
                "description" => 'Křižovatka polní cesty a silnice z Kladrub. V pozadí kopce Na Kobylinkách a Radlín u vsi Svaté Pole nad Horažďovicemi. Strom vlevo může být jeden z ...',

            ],
            [
                "link" => "/pradlena-v-chaloupkach/", "img" => "/templates/artopos/images/redac/thumbnail///109_gen-1.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Pradlena v Chaloupkách',
                "status" => 'Nalezeno',
                "description" => 'Publikováno v diplomové práci Hany Krubnerové (A. Moravec a Sdružení jihočeských výtvarníků, FF UP Olomouc, 2012, s. 87, s. 134), zde s malým písmenem u slova ...',

            ],
            [
                "link" => "/strakonsky-rybnik/", "img" => "/templates/artopos/images/redac/thumbnail///107_gen.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Strakoňský rybník',
                "status" => 'Hledáme',
                "description" => 'V diplomové práci Hany Krubnerové (A. Moravec a Sdružení jihočeských výtvarníků, FF UP Olomouc, 2012, s. 87) je uvedeno, že rybník leží v okolí Střelských Hoštic; ...',

            ],
            [
                "link" => "/pohled-na-kasperske-hory/", "img" => "/templates/artopos/images/redac/thumbnail///106_screenshot-2014-04-26-10.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Pohled na Kašperské Hory',
                "status" => 'Nalezeno',
                "description" => 'Pohled na Kašperské Hory od jihozápadu z doby, kdy sem malíř po deset let pravidelně zajížděl a na objednávku ředitele muzea Emanuela Boušky maloval obrazy již ...',

            ],
            [
                "link" => "/ve-strasicich-u-burianu/", "img" => "/templates/artopos/images/redac/thumbnail///108_burianu.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Ve Strašicích u Burianů',
                "status" => 'Nalezeno',
                "description" => 'Obraz z posledního údobí autorovy tvorby je publikován v diplomové práci Hany Krubnerové (A. Moravec a Sdružení jihočeských výtvarníků, FF UP Olomouc, 2012, s. 90, s. ...',

            ],
            [
                "link" => "/cesta-v-polich/", "img" => "/templates/artopos/images/redac/thumbnail///104_moravec-alois_cesta-v-polich-1940.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Cesta v polích',
                "status" => 'Hledáme',
                "description" => 'Jak pásmo hor v pozadí naznačuje, jde o krajinu z okolí Trhových Svin.              ...',

            ],
            [
                "link" => "/pohled-na-trhove-sviny/", "img" => "/templates/artopos/images/redac/thumbnail///103_moravec.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Pohled na Trhové Sviny',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/vstup-na-stadion-fotbaloveho-klubu-dfc-prag-v-praze-na-letne/", "img" => "/templates/artopos/images/redac/thumbnail///102_unnamed.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Vstup na stadion D.F.C. Prag v Praze',
                "status" => 'Nalezeno',
                "description" => 'Vstup na stadion fotbalového klubu D.F.C. Prag (Deutscher Fussball Club), nejznámějšího a nejúspěšnějšího fotbalového týmu německé menšiny v Praze. (Kolem klubu se soustedila tehdejší německy mluvící ...',

            ],
            [
                "link" => "/na-jezu/", "img" => "/templates/artopos/images/redac/thumbnail///100_o_602.jpg",
                "author" => 'Miloš Jiránek',
                "title" => 'Na jezu',
                "status" => 'Nalezeno',
                "description" => 'Motiv koupání se v Jiránkově díle opakuje častěji. Setkáme se s ním už na obrazových sériích z Obříství (1898) nebo jihomoravské Velké (1902), kde se ještě ...',

            ],
            [
                "link" => "/predjari-na-periferii/", "img" => "/templates/artopos/images/redac/thumbnail///474_o_148.jpg",
                "author" => 'Karel Holan',
                "title" => 'Předjaří na periferii (Letná)',
                "status" => 'Nalezeno',
                "description" => 'Vodítkem pro určení tohoto blíže nespecifikovaného motivu z pražské periferie byl nápis na štítu vlevo, který lze přečíst jako D. F. C. Fotbalový klub D.F.C. Prag ...',

            ],
            [
                "link" => "/z-prazske-periferie/", "img" => "/templates/artopos/images/redac/thumbnail///95_o_74.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Z pražské periferie',
                "status" => 'Hledáme',
                "description" => 'Motiv z periferie, v pozadí snad most s jedoucím vlakem, ovšem není to jasně identifikovatelné (Smíchov?).           ...',

            ],
            [
                "link" => "/na-hradcanech/", "img" => "/templates/artopos/images/redac/thumbnail///93_o_273_orez.jpg",
                "author" => 'Vincenc Beneš',
                "title" => 'Na Hradčanech',
                "status" => 'Nalezeno',
                "description" => 'Autor stál na oblíbeném místě malířů nad Novým Světem. Vpravo vzadu vedle Černínského paláce vykukuje rozhledna na Petříně.         ...',

            ],
            [
                "link" => "/fotbalove-hriste/", "img" => "/templates/artopos/images/redac/thumbnail///92_o_71.jpg",
                "author" => 'Karel Holan',
                "title" => 'Fotbalové hřiště',
                "status" => 'Hledáme',
                "description" => 'Raný obraz ještě ze sociálního období patří k tomu nejpůsobivějšímu z Holanova díla, pávě díky své atypičnosti. Zobrazuje tresť lidových zábav - najdeme tu cirkus, pouť ...',

            ],
            [
                "link" => "/z-zelenova/", "img" => "/templates/artopos/images/redac/thumbnail///91_569-alois-moravec-z-zelenova.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Z Želenova',
                "status" => 'Nalezeno',
                "description" => 'V pozdním věku 79 let namaloval Alois Moravec tento obraz z "jeho" Pošumaví z malé obce Frymburk, která se za minulého režimu musela přejmenovat na Želenov ...',

            ],
            [
                "link" => "/privoz-na-luznici-u-rudne/", "img" => "/templates/artopos/images/redac/thumbnail///90_48634.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Přívoz na Lužnici u Roudné',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/na-otave-u-strakonic/", "img" => "/templates/artopos/images/redac/thumbnail///89_54453.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Na Otavě u Strakonic',
                "status" => 'Hledáme',
                "description" => 'Motiv v typickém autorově duchu, kdy krajinu doplňuje stafáží pracujících vesničanů, lze identifikovat podle strakonického hradu v pozadí. Moravec v tomto kraji hojně pobýval a maloval, ...',

            ],
            [
                "link" => "/kostel-nanebevzeti-panny-marie-v-trhovych-svinech/", "img" => "/templates/artopos/images/redac/thumbnail///88_54454.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Kostel Nanebevzetí Panny Marie v Trhových Svinech',
                "status" => 'Nalezeno',
                "description" => 'Z obrázku současného stavu plyne, že situace na místě zůstala v podstatě stejná: všechny domy tu dodnes stojí, zmizely jen vzrostlé stromy a s nimi i ...',

            ],
            [
                "link" => "/horni-porici-na-rece-otave/", "img" => "/templates/artopos/images/redac/thumbnail///87_122042.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Horní Poříčí na řece Otavě',
                "status" => 'Nalezeno',
                "description" => 'Alois Moravec vyrostl v nedalekých Střelských Hošticích a do rodného kraje se rád vracel. Často pobýval v nedalekém Poříčí u majitele Šebestova, dnes Kubešova mlýna Petra ...',

            ],
            [
                "link" => "/v-hermani/", "img" => "/templates/artopos/images/redac/thumbnail///86_35677.jpg",
                "author" => 'Alois Moravec',
                "title" => 'V Heřmani',
                "status" => 'Hledáme',
                "description" => 'Nedatované, nejspíš pozdní dílo Aloise Moravce. V jižních Čechách existují dvě lokality s tímto jménem, v tomto případě jde o menší ves ležící pod Českými Budějovicemi. ...',

            ],
            [
                "link" => "/hradiste-sedlo/", "img" => "/templates/artopos/images/redac/thumbnail///85_30078.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Hradiště Sedlo',
                "status" => 'Hledáme',
                "description" => 'Malý skicovitě pojatý obrázek z pozdní Moravcovy tvorby zobrazuje hradiště Sedlo u Sušice, nejspíš z nějakého místa na východ od něj. Možná souvisí s častými cestami ...',

            ],
            [
                "link" => "/ulice/", "img" => "/templates/artopos/images/redac/thumbnail///84_o_138.jpg",
                "author" => 'Čestmír Kafka',
                "title" => 'Ulice',
                "status" => 'Hledáme',
                "description" => 'Výjimečná krajina v Kafkově díle zobrazuje blíže neurčenou městskou scenérii (Nový svět na Hradčanech?) v duchu tehdy populárního "posledního modernisty" pařížské školy Bernarda Buffeta (kresebný charakter, ...',

            ],
            [
                "link" => "/chebska-veduta-v-modrem/", "img" => "/templates/artopos/images/redac/thumbnail///83_pozemkovy_urad_02.jpg",
                "author" => 'Bojmír Hutta',
                "title" => 'Chebská veduta v modrém',
                "status" => 'Hledáme',
                "description" => 'Obraz z Huttova pozdního období tvorby zachycuje jednu z nejvýraznějších pohledových dominant Chebu - železniční viadukt, který v jz části města překonává řeku Ohři. Pochází z ...',

            ],
            [
                "link" => "/stary-cheb-–-trcky-z-lipy/", "img" => "/templates/artopos/images/redac/thumbnail///82_pozemkovy_urad_01.jpg",
                "author" => 'Bojmír Hutta',
                "title" => 'Starý Cheb',
                "status" => 'Nalezeno',
                "description" => 'Obraz zachycuje stav ještě před asanací, která proběhla na začátku 60. let, jíž padla za oběť celý blok domů, jež tvořil pravou stranu této uličky; z ...',

            ],
            [
                "link" => "/stary-cheb-–-jakubska/", "img" => "/templates/artopos/images/redac/thumbnail///80_spurny_02.jpg",
                "author" => 'Bojmír Hutta',
                "title" => 'Starý Cheb – Jakubská',
                "status" => 'Nalezeno',
                "description" => 'Záběr po Růžové ulici s koncem, který tvoří Jakubská. Domy vzadu zcela neodpovídají skutečnosti, kterou si zde malíř mírně uzpůsobil; pravá strana domů je dnes zcela ...',

            ],
            [
                "link" => "/krajina-v-okoli-skalne/", "img" => "/templates/artopos/images/redac/thumbnail///79_o_1166_muzeum_cheb.jpg",
                "author" => 'Bojmír Hutta',
                "title" => 'Krajina v okolí Skalné',
                "status" => 'Hledáme',
                "description" => 'Pohled na lomy v okolí Skalné. Obraz není datován, ale nejspíš jde o 50. či 60. léta.          ...',

            ],
            [
                "link" => "/cirkus-humberto-v-chebu/", "img" => "/templates/artopos/images/redac/thumbnail///78_o_1261_muzeum_cheb.jpg",
                "author" => 'Bojmír Hutta',
                "title" => 'Cirkus Humberto v Chebu',
                "status" => 'Nalezeno',
                "description" => 'Cirkus si své šapitó rozložil na místě, kde pak vzápětí na zač. 60. let vznikla čtveřice paneláků. Ta dnes zakrývá řadu domů v Karlově ulici, které ...',

            ],
            [
                "link" => "/z-ceskeho-krumlova-273/", "img" => "/templates/artopos/images/redac/thumbnail///77_kuba-predmesti-34345.jpg",
                "author" => 'Ludvík Kuba',
                "title" => 'Z Českého Krumlova',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/lounovice-pod-blanikem-270/", "img" => "/templates/artopos/images/redac/thumbnail///73_kuba-lounovice.jpg",
                "author" => 'Ludvík Kuba',
                "title" => 'Louňovice pod Blaníkem',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/nadvori-klastera-na-frantisku-i-265/", "img" => "/templates/artopos/images/redac/thumbnail///72_holan-frantisek45061.jpg",
                "author" => 'Karel Holan',
                "title" => 'Nádvoří kláštera Na Františku II',
                "status" => 'Nalezeno',
                "description" => 'Jeden z dvorů kláštera sv. Anežky, pohled od severu. Ve stejném roce zachytil Holan tento motiv ještě z druhé strany nádvoří.      ...',

            ],
            [
                "link" => "/mestsky-motiv-s-vezi-262/", "img" => "/templates/artopos/images/redac/thumbnail///71_holan.jpg",
                "author" => 'Karel Holan',
                "title" => 'Nádvoří kláštera Na Františku I',
                "status" => 'Nalezeno',
                "description" => 'V roce 1963 se majitelem kláštera stala Národní galerie, která ho renovovala pro své expozice. Průjezd je dnes zaslepen skleněnou výplní. Ve stejném roce zachytil Holan ...',

            ],
            [
                "link" => "/brevnovska-klasterni-zahrada-259/", "img" => "/templates/artopos/images/redac/thumbnail///69_holan-brevnov.jpg",
                "author" => 'Karel Holan',
                "title" => 'Břevnovská klášterní zahrada',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/z-provence-motiv-z-antibes-nebo-cagnes-sur-mer/", "img" => "/templates/artopos/images/redac/thumbnail///68_16034.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Z Provence (Motiv z Antibes nebo Cagnes sur Mer)',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/z-libne/", "img" => "/templates/artopos/images/redac/thumbnail///67_z-libne.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Z Libně',
                "status" => 'Hledáme',
                "description" => 'Do roku 1922 je v monografii J. Baleky datován obraz Domy v Libni, který v knize však není reprodukován - je to tento? Obraz by mohl ...',

            ],
            [
                "link" => "/zeleznicni-stanice/", "img" => "/templates/artopos/images/redac/thumbnail///65_45828.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Stanice v Jetěticích',
                "status" => 'Nalezeno',
                "description" => 'Nápis odpovídá názvu Jetětice, jak byla stanice nazvána původně; ovšem ležela poměrně daleko od obce. Jedná se však nejspíš o současné nádraží Červená. Je to nejspíš ...',

            ],
            [
                "link" => "/naves/", "img" => "/templates/artopos/images/redac/thumbnail///63_46739.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Tání ve Slatině',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/u-veleslavina-v-predjari/", "img" => "/templates/artopos/images/redac/thumbnail///57_119298.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'U Veleslavína v předjaří',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/lom-v-sarce-243/", "img" => "/templates/artopos/images/redac/thumbnail///54_130213.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Krajina s lomem v Šárce',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/jarni-motiv-z-motola-ii-241/", "img" => "/templates/artopos/images/redac/thumbnail///53_132825.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Jarní motiv z Motola II',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/motiv-z-kutne-hory/", "img" => "/templates/artopos/images/redac/thumbnail///52_o_200_pelc.jpg",
                "author" => 'Antonín Pelc',
                "title" => 'Motiv z Kutné Hory',
                "status" => 'Hledáme',
                "description" => 'Když Antonín Pelc v deseti letech osiřel, ujal se ho strýc - středoškolský profesor z Kutné Hory, kde od té doby bydlel. Těžko identifikovatelný motiv z ...',

            ],
            [
                "link" => "/z-bubence/", "img" => "/templates/artopos/images/redac/thumbnail///51_o_317_hubacek.jpg",
                "author" => 'Josef Hubáček',
                "title" => 'Z Bubenče',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/trh-ve-wiener-neustadt/", "img" => "/templates/artopos/images/redac/thumbnail///50_125504.jpg",
                "author" => 'Hans Hamza',
                "title" => 'Trh ve Wiener Neustadt',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/prazske-zakouti-230/", "img" => "/templates/artopos/images/redac/thumbnail///47_4982.jpg",
                "author" => 'Jaro Procházka',
                "title" => 'Z Nového Světa na Hradčanech',
                "status" => 'Nalezeno',
                "description" => 'Pohled ze severovýchodniho konce Kapucínské směrem k Novému Světu. Dům s arkýřem je rohový dům č.p. 78 v ulici Nový Svět.      ...',

            ],
            [
                "link" => "/krajina-s-hradem-rabi/", "img" => "/templates/artopos/images/redac/thumbnail///46_9125.jpg",
                "author" => 'Alois Moravec',
                "title" => 'Krajina s hradem Rabí',
                "status" => 'Nalezeno',
                "description" => 'Dálkový pohled na hrad Rabí od východu lokalizuje mlýn při Černíčském potoku (Bojanovice 35), který zde dodnes stojí v původní podobě, citllivě opraven. Podle vyprávění maitele ...',

            ],
            [
                "link" => "/lipnice-1890/", "img" => "/templates/artopos/images/redac/thumbnail///44_o_166_zrzavy.jpg",
                "author" => 'Jan Zrzavý',
                "title" => 'Lipnice 1890',
                "status" => 'Nalezeno',
                "description" => 'Jeden z obrazů z Vysočiny malovaných během několika let po Mnichovu, kdy se na protest proti zradě Francie zařekl, že ji již nikdy nenavštíví, a poprvé ...',

            ],
            [
                "link" => "/jedouchov/", "img" => "/templates/artopos/images/redac/thumbnail///43_131042.jpg",
                "author" => 'Jan Zrzavý',
                "title" => 'Jedouchov',
                "status" => 'Nalezeno',
                "description" => 'Další z obrázků ze Zrzavého rodného kraje, které namaloval v prvních letech války: Jedouchov je 7 km od rodné Okrouhlice. Podoba i atmosféra venkovské návsi zůstala ...',

            ],
            [
                "link" => "/zwieseler-waldhaus-220/", "img" => "/templates/artopos/images/redac/thumbnail///40_131289.jpg",
                "author" => 'Jaroslav Šetelík',
                "title" => 'Zwieseler Waldhaus',
                "status" => 'Nalezeno',
                "description" => 'Raný Šetelíkův obraz ještě z dob studií na pražské akademii zobrazuje Zwieseler Waldhaus, údajně nejstarší restaurant Bavorského lesa - výčep je zde už od roku 1779. Nachází ...',

            ],
            [
                "link" => "/z-ceskeho-stredohori/", "img" => "/templates/artopos/images/redac/thumbnail///39_131264.jpg",
                "author" => 'Otakar Nejedlý',
                "title" => 'Z Českého středohoří',
                "status" => 'Hledáme',
                "description" => 'Pro znalce Středohoří by neměl být problém určit, co za kopce a vesnici zde Nejedlý zobrazil.           ...',

            ],
            [
                "link" => "/motiv-z-malomesta/", "img" => "/templates/artopos/images/redac/thumbnail///38_131048.jpg",
                "author" => 'Alois Kalvoda',
                "title" => 'Motiv z malého města',
                "status" => 'Hledáme',
                "description" => 'Drobný olej na papíru zobrazuje malé město s holými vršky v pozadí - mělo by jít krásně určit, tipoval bych jižní Moravu.     ...',

            ],
            [
                "link" => "/stramberske-zahradnictvi/", "img" => "/templates/artopos/images/redac/thumbnail///37_90304.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Žamberské zahradnictví',
                "status" => 'Hledáme',
                "description" => 'Ve stejném roce maloval Zahradníkův domek v Žamberku - není zcela jasné, zda jde o tento obraz, spíše nikoliv. Několikrát neśpěšně prodáváno (Vltavín, Dototheum) pod názvem ...',

            ],
            [
                "link" => "/spania-dolina/", "img" => "/templates/artopos/images/redac/thumbnail///36_131011.jpg",
                "author" => 'Antonín Hudeček',
                "title" => 'Špania Dolina',
                "status" => 'Hledáme',
                "description" => 'V malebné hornické vsi nedaleko Banské Bystrice už dřevěnice asi nenajdeme a motiv půjde určit jen obtížně.          ...',

            ],
            [
                "link" => "/v-bubenci/", "img" => "/templates/artopos/images/redac/thumbnail///35_131007.jpg",
                "author" => 'Arno Naumann',
                "title" => 'V Bubenči',
                "status" => 'Nalezeno',
                "description" => 'V popředí zahradní besídka v čínském stylu u zdi statku č. p. 5 v Pelléově ulici v Bubenči.         ...',

            ],
            [
                "link" => "/pohled-na-troju-od-bubence/", "img" => "/templates/artopos/images/redac/thumbnail///34_130999.jpg",
                "author" => 'Vlastimil Rada',
                "title" => 'Pohled na Tróju od Bubenče',
                "status" => 'Hledáme',
                "description" => 'Definitivní verze motivu se nachází ve sbírkách GHMP.                   ...',

            ],
            [
                "link" => "/ve-stresovickach-207/", "img" => "/templates/artopos/images/redac/thumbnail///33_111547.jpg",
                "author" => 'Karel Holan',
                "title" => 'Ve Střešovičkách',
                "status" => 'Nalezeno',
                "description" => 'Jde o pohled východně ulicí Na Petynce směrem ke křižovatce s ulicí Na Zástřelu, jejíž schody severním směrem jsou v levé části obrazu.    ...',

            ],
            [
                "link" => "/viadukt-na-okori/", "img" => "/templates/artopos/images/redac/thumbnail///32_126210.jpg",
                "author" => 'Karel Holan',
                "title" => 'Viadukt na Okoři',
                "status" => 'Nalezeno',
                "description" => 'Poválečný obraz Karla Holana: viadukt se za 60 let nezměnil, chajda vpředu už ano.             ...',

            ],
            [
                "link" => "/vesnice-u-stozic/", "img" => "/templates/artopos/images/redac/thumbnail///589_31_70523.jpg",
                "author" => 'Vratislav Hlava (Rudolf Bém)',
                "title" => 'Vesnice u Stožic',
                "status" => 'Hledáme',
                "description" => 'Slavíčkovsky laděná krajina ze Stožic nedaleko Chelčic, kam Hlava zajížděl mezi dvěma válkami.              ...',

            ],
            [
                "link" => "/frantiskanska-zahrada/", "img" => "/templates/artopos/images/redac/thumbnail///30_orlik.jpg",
                "author" => 'Emil Orlik',
                "title" => 'Františkánská zahrada',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/obleva-ve-sneznem/", "img" => "/templates/artopos/images/redac/thumbnail///28_101089.jpg",
                "author" => 'František Emler',
                "title" => 'Obleva ve Sněžném',
                "status" => 'Hledáme',
                "description" => '',

            ],
            [
                "link" => "/nezdicky-potok-u-zichovic/", "img" => "/templates/artopos/images/redac/thumbnail///26_1-1.jpg",
                "author" => 'Vratislav Hlava (Rudolf Bém)',
                "title" => 'Nezdický potok u Žichovic',
                "status" => 'Nalezeno',
                "description" => 'Pohled na louky za kopcem Hradec směrem k zalesněnému vrchu Ňoulavka. Podmáčené louky byly na konci 70. let meliorovány, meandrující potok srovnán a zčásti vybetonován, louky ...',

            ],
            [
                "link" => "/otava-pod-rabim/", "img" => "/templates/artopos/images/redac/thumbnail///21_1.jpg",
                "author" => 'Vratislav Hlava (Rudolf Bém)',
                "title" => 'Otava pod Rabím',
                "status" => 'Nalezeno',
                "description" => 'Obraz z pozdního období jeho tvorby, kdy pravidelně zajížděl na letní pobyty na zámek v nedalekých Žichovicích. Vlastenec Hlava, který vytvořil portrét Žižky, hrad Rabí maloval ...',

            ],
            [
                "link" => "/valkova-restaurace/", "img" => "/templates/artopos/images/redac/thumbnail///20_valkova-restaurace.jpg",
                "author" => 'Karel Holan',
                "title" => 'Válkova restaurace',
                "status" => 'Nalezeno',
                "description" => 'Charakteristická holanovská scenérie zimní pražské periferie zobrazuje bránu Císařského mlýna v Bubenči. Pohled je o to vzácnější, že známé fotografie ji zachycují z čelního pohledu, nikoliv ...',

            ],
            [
                "link" => "/casablanca/", "img" => "/templates/artopos/images/redac/thumbnail///19_o_460_back_pelc.jpg",
                "author" => 'Antonín Pelc',
                "title" => 'Casablanca',
                "status" => 'Nalezeno',
                "description" => 'Akvarel pochází z Pelcova nedobrovolného pobytu v Cacablance spolu s výtvarníky Maximem Kopfem, Alénem Divišem a Adolfem Hoffmeisterem během druhé světové války (více viz zde). Skupina ...',

            ],
            [
                "link" => "/most-v-krsicich/", "img" => "/templates/artopos/images/redac/thumbnail///18_o_351_holy.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Most v Krsicích',
                "status" => 'Nalezeno',
                "description" => 'V roce 1930 Holý poprvé pobýval v nedalekých Mirovicích. Na obraze zachtil kamenný klasicistní most přes řeku Skalici. Použil tu nezvyklou perspektivní zkratku, když střední část ...',

            ],
            [
                "link" => "/cementarna-v-podoli/", "img" => "/templates/artopos/images/redac/thumbnail///17_o_349_holy_cementarna_v_podoli.jpg",
                "author" => 'Miloslav Holý',
                "title" => 'Cementárna v Podole',
                "status" => 'Nalezeno',
                "description" => 'Obraz s motivem zaniklé cementárny v Praze - Podolí  (dříve se používal i název Podol). Na jejím místě dnes stojí známý plavecký stadion. K historii cementárny ...',

            ],
            [
                "link" => "/stary-cheb/", "img" => "/templates/artopos/images/redac/thumbnail///13_hutta_-89-116cm2.jpg",
                "author" => 'Bojmír Hutta',
                "title" => 'Starý Cheb (Pohled od hradu)',
                "status" => 'Hledáme',
                "description" => 'Fotografie tohoto raného Huttova obrazu z období hned po jeho příchodu do Chebu jsem našel v počítači - nejspíš mi ji někdo poslal v souvislosti s ...',

            ],
            [
                "link" => "/pristav-v-honfleur/", "img" => "/templates/artopos/images/redac/thumbnail///11_o_121_rada.jpg",
                "author" => 'Vlastimil Rada',
                "title" => 'Přístav v Honfleur',
                "status" => 'Nalezeno',
                "description" => 'Motiv ze starého přístavu zachycuje La Lieutenance, pozůstakty sídla královského guvernéra ze šestnáctého století. Honfleur byl oblíbeným místem malířů 19. st. (Turner, Courbet, Boudin, Jongkind, Monet). ...',

            ],
            [
                "link" => "/rybarska-ulice-v-ceskem-krumlove/", "img" => "/templates/artopos/images/redac/thumbnail///7_o_390_kuba.jpg",
                "author" => 'Ludvík Kuba',
                "title" => 'Rybářská ulice v Českém Krumlově',
                "status" => 'Nalezeno',
                "description" => 'Obraz byl veden ve sbírkách Galerie výtvarného umění v Chebu jako Labská zátoka u Poděbrad - název se však opíral jen o fakt, že se zde ...',

            ],
            [
                "link" => "/zichovicke-podzamci/", "img" => "/templates/artopos/images/redac/thumbnail///16_o_612.jpg",
                "author" => 'František Viktor Mokrý',
                "title" => 'Žichovické podzámčí',
                "status" => 'Nalezeno',
                "description" => 'Motiv zobrazuje obytnou budovu s barokním štítem v hospodářské části zámku Žichovice (lihovar, pak mlékárna, nyní bez využití a opuštěn), při cestě podél Nezdického potoka. Obraz ...',

            ],
            [
                "link" => "/kontrolni-vez/", "img" => "/templates/artopos/images/redac/thumbnail///2_kontrolni-vez.jpg",
                "author" => 'Dobroslav Foll',
                "title" => 'Kontrolní věž',
                "status" => 'Hledáme',
                "description" => 'Dílo z raného období autorovy tvorby ještě z 50. let, v níž se  vyrovnával s požadavkem socialistického realismu, nicméně vykazuje  autorovo tíhnutí k zobrazování civilizačních výdobytků ...',

            ]
        ];
    }
}
