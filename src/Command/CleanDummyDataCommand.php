<?php
/**
 * Created by PhpStorm.
 * User: twisty01
 * Date: 11-Apr-18
 * Time: 14:54
 */

namespace App\Command;

use App\Entity\Author;
use App\Entity\Institution;
use App\Entity\Painting;
use App\Entity\Post;
use App\Entity\Tag;
use App\Repository\AuthorRepository;
use App\Repository\InstitutionRepository;
use App\Repository\PaintingRepository;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanDummyDataCommand extends ContainerAwareCommand {
    private $authorRepository;
    private $paintingRepository;
    private $institutionRepository;
    private $tagRepository;
    private $postRepository;

    public function __construct(
        PaintingRepository $paintingRepository,
        PostRepository $postRepository,
        TagRepository $tagRepository,
        InstitutionRepository $institutionRepository,
        AuthorRepository $authorRepository
    ) {
        parent::__construct();
        $this->paintingRepository = $paintingRepository;
        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
        $this->institutionRepository = $institutionRepository;
        $this->authorRepository = $authorRepository;
    }

    protected function configure() {
        $this->setName('app:clean-dummy-data');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        // add institutions
        $this->removeAll($this->postRepository);
        $this->removeAll($this->paintingRepository);
        $this->removeAll($this->tagRepository);
        $this->removeAll($this->institutionRepository);
        $this->removeAll($this->authorRepository);
    }


    private function removeAll(EntityRepository $r){
        foreach ($r->findAll() as $e){
            $r->delete($e);
        }
    }
}