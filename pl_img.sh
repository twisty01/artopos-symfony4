#!/usr/bin/env bash

mkdir "$1/placeholders";

I=0;

for fn in "$1"*;
 do
  I=$((I + 1));
  mv "${fn}" "${1}/placeholders/${I}.${fn##*.}";
 done
