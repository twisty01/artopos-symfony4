## Artopos Backend

#### Introduction
    This repository contains backend aplication in Symfony 4 with API and authentication.
    This project uses rest-bundle for REST API and oauth-server-bundle fo OAuth authentication, both bundle are from frinedsofsymfony.
    Oauth uses snakeCase naming for params and properties and API uses camelCase convetion for params and properties.
#### Requirements
* PHP7.1 and common extensions
* MySQL server
* Composer


#### Setup
* in .env file set `DATABASE_URL` to mysql server url
* run `composer install`
* run `bin/console doctrine:database:create`
* run `bin/console doctrine:schema:create`
* to import data to dabatase run `bin/console doctrine:database:import db_export.sql`
* to run server use `php -S localhost:8000 -t ./public/`
* for api tests run `./vendor/bin/simple-phpunit` 
