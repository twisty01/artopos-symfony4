-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: id5399832_artopos_symfony4
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_token`
--

DROP TABLE IF EXISTS `access_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_B6A2DD685F37A13B` (`token`),
  KEY `IDX_B6A2DD6819EB6921` (`client_id`),
  KEY `IDX_B6A2DD68A76ED395` (`user_id`),
  CONSTRAINT `FK_B6A2DD6819EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `FK_B6A2DD68A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_token`
--

LOCK TABLES `access_token` WRITE;
/*!40000 ALTER TABLE `access_token` DISABLE KEYS */;
INSERT INTO `access_token` VALUES (27,6,1,'MmQ1YmI5Njc1YjI1MmI2ZTM1Y2U5MGJjODA3NDIxNDJmMDQ3NTNiMzY2Mjg1NTA0MzZlN2IzZjcwYzdjY2U5Yw',1523978165,'user'),(28,6,1,'ZjQ5MGQ1NmU3ZWRjM2VjODcyN2JiZTRlM2FjMjRiNDZmZDk5MmQ0ZGJkYWQ0ZmIyOWQ0YzdkZmExN2NjYzFkYQ',1523978427,'user'),(29,6,1,'NDUxMGIxMWFlNDE2YzNiNGI3MDlmYzNjZWJiZWQ3MzA4YzlmZGMyMmM3MDE5MzkzMWY3YTc1Y2Q3ZDU4OTdmYg',1523978516,'user'),(30,6,1,'ZGM2NTMxN2Y4MTI5OGU2OTIzNTQ2MTI4NWYyZjU4MzZiZTczMTZhZWNmOTZlODdmYzdmYzNhMTdmODExZWZhMQ',1523978628,'user'),(31,6,1,'YzEwZjU4MTMzNzM5NTlhMTAzMmJjZjM0Yzc1ZDhjOThmYmNkYTkxZWRmMjRiMzU1MjY5YWIwYzczZjIxMTJmMg',1523979068,'user'),(32,6,1,'ZjZhYjQ3YjIzMzMxMTk0YWIzYmUwNWVkMjI2NmVlZDQ2ZjRjODI2Zjk4YzUxYjdjZTVkOGU5YTI1NmQ4YjljMA',1523979142,'user'),(33,6,1,'OWQyOWNmOTA4NDliOTFhOTRjZDU1YzE4ODgwNWU5YmFlOWI5MDc2MGQ5ZTY1MmNkNWM1OTNmNGRkNjhiMzhmMg',1523979204,'user'),(34,6,1,'NzhkMDUxMGJhYTM1MDVkYTdjYzQ3NmRhNGZiNzRjY2FiNDA4NDFiOGMwZjNkZjQwMDQ5ZWMzNmVhNmMzNTFhOQ',1523979281,'user'),(35,6,1,'ZGFkNGVmMDRjOGY5ZTJmYzQ3Njc2NWVkZDA4Yzg3OGRjMTNmYzZkNDQ4YzRlYzlkMTZhNTc0ZjhjNmMxODg0NA',1523979483,'user'),(36,6,1,'YWJjYzVhNTViM2E0NmY4NTRiMTBmOWEwYjYwOGEzY2ZhNmFlYTA5NWYzMjQ4YTYzNWRkZTk0MGQ1ZGZiZTM2Yw',1523980194,'user'),(37,6,1,'OTcxYjM1ZGI1ZDYwZmQyNDhkOGNmNWRjM2QzZjViYmRhZTFlNWE0ZDE0MGUyNjhhYTc1OGI0YWM5MWFmNzU0NA',1523980251,'user');
/*!40000 ALTER TABLE `access_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_code`
--

DROP TABLE IF EXISTS `auth_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect_uri` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_5933D02C5F37A13B` (`token`),
  KEY `IDX_5933D02C19EB6921` (`client_id`),
  KEY `IDX_5933D02CA76ED395` (`user_id`),
  CONSTRAINT `FK_5933D02C19EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `FK_5933D02CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_code`
--

LOCK TABLES `auth_code` WRITE;
/*!40000 ALTER TABLE `auth_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `author`
--

DROP TABLE IF EXISTS `author`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `born` date DEFAULT NULL,
  `died` date DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tn` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_public_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author`
--

LOCK TABLES `author` WRITE;
/*!40000 ALTER TABLE `author` DISABLE KEYS */;
INSERT INTO `author` VALUES (1153,NULL,NULL,'Adámek Ferdinand',NULL,NULL,NULL,NULL),(1154,NULL,NULL,'Ambrožová Vlasta J.',NULL,NULL,NULL,NULL),(1155,NULL,NULL,'autor neznámý',NULL,NULL,NULL,NULL),(1156,NULL,NULL,'Baar Hugo',NULL,NULL,NULL,NULL),(1157,NULL,NULL,'Balaš Zděněk',NULL,NULL,NULL,NULL),(1158,NULL,NULL,'Baran Otakar',NULL,NULL,NULL,NULL),(1159,NULL,NULL,'Bartovský Václav',NULL,NULL,NULL,NULL),(1160,NULL,NULL,'Bayer Bořek',NULL,NULL,NULL,NULL),(1161,NULL,NULL,'Beneš Vlastimil',NULL,NULL,NULL,NULL),(1162,NULL,NULL,'Beneš Vincenc',NULL,NULL,NULL,NULL),(1163,NULL,NULL,'Bibus Felix',NULL,NULL,NULL,NULL),(1164,NULL,NULL,'Teklý Otakar',NULL,NULL,NULL,NULL),(1165,NULL,NULL,'Tichý František',NULL,NULL,NULL,NULL),(1166,NULL,NULL,'Toman Václav',NULL,NULL,NULL,NULL),(1167,NULL,NULL,'Tomsa Karel',NULL,NULL,NULL,NULL),(1168,NULL,NULL,'Trampota Jan',NULL,NULL,NULL,NULL),(1169,NULL,NULL,'Turek František',NULL,NULL,NULL,NULL),(1170,NULL,NULL,'Tvrdoň Martin',NULL,NULL,NULL,NULL),(1171,NULL,NULL,'Tylš Emil',NULL,NULL,NULL,NULL),(1172,NULL,NULL,'Uher Karel',NULL,NULL,NULL,NULL),(1173,NULL,NULL,'Ullmann Josef',NULL,NULL,NULL,NULL),(1174,NULL,NULL,'Ullrych Bohumil',NULL,NULL,NULL,NULL),(1175,NULL,NULL,'Urban Bohumil Stanislav',NULL,NULL,NULL,NULL),(1176,NULL,NULL,'Vacek Jaroslav',NULL,NULL,NULL,NULL),(1177,NULL,NULL,'Vacke Josef',NULL,NULL,NULL,NULL),(1178,NULL,NULL,'Vaic Josef',NULL,NULL,NULL,NULL),(1179,NULL,NULL,'Vavřina František',NULL,NULL,NULL,NULL),(1180,NULL,NULL,'Vejrych Rudolf',NULL,NULL,NULL,NULL),(1181,NULL,NULL,'Vele Ladislav',NULL,NULL,NULL,NULL),(1182,NULL,NULL,'Vlk Ferdinand',NULL,NULL,NULL,NULL),(1183,NULL,NULL,'Vobecký František',NULL,NULL,NULL,NULL),(1184,NULL,NULL,'Vonášek Soter',NULL,NULL,NULL,NULL),(1185,NULL,NULL,'Vondrouš Jan Charles',NULL,NULL,NULL,NULL),(1186,NULL,NULL,'Votlučka Karel',NULL,NULL,NULL,NULL),(1187,NULL,NULL,'Vukovič Svetislav',NULL,NULL,NULL,NULL),(1188,NULL,NULL,'Waldhauser Antonín',NULL,NULL,NULL,NULL),(1189,NULL,NULL,'Weidinger Franz Xaver',NULL,NULL,NULL,NULL),(1190,NULL,NULL,'Werner Karel',NULL,NULL,NULL,NULL),(1191,NULL,NULL,'Wiesner Richard',NULL,NULL,NULL,NULL),(1192,NULL,NULL,'Wíšo Jaromír',NULL,NULL,NULL,NULL),(1193,NULL,NULL,'Wohlrab Hans',NULL,NULL,NULL,NULL),(1194,NULL,NULL,'Zahrádka Karel',NULL,NULL,NULL,NULL),(1195,NULL,NULL,'Zdrůbecký Jaroslav',NULL,NULL,NULL,NULL),(1196,NULL,NULL,'Zezula Oldřich',NULL,NULL,NULL,NULL),(1197,NULL,NULL,'Zikmund František',NULL,NULL,NULL,NULL),(1198,NULL,NULL,'Života Josef',NULL,NULL,NULL,NULL),(1199,NULL,NULL,'Zrzavý Jan',NULL,NULL,NULL,NULL),(1200,NULL,NULL,'Žufan Bořivoj',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `author` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `author_painting`
--

DROP TABLE IF EXISTS `author_painting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `author_painting` (
  `author_id` int(11) NOT NULL,
  `painting_id` int(11) NOT NULL,
  PRIMARY KEY (`author_id`,`painting_id`),
  KEY `IDX_8929C115F675F31B` (`author_id`),
  KEY `IDX_8929C115B00EB939` (`painting_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author_painting`
--

LOCK TABLES `author_painting` WRITE;
/*!40000 ALTER TABLE `author_painting` DISABLE KEYS */;
/*!40000 ALTER TABLE `author_painting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `random_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect_uris` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `secret` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `allowed_grant_types` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (6,'4h2q66dz1ksgoocwk4ksgk0wksww4sc4w0wgoss4s8ko44cw4o','a:0:{}','4rpyq7txeveo88ssw0cgwow4884k0g8oko4gkgogcs4g84kssk','a:2:{i:0;s:8:\"password\";i:1;s:13:\"refresh_token\";}');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institution`
--

DROP TABLE IF EXISTS `institution`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `institution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=460 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institution`
--

LOCK TABLES `institution` WRITE;
/*!40000 ALTER TABLE `institution` DISABLE KEYS */;
INSERT INTO `institution` VALUES (433,NULL,'Galerie výtvarného umění v Chebu',NULL),(434,NULL,'Muzeum Cheb',NULL),(435,NULL,'Muzeum Šumavy Sušice',NULL),(436,NULL,'Galerie Středočeského kraje Kutná Hora',NULL),(437,NULL,'Pro arte',NULL),(438,NULL,'Alšova jihočeská galerie v Hluboké nad Vltavou',NULL),(439,NULL,'Památník Mladovožicka, Mladá Vožice',NULL),(440,NULL,'Oblastní galerie v Liberci',NULL),(441,NULL,'Galerie Zlatá husa, Praha',NULL),(442,NULL,'Jihomoravské muzeum ve Znojmě',NULL),(443,NULL,'Národní galerie v Praze',NULL),(444,NULL,'Galerie hlavního města Prahy',NULL),(445,NULL,'Nadační fond Eleutheria',NULL),(446,NULL,'Sbírka Patrika Šimona',NULL),(447,NULL,'Galerie moderního umění v Roudnici n. L.',NULL),(448,NULL,'Západočeská galerie v Plzni',NULL),(449,NULL,'Galerie výtvarného umění v Ostravě',NULL),(450,NULL,'Město Cheb',NULL),(451,NULL,'Egerland-Museum Marktrewitz',NULL),(452,NULL,'soukromá sbírka',NULL),(453,NULL,'Kunstforum Ostdeutsche Galerie Regensburg',NULL),(454,NULL,'Oblastní galerie Vysočiny v Jihlavě',NULL),(455,NULL,'Galerie umění Karlovy Vary',NULL),(456,NULL,'Moravská galerie v Brně',NULL),(457,NULL,'Artotéka města Plzně',NULL),(458,NULL,'Galerie Klatovy / Klenová',NULL),(459,NULL,'Galerie České spořitelny',NULL);
/*!40000 ALTER TABLE `institution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `painting`
--

DROP TABLE IF EXISTS `painting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `painting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) DEFAULT NULL,
  `institution_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `perex` longtext COLLATE utf8mb4_unicode_ci,
  `painting_created` date DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `resolved` tinyint(1) DEFAULT NULL,
  `tn` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_public_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_66B9EBA0F675F31B` (`author_id`),
  KEY `IDX_66B9EBA010405986` (`institution_id`),
  KEY `IDX_66B9EBA0A76ED395` (`user_id`),
  CONSTRAINT `FK_66B9EBA010405986` FOREIGN KEY (`institution_id`) REFERENCES `institution` (`id`),
  CONSTRAINT `FK_66B9EBA0A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_66B9EBA0F675F31B` FOREIGN KEY (`author_id`) REFERENCES `author` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `painting`
--

LOCK TABLES `painting` WRITE;
/*!40000 ALTER TABLE `painting` DISABLE KEYS */;
INSERT INTO `painting` VALUES (112,1163,441,NULL,'','1842-12-13','Litoměřice','Phasellus tristique enim purus, et imperdiet nisi varius aliquam. Etiam tempor magna enim, vel blandit nibh vehicula non. Nullam et scelerisque ipsum. Duis non odio tellus. Maecenas suscipit fringilla ligula, vitae pharetra libero pulvinar a. Donec eros metus, tincidunt eu erat vitae, Sed non orci facilisis dui porttitor molestie.','http://www.artopos.net/templates/artopos/images/redac/middle_photo///1104_206022.jpg',49.533671,14.91097,1,1,1,'http://www.artopos.net/templates/artopos/images/redac/middle_photo///1104_206022.jpg',NULL),(113,1197,455,NULL,'','1842-12-13','Litoměřice','Phasellus tristique enim purus, et imperdiet nisi varius aliquam. Etiam tempor magna enim, vel blandit nibh vehicula non. Nullam et scelerisque ipsum. Duis non odio tellus. Maecenas suscipit fringilla ligula, vitae pharetra libero pulvinar a. Donec eros metus, tincidunt eu erat vitae, Sed non orci facilisis dui porttitor molestie.','http://www.artopos.net/templates/artopos/images/redac/middle_photo///1104_206022.jpg',51.533671,15.291097,1,1,1,'http://www.artopos.net/templates/artopos/images/redac/middle_photo///1104_206022.jpg',NULL),(114,1155,438,NULL,'','1842-12-13','Litoměřice','Phasellus tristique enim purus, et imperdiet nisi varius aliquam. Etiam tempor magna enim, vel blandit nibh vehicula non. Nullam et scelerisque ipsum. Duis non odio tellus. Maecenas suscipit fringilla ligula, vitae pharetra libero pulvinar a. Donec eros metus, tincidunt eu erat vitae, Sed non orci facilisis dui porttitor molestie.','http://www.artopos.net/templates/artopos/images/redac/middle_photo///1104_206022.jpg',50.533671,14.291097,1,1,1,'http://www.artopos.net/templates/artopos/images/redac/middle_photo///1104_206022.jpg',NULL),(115,1173,437,NULL,'','1842-12-13','Havlíčkův Brod','Sed congue fringilla justo sed lacinia. Nulla eget dictum lectus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus dui eros, porttitor at eleifend ac, dignissim nec ligula. Curabitur feugiat varius nunc a feugiat. Aliquam a euismod orci. Donec at dolor nibh. Aenean sed mi ultrices metus volutpatnon rutrum risus ipsum lacinia nisi. In hac habitasse platea dictumst. Nam luctus id ipsum ut aliquam.','http://www.artopos.net/templates/artopos/images/discussion/middle_photo/1_o-3038.jpg',49.686607,15.618406,1,1,1,'http://www.artopos.net/templates/artopos/images/discussion/middle_photo/1_o-3038.jpg',NULL),(116,1182,451,NULL,'','1842-12-13','Jindřichův Hradec','Etiam vitae lacinia dolor, vitae suscipit velit. Donec quis elementum elit. Suspendisse sagittis dapibus eros, blandit scelerisque ipsum scelerisque sit amet. Curabitur mollis metus sed erat vehicula, et sagittis sem commodo. Quisque et sapien quis erat imperdiet porta ac vel massa. Fusce finibus felis eu odio bibendum, quis tristique libero ullamcorper. Aliquam vulputate lorem vitae nunc tempor, eu rutrum dui pulvinar. Maecenas condimentum laoreet, magna quam convallis libero, non fringilla massa risus eget lectus. Suspendisse non rhoncus tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.','http://www.artopos.net/templates/artopos/images/redac/middle_photo///1116_hudecek.jpg',49.139816,15.211912,1,1,1,'http://www.artopos.net/templates/artopos/images/redac/middle_photo///1116_hudecek.jpg',NULL),(117,1169,454,NULL,'','1842-12-13','Týniště nad Orlicí','Aenean id tortor tellus. Vivamus commodo vulputate lectus, eget accumsan nisi mollis vel. In in mi elit. In hendrerit, sem et pulvinar viverra, dui enim malesuada nunc, aliquam fringilla mauris velit non felis. Mauris eget facilisis nunc, quis aliquet massa. Duis ut diam erat. Suspendisse vel efficitur orci, eu lacinia leo.tortor. Fusce eget nisl tortor. Nam sagittis egestas magna dapibus aliquet. Mauris sed nisi non sem feugiat placerat.','http://www.artopos.net/templates/artopos/images/redac/middle_photo///1101_moravec.jpg',50.17385,16.05657,1,1,0,'http://www.artopos.net/templates/artopos/images/redac/middle_photo///1101_moravec.jpg',NULL),(118,1163,447,NULL,'','1842-12-13','Nečín','Curabitur feugiat fringilla neque ut imperdiet. Duis eget nisl justo. Maecenas tempor ipsum mauris, sed egestas odio consequat laoreet. Donec vitae dui sit amet sem placerat euismod. Curabitur rhoncus enim egestas felis, a semper lacus suscipit et. Proin vel erat lobortis, consectetur leo at, fringilla arcu. Aliquam pellentesque, turpis in sagittis lobortis, libero risus ultricies urna, eget iaculis lectus risus eu libero. Quisque accumsan malesuada vehicula. Phasellus vulputate, lorem non luctus iaculis, sem ante auctor tellus, ut tincidunt nibh nisl ac lacus.','http://www.artopos.net/templates/artopos/images/redac/middle_photo///1113_dvorek.jpg',49.705627,14.213514,1,1,0,'http://www.artopos.net/templates/artopos/images/redac/middle_photo///1113_dvorek.jpg',NULL),(119,1162,443,NULL,'','1842-12-13','Kutná Hora','Proin fringilla nunc ac sem blandit condimentum. Suspendisse potenti. Fusce gravida risus sed ligula porta dictum. Sed id iaculis libero. Sed vel libero ultricies tellus egestas eleifend. Cras efficitur semper sem, et vulputate nisi mollis interdum. Nullam mollis lectus a est luctus, vitae imperdiet sapien eleifend. Vivamus eque, a malesuada nisl tortor et ante. Suspendisse vel congue leo, sed euismod est. Suspendisse gravida risus vel nibh condimentum, venenatis porttitor neque luctus. Proin nibh justo, pellentesque sed porta eget, tincidunt a mi.','http://www.artopos.net/templates/artopos/images/redac/middle_photo///1101_moravec.jpg',49.9405,15.190821,1,1,0,'http://www.artopos.net/templates/artopos/images/redac/middle_photo///1101_moravec.jpg',NULL),(120,1177,449,NULL,'','1842-12-13','Šumperk','Ut fringilla nunc id tortor ullamcorper pulvinar at non massa. Curabitur sed ante quis est pulvinar convallis eu nec dolor. Quisque vel nulla ac ante sollicitudin fermentum. Praesent eget aliquam elit. In hac habitasse platea dictumst. Nullam consectetur, quam a feugiat rutrum, quam felis tincidunt magna, et Maecenas dignissim vehicula ligula, id fringilla ligula malesuada et. Nulla facilisi. Mauris molestie ex nulla, nec malesuada mauris maximus eu. Curabitur ultrices sagittis lobortis.','http://www.artopos.net/templates/artopos/images/redac/middle_photo///1115_cze_mg.jpg',49.969284,16.988491,1,1,0,'http://www.artopos.net/templates/artopos/images/redac/middle_photo///1115_cze_mg.jpg',NULL);
/*!40000 ALTER TABLE `painting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post`
--

DROP TABLE IF EXISTS `post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `resolved` tinyint(1) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `img` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `text` longtext COLLATE utf8mb4_unicode_ci,
  `active` tinyint(1) DEFAULT NULL,
  `painting_id` int(11) DEFAULT NULL,
  `tn` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_public_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5A8A6C8DA76ED395` (`user_id`),
  KEY `IDX_5A8A6C8DB00EB939` (`painting_id`),
  CONSTRAINT `FK_5A8A6C8DA76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_5A8A6C8DB00EB939` FOREIGN KEY (`painting_id`) REFERENCES `painting` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=495 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post`
--

LOCK TABLES `post` WRITE;
/*!40000 ALTER TABLE `post` DISABLE KEYS */;
INSERT INTO `post` VALUES (457,1,1,1,'https://artopos.000webhostapp.com/img/placeholders/45.jpg',49.522237666667,14.919503333333,'mobecofowi bobojayiti kufu huluzihu dukudo wefudumu. zine yaturifitu. ',1,112,'https://artopos.000webhostapp.com/img/placeholders/45.jpg',NULL,NULL),(458,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/58.jpg',49.520204333333,14.922503333333,'suwe juta coto ma. nixiye. tolebuceru. himabedi guxu ce. tibulatokixi. gisayoye gijutisu. vewixemo yakefi kutamehetipe ',1,112,'https://artopos.000webhostapp.com/img/placeholders/58.jpg',NULL,NULL),(459,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/39.jpg',49.539037666667,14.90147,'navi ce ceza kokufa laligiwa jazo hibaci. ',1,112,'https://artopos.000webhostapp.com/img/placeholders/39.jpg',NULL,NULL),(460,1,1,1,'https://artopos.000webhostapp.com/img/placeholders/40.jpg',51.543671,15.292997,'nobadu. bu mirimoxuji hisenifi maronayohefu dovitowoyo. bimadu nesugihayi nuwodoku re nujojayoga wiretisi weyayiracu fusogesagasa gi jelupera nihuzu. sifu wabugipaniyi fixomebileni kodidaro fita. ',1,113,'https://artopos.000webhostapp.com/img/placeholders/40.jpg',NULL,NULL),(461,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/35.jpg',51.518971,15.284763666667,'biyilisuciva mekazuxuwatu. xodoregude. yenoyehi gakimasapilo piji janezesivi laxiwo wubolayuje. page. hixuzonuse buyaxahahi xareva muvagasase dazutu dokifezofu sowu xewovayebucu mi lufove. javivuyimi mozolirawide hedo jonipocifa horokiyejuni ',1,113,'https://artopos.000webhostapp.com/img/placeholders/35.jpg',NULL,NULL),(462,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/37.jpg',51.534471,15.281097,'ni nezazamevoxu. ketidulo xajakurawohe. jixu radezejono voliru pogo guyonofefova seruyuhenu fudege. da lukudati zasaki. sihuyayifice. zuce. yaricejosa helato bihefalive yazacucewifo ',1,113,'https://artopos.000webhostapp.com/img/placeholders/37.jpg',NULL,NULL),(463,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/17.jpg',51.531571,15.307063666667,'vakujoli xevowuwa tiyonufu xazadi niluzu vude keto wubo geworiyidipe modofaxibu gipawageke waxovutucuni xera nijepanoze gedopubo vozaka. ',1,113,'https://artopos.000webhostapp.com/img/placeholders/17.jpg',NULL,NULL),(464,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/49.jpg',50.527771,14.280763666667,'difoxe wodiwi gore xo. xa tatazedona curogeri. vipapipipu. ',1,114,'https://artopos.000webhostapp.com/img/placeholders/49.jpg',NULL,NULL),(465,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/13.jpg',50.549337666667,14.275730333333,'vuluso. vetenulebuju. limarawuri hifopapofiva. zexe jesapayayu zawaxo rigazari. vosilojemeju zevezixe duce hiwivaxitu moyihagoxi. bila wijemewuge nako. ',1,114,'https://artopos.000webhostapp.com/img/placeholders/13.jpg',NULL,NULL),(466,1,1,1,'https://artopos.000webhostapp.com/img/placeholders/29.jpg',50.545671,14.301130333333,'yodudinaxiku hapi rehovofisilu peterumeju. viciyijeme kovaxutojasa ',1,114,'https://artopos.000webhostapp.com/img/placeholders/29.jpg',NULL,NULL),(467,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/28.jpg',49.677707,15.615306,'fo haso duluka juporexi. suxa josazinata. vupozidigo kocovuxixuri bigaye ge ',1,115,'https://artopos.000webhostapp.com/img/placeholders/28.jpg',NULL,NULL),(468,1,1,1,'https://artopos.000webhostapp.com/img/placeholders/44.jpg',49.680173666667,15.624106,'lefowiwekozi. zayapafujoti yeya fuxa kapekawe mahodirezike luwo luzele kegepoliha. koxofi. di. ',1,115,'https://artopos.000webhostapp.com/img/placeholders/44.jpg',NULL,NULL),(469,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/21.jpg',49.694540333333,15.608472666667,'yaha co viwoma beyuwo xule nipexegajolo jomatoyi ne vowifacu. gehole detu sohuhiweza xetenihu xoti mudocona befihi zuxi. xasi nekuvu cexoka moyudi hibivepi. ',1,115,'https://artopos.000webhostapp.com/img/placeholders/21.jpg',NULL,NULL),(470,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/10.jpg',49.697140333333,15.625139333333,'dihoce vekinokomeke. taxubojuzi vagu wepiwi. nozazasivexi xizovisiye. gafuwice puhoga. ruvo. nili ra. ciwovuvagixi xu fapiseja nepa ',1,115,'https://artopos.000webhostapp.com/img/placeholders/10.jpg',NULL,NULL),(471,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/28.jpg',49.670773666667,15.606672666667,'fitojawe pi yisa. nedobofivoyo faxino ti zejo xadasejofo sanizajaru yofulivepabi todiwo lo. vopivivovide muso jihene tuca biviri sosetibo gutezi. ratuyuxe xenupahizu leseyo ',1,115,'https://artopos.000webhostapp.com/img/placeholders/28.jpg',NULL,NULL),(472,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/20.jpg',49.681673666667,15.615972666667,'novoco peveripafuve. xuzama wo sucasohoni. rudivoka mofujayegaza bu hobugico. jaxoxawera fodipe. cudivi. ketete coyaxova hacime xocabuxaxoje bimo cikekubi ',1,115,'https://artopos.000webhostapp.com/img/placeholders/20.jpg',NULL,NULL),(473,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/45.jpg',49.152416,15.208778666667,'raruga. wo vuyifugu pitubuwa roza. codofetide yikolo. sifapise zani hirelesupe. mudutexacaku ne vavilu. nafawutegume. ',1,116,'https://artopos.000webhostapp.com/img/placeholders/45.jpg',NULL,NULL),(474,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/34.jpg',49.150482666667,15.217512,'xive dabaronuse hawolabazeni polobo dizaxijafu. faxihaci musoyogu nugici cifixaxagaki. tagifaku zasa ',1,116,'https://artopos.000webhostapp.com/img/placeholders/34.jpg',NULL,NULL),(475,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/53.jpg',49.149182666667,15.223712,'vasatami supo vuhoza leku socojacere. gezigabudo hazi. jozi zehisulezu vutuha. biro vi. soba cujagecemijo paxa zederuwayo tebejuyolulo zojiwejagama. fonodonuliro yefocofudo winafiga lefu huwe ',1,116,'https://artopos.000webhostapp.com/img/placeholders/53.jpg',NULL,NULL),(476,1,1,1,'https://artopos.000webhostapp.com/img/placeholders/31.jpg',49.133616,15.217545333333,'razojiragusa pekimorogo jimeyixuxepi. toyohu deyopenu. suwitibo sokexi. wimo nama lavoduzupo foxa pebumugimuji yatekozu wolokutuzezo. rupedone po pimimiju gubunawuvuro yudexupu. conubafesuge. tujupucejuja. juvejeyomoto. ',1,116,'https://artopos.000webhostapp.com/img/placeholders/31.jpg',NULL,NULL),(477,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/52.jpg',49.139682666667,15.202645333333,'fuka lo. do jubu nipu vewevitave vuyemese tofehifuhiru ',1,116,'https://artopos.000webhostapp.com/img/placeholders/52.jpg',NULL,NULL),(478,1,1,1,'https://artopos.000webhostapp.com/img/placeholders/38.jpg',49.150116,15.196245333333,'hiboweyulovu lupufocuje zimihowefuxa paxutabazupa dubajevefe wimowuluji cituzi cefajoci metuzo. ',1,116,'https://artopos.000webhostapp.com/img/placeholders/38.jpg',NULL,NULL),(479,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/53.jpg',49.146649333333,15.195712,'kanoreboguca do. gucedakezuce runududeyufa xajujitu. conunecu lokuro yini cuja lahude. fa tofihucuhu boka pivesahoya tisu yagenu malekuwodi. ',1,116,'https://artopos.000webhostapp.com/img/placeholders/53.jpg',NULL,NULL),(480,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/9.jpg',49.136849333333,15.218112,'pika tiwizija du romamuzomo tixuzofubadi ho ropuwudohedo goxudewupu rayibe legahejeke kopo fadu johimirodi lerezifode. sujifi fowi pinuhosegawe. ',1,116,'https://artopos.000webhostapp.com/img/placeholders/9.jpg',NULL,NULL),(481,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/18.jpg',49.133282666667,15.201778666667,'duwona bo va ralosedupeho surafe deretasete zugezivebawa kuxote sozozusa jufo jafadafupi woxoxu dayu hopavico cuxo bocehuhari pexeto ',1,116,'https://artopos.000webhostapp.com/img/placeholders/18.jpg',NULL,NULL),(482,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/53.jpg',50.162883333333,16.063703333333,'ceyaviweparu. pade retu kunezajagaro gapeyukotu wa nucewi. locuta zayo kohofure wirugone bisehizo. de wetobo futivi jahurigemebe ',1,117,'https://artopos.000webhostapp.com/img/placeholders/53.jpg',NULL,NULL),(483,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/19.jpg',50.179283333333,16.053503333333,'modicipi suyisuzo si weyafukuveze lixu. yapovocoku moca. zoluhoyifopo gawaputo xururiza pa. gubo ',1,117,'https://artopos.000webhostapp.com/img/placeholders/19.jpg',NULL,NULL),(484,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/3.jpg',50.177283333333,16.04457,'tiriyajuse ligefaxi levike. puxe zigozu haselanutoci sale. bomu ',1,117,'https://artopos.000webhostapp.com/img/placeholders/3.jpg',NULL,NULL),(485,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/34.jpg',49.708627,14.212814,'mekuvapa. lute xagenisu zarine dimi boca doyo bajime vavovecacuna pupejepi. ra ',1,118,'https://artopos.000webhostapp.com/img/placeholders/34.jpg',NULL,NULL),(486,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/14.jpg',49.704893666667,14.203514,'koti miwewozeyu cuxeca xakunofexaze tikete dejocoxekalu. wi. jemivucu xuminayeza. cucu bawe dilafi ',1,118,'https://artopos.000webhostapp.com/img/placeholders/14.jpg',NULL,NULL),(487,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/32.jpg',49.954033333333,15.198021,'ceguwivojufe pafunisiti dagixozeduco pi helu jogu ',1,119,'https://artopos.000webhostapp.com/img/placeholders/32.jpg',NULL,NULL),(488,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/20.jpg',49.9494,15.206887666667,'loroluraxo hinojovojada yafiri setuju ruzikefogovu xapole xakaha. juyikopiso. dapuvutatiye wapefarebati vaje hegukizoga legipo juhubiru nozosenugebi. riyo zoxo hiki toludawasu huriwegogesa. kofa lodotu xecolalaro ',1,119,'https://artopos.000webhostapp.com/img/placeholders/20.jpg',NULL,NULL),(489,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/59.jpg',49.9322,15.201387666667,'lu hoyiwo. mivumucaho nudome je xezemacu je bomewanudu suhixaseseru xadede kofo godofu. kanugo suripadozilo. jobuvokaxe kejuya rofuxufutoxa zutoli vebefu yadi nerudatokefa winivu. ',1,119,'https://artopos.000webhostapp.com/img/placeholders/59.jpg',NULL,NULL),(490,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/56.jpg',49.924833333333,15.186121,'fepiyumodudu. woxaxabayoge mudiwuvuva gexudugole ji vokiviye tugixoduto pudanu. pu labuzixewove tezewikuto dote hafogeleje nojajoka kanojolaveca gadegofiveli hexegi. bohavu ',1,119,'https://artopos.000webhostapp.com/img/placeholders/56.jpg',NULL,NULL),(491,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/33.jpg',49.9517,15.196187666667,'ne nipamojibe fulula xalunisicafu libijo hogulo wuwale coxahejafaxa. zacefayu jaretuza. fikexuyenogu. fuvi gajeyuwuwowo ',1,119,'https://artopos.000webhostapp.com/img/placeholders/33.jpg',NULL,NULL),(492,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/36.jpg',49.954366666667,15.203854333333,'fegomobuxa mubotijuko pecejuxefuca fo. gemufaba loxeyo ',1,119,'https://artopos.000webhostapp.com/img/placeholders/36.jpg',NULL,NULL),(493,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/41.jpg',49.949133333333,15.193854333333,'dolaba jibasupapuni fifarobeko kehenofikoba hutuvolayova. forewo cumahi fuwepefu fevefedoziso ridifihezo. nodogaye cani he fowufi lo ',1,119,'https://artopos.000webhostapp.com/img/placeholders/41.jpg',NULL,NULL),(494,1,0,1,'https://artopos.000webhostapp.com/img/placeholders/17.jpg',49.981217333333,16.982291,'kifupamogu togazi yi nivi lebodaxu joziwoma nobodoza rupahekocaxo ridotidojure riza xusuzibowe selibi lisana dinifi. niyasawata ',1,120,'https://artopos.000webhostapp.com/img/placeholders/17.jpg',NULL,NULL);
/*!40000 ALTER TABLE `post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refresh_token`
--

DROP TABLE IF EXISTS `refresh_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refresh_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expires_at` int(11) DEFAULT NULL,
  `scope` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C74F21955F37A13B` (`token`),
  KEY `IDX_C74F219519EB6921` (`client_id`),
  KEY `IDX_C74F2195A76ED395` (`user_id`),
  CONSTRAINT `FK_C74F219519EB6921` FOREIGN KEY (`client_id`) REFERENCES `client` (`id`),
  CONSTRAINT `FK_C74F2195A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refresh_token`
--

LOCK TABLES `refresh_token` WRITE;
/*!40000 ALTER TABLE `refresh_token` DISABLE KEYS */;
INSERT INTO `refresh_token` VALUES (27,6,1,'ODNkYzgzNTY4YzgyN2VlY2I3ZjFlNTkzNGU3ZTlmZTk4ODZhOTc3ZmFmNzBlMzk5Mzk5MDVkMzk5MWY1OWFmZg',1525184165,'user'),(28,6,1,'MGY3MzE5MWRmNTRjMGY2NjIzNmJlYzVlNGY5ZDE1NDNlMzViNTg4Mzk5ZDE2NGU1MzY1YjdjYTI2MmVhYTAwYg',1525184428,'user'),(29,6,1,'MjFkMGNhZGUyYWZkZjU3MzkyYWI3NzcyOTU0NWE2NWMwZWU1NTEzNjk0ZjE4NDY1NTJkNGIxMzljYTRhZmMyYw',1525184516,'user'),(30,6,1,'OTdkZjAyMzIyOTUwYjc1OTU0ZTRmOTBkMjhjNDhjOTRkNGIyZjFhNzM5ZjYyODVjYjQyNjk0NTUwMWMxMWZlMQ',1525184628,'user'),(31,6,1,'ZGNmMWY5MWZhY2EzZGU2NDllMDBmZGY3ZjNjZjYzNDE4OGIxYTIyNGVlMTZmNjY2MDNjYjE1ZGEzMmU3NzFhNw',1525185069,'user'),(32,6,1,'ZGU0YjgwYTIwMzQzMzZjYTdlOGJkOWMzOWYzNWY1ZWY3OGM0MmQ2YWU3ODlkOTlkYzhkNmMzZDFiMmYzYTkxYg',1525185142,'user'),(33,6,1,'N2YxZWIzYzQxMDM3M2U4OGYwN2UxZGJlYzA3NWQ5NDljZDI4MjAyNTM1NjFmYzhlM2EwMjU5NWZhOTU2MWNiNA',1525185204,'user'),(34,6,1,'OTZjYzcyZGVlYmE4ZjYyNmQ2ZjVlOGM4NTMzNDVlNmFmYzAyMjJlZTllYTA1NGVmNDI1OGVkOTAwYzIzZDYwYg',1525185281,'user'),(35,6,1,'MDY1Y2Q3YjM1MDE0ZjU2NjMxZjU0YjkxNDVhYzdjMGRjMTg5OWU4MDY0NTllY2Y2Yzk2NWYzYWM4YzhjNzU5YQ',1525185483,'user'),(36,6,1,'ZTU2ZmU5M2MyN2Y5OWY1MTg5OGUyNDA0YTgxNzI1M2JlMzRjNGE1M2I0NTI0YjA3OTAyNWNkNzQ3NjBhNGFhNA',1525186194,'user'),(37,6,1,'ZTVjYzMwYzE1YTg3MTI2ZWIyNzIxYjc3ZTc2ZGEzZDRmYzg5MGU5MGU4NWFjOTc2MTNhZmMxNTJhNTYwMWU0OA',1525186251,'user');
/*!40000 ALTER TABLE `refresh_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=597 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
INSERT INTO `tag` VALUES (558,'judibeso'),(559,'bolehiwe'),(560,'kifixovofoci'),(561,'himuwalinosupi'),(562,'nu'),(563,'ce'),(564,'hibelowa'),(565,'vavazocenica'),(566,'zitesupojexaza'),(567,'yogotowasi'),(568,'haboponapeyo'),(569,'wuwa'),(570,'vo'),(571,'kaxu'),(572,'nixixayotahecu'),(573,'wepilibewuxuzo'),(574,'joze'),(575,'segomanaxe'),(576,'sapeyudo'),(577,'josavebuda'),(578,'yafexi'),(579,'werumoju'),(580,'lexemeyoboceja'),(581,'jixa'),(582,'johimiro'),(583,'zotekiduzaxa'),(584,'muga'),(585,'rusefoxigayu'),(586,'tilehusazivore'),(587,'bojacoxi'),(588,'loraguvakeho'),(589,'hugoveroxale'),(590,'nebe'),(591,'lijetejo'),(592,'za'),(593,'kuhudoyusi'),(594,'ritutepicuxe'),(595,'ficagorojehi'),(596,'liri');
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_painting`
--

DROP TABLE IF EXISTS `tag_painting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_painting` (
  `tag_id` int(11) NOT NULL,
  `painting_id` int(11) NOT NULL,
  PRIMARY KEY (`tag_id`,`painting_id`),
  KEY `IDX_5088907BBAD26311` (`tag_id`),
  KEY `IDX_5088907BB00EB939` (`painting_id`),
  CONSTRAINT `FK_5088907BB00EB939` FOREIGN KEY (`painting_id`) REFERENCES `painting` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_5088907BBAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_painting`
--

LOCK TABLES `tag_painting` WRITE;
/*!40000 ALTER TABLE `tag_painting` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag_painting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salt` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:simple_array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'adam','adakes162@gmail.com','e6446187da1f941d834e663c530fef91','$2y$13$2dWgHXHOOCmjPWd7jbctmuD28VbKoI8rJqKYOjkyPPsPi.Jq./OcO',1,'[\"ROLE_ADMIN\"]'),(3,'aaa','a@a.a','d96f987d336f6fb0330f39eb3032864d','$2y$13$baeOTMFV.EheodYz7IKkx.dnR0/Giy8y1Qy56JFVeZUzYXfrkp0AS',1,'ROLE_USER'),(4,'test_user','test@user.test','4d439f6015fc7f9ddf216ee16ca5b511','$2y$13$a6V.7TfxLsg9XMU8.lPoRuL/uHdsUjwGrE//6aihGCemRoAXDJea.',1,'ROLE_USER');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-27 14:25:24
